#ifndef __GATE_NODEHANDLE_H__
#define __GATE_NODEHANDLE_H__
#include "core/common/singleton.h"
#include "server/data_dispatcher.h"

namespace Gate {
	class NodeHandle : public Common::Singleton<NodeHandle>
	{
	public:
		void Init(Server::DataDispatcher *dp);

		void ServerAddRes(const Proto::Core::MsgBody& msg, Server::DisParam& dp);
		void ServerAddTrap(const Proto::Core::MsgBody& msg, Server::DisParam& dp);
	};
}
#endif

