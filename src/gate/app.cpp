#include "core/common/network.h"
#include "server/serverdata_mgr.h"
#include "gate/node_handle.h"
#include "gate/app.h"

namespace Gate {
	static App _app;
	App::App()
		:ServerApp(Proto::Core::STGate)
	{
	}


	App::~App()
	{
	}

	/*
	*Init Server
	*/
	bool App::InitServer()
	{
		CXEngine::App::AppConfig &config = GetAppConfig();
		Server::ServerData &data = Server::ServerDataMgr::Instance()->GetMyServerData();

		std::string ip  = Common::GetInterfaceAddressStr();
		_geteServer = new CXEngine::Event::TcpServer(GetEventLoop());
		_gateDis = new Server::DataDispatcher();
		Common::uint16_t port = _geteServer->CreateServer(ip, StartBindPort, EndBindPort, _gateDis);
		if (port == 0) {
			printf("[ERROR] create gate server failed\n");
			return false;
		}

		data.info.set_ip(Common::Ip4ToAddr(ip));
		data.info.set_port(htons(port));
		printf("[LOG] create gate server %s:%d sucess!\n", ip.c_str(), port);

		NodeHandle::Instance()->Init(_nodeDis);

		return true;
	}
}
