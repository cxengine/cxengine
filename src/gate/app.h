#ifndef __GATE_APP_H__
#define __GATE_APP_H__
#include "server/server_app.h"
#include "core/event/tcp_server.h"
#include "core/event/tcp_client.h"

namespace Gate {
	class App : public Server::ServerApp
	{
	public:
		App();
		~App();

		/*
		*Init Server
		*/
		virtual bool InitServer();

	private:
		CXEngine::Event::TcpServer *_geteServer;
		Server::DataDispatcher *_gateDis;
	};
}

#endif

