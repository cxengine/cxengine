#include "core/common/network.h"
#include "server/serverdata_mgr.h"
#include "social/node_handle.h"
#include "social/app.h"

namespace Social {
	static App _app;
	App::App()
		:ServerApp(Proto::Core::STSocial)
	{
	}


	App::~App()
	{
	}

	/*
	*Init Server
	*/
	bool App::InitServer()
	{
		CXEngine::App::AppConfig &config = GetAppConfig();

		NodeHandle::Instance()->Init(_nodeDis);
		return true;
	}
}
