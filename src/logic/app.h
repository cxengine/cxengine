#ifndef __LOGIC_APP_H__
#define __GATE_APP_H__
#include "server/server_app.h"
#include "core/event/tcp_server.h"
#include "core/event/tcp_client.h"

namespace Logic {
	class App : public Server::ServerApp
	{
	public:
		App();
		~App();

		/*
		*Init Server
		*/
		virtual bool InitServer();

	};
}

#endif

