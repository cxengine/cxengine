#include "core/common/network.h"
#include "server/serverdata_mgr.h"
#include "logic/node_handle.h"
#include "logic/app.h"


namespace Logic {
	static App _app;
	App::App()
		:ServerApp(Proto::Core::STLogic)
	{
	}


	App::~App()
	{
	}

	/*
	*Init Server
	*/
	bool App::InitServer()
	{
		CXEngine::App::AppConfig &config = GetAppConfig();

		NodeHandle::Instance()->Init(_nodeDis);
		return true;
	}
}
