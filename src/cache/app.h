#ifndef __CACHE_APP_H__
#define __CACHE_APP_H__
#include "core/event/tcp_server.h"
#include "core/event/tcp_client.h"
#include "mysqlcache/cache_server.h"
#include "server/server_app.h"

namespace Cache {
	class App : public Server::ServerApp
	{
	public:
		App();
		~App();

		/*
		*Init Server
		*/
		virtual bool InitServer();

	private:
		bool CreateServer();
		bool CreateDbCache();

	private:
		CXEngine::Event::TcpServer *_cacheServer;
		Server::DataDispatcher *_cacheDis;
		MysqlCache::CacheServer *_cacheInstance;
	};
}

#endif

