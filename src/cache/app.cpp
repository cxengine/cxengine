#include "core/common/network.h"
#include "core/common/string.h"
#include "server/serverdata_mgr.h"
#include "mysqlcache/cache_def.h"
#include "cache/node_handle.h"
#include "cache/app.h"

namespace Cache {
	static App _app;
	App::App()
		:ServerApp(Proto::Core::STCache)
	{
	}


	App::~App()
	{
	}

	/*
	*Init Server
	*/
	bool App::InitServer()
	{
		CreateServer();
		CreateDbCache();

		NodeHandle::Instance()->Init(_nodeDis);

		return true;
	}

	bool App::CreateServer()
	{
		CXEngine::App::AppConfig &config = GetAppConfig();
		
		std::string ip = Common::GetInterfaceAddressStr();
		_cacheServer = new CXEngine::Event::TcpServer(GetEventLoop());
		_cacheDis = new Server::DataDispatcher();
		Common::uint16_t port = _cacheServer->CreateServer(ip, StartBindPort, EndBindPort, _cacheDis);
		if (port == 0) {
			printf("[ERROR] create cache server failed\n");
			return false;
		}

		Server::ServerData &data = Server::ServerDataMgr::Instance()->GetMyServerData();
		data.info.set_ip(Common::Ip4ToAddr(ip));
		data.info.set_port(htons(port));
		printf("[LOG] create cache server %s:%d sucess!\n", ip.c_str(), port);

		return true;
	}

	bool App::CreateDbCache()
	{
		CXEngine::App::AppConfig &config = GetAppConfig();

		MysqlCache::CacheParam cp;
		MysqlCache::ProtoParam pp;
		std::string protodir = config.GetStrValue("proto_dir");

		std::vector<MysqlCache::CacheParam> cacheParams;
		Common::Value value = config.GetValue("mysql");
		Common::ValueMap& valueMap = value.asValueMap();
		for (auto iter = valueMap.begin(); iter != valueMap.end(); ++iter) {
			Common::Value tmpValue = iter->second;
			Common::ValueMap& tmpMap = tmpValue.asValueMap();
			MysqlCache::CacheParam param;
			param.dbname = iter->first;
			param.ip = tmpMap["ip"].asString();
			param.port = tmpMap["port"].asInt();
			param.user = tmpMap["user"].asString();
			param.pwd = tmpMap["pwd"].asString();
			param.protofile = tmpMap["proto"].asString();
			cacheParams.push_back(param);
		}
		_cacheInstance = new MysqlCache::CacheServer();
		return _cacheInstance->InitCache(_cacheDis, protodir, cacheParams);
	}
}
