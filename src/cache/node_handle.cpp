#include "core/common/types.h"
#include "proto/ss/ss_def.pb.h"
#include "proto/ss/ss_session.pb.h"
#include "server/serverdata_mgr.h"
#include "cache/node_handle.h"


namespace Cache {
	void NodeHandle::Init(Server::DataDispatcher *dp)
	{
		dp->Register(Proto::SS::CTSession, Proto::SS::CCSServerAddRes, BIND_CALLBACK_2(NodeHandle::ServerAddRes, this));
		dp->Register(Proto::SS::CTSession, Proto::SS::CCSServerAddTrap, BIND_CALLBACK_2(NodeHandle::ServerAddTrap, this));
	}

	void NodeHandle::ServerAddRes(const Proto::Core::MsgBody& msg, Server::DisParam& dp)
	{
		const Proto::SS::ServerAddRes &res = msg.GetExtension(Proto::SS::server_add_res);
		for (int i = 0; i < res.infos_size(); i++) {
			const ::Proto::Core::ServerInfo& info = res.infos(i);
			Server::ServerData &data = Server::ServerDataMgr::Instance()->GetServerData(info.server_id());
			data.info.CopyFrom(info);
		}
	}

	void NodeHandle::ServerAddTrap(const Proto::Core::MsgBody& msg, Server::DisParam& dp)
	{
		const Proto::SS::ServerAddTrap &trap = msg.GetExtension(Proto::SS::server_add_trap);
		const ::Proto::Core::ServerInfo& info = trap.info();
		Server::ServerData &data = Server::ServerDataMgr::Instance()->GetServerData(info.server_id());
		data.info.CopyFrom(info);
	}
}
