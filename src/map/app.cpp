#include "core/common/network.h"
#include "server/serverdata_mgr.h"
#include "map/node_handle.h"
#include "map/app.h"

namespace Map {
	static App _app;
	App::App()
		:ServerApp(Proto::Core::STMap)
	{
	}


	App::~App()
	{
	}

	/*
	*Init Server
	*/
	bool App::InitServer()
	{
		CXEngine::App::AppConfig &config = GetAppConfig();

		NodeHandle::Instance()->Init(_nodeDis);
		return true;
	}
}
