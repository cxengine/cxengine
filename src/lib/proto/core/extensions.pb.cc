// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: proto/core/extensions.proto

#define INTERNAL_SUPPRESS_PROTOBUF_FIELD_DEPRECATION
#include "proto/core/extensions.pb.h"

#include <algorithm>

#include <google/protobuf/stubs/common.h>
#include <google/protobuf/stubs/once.h>
#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/wire_format_lite_inl.h>
#include <google/protobuf/descriptor.h>
#include <google/protobuf/generated_message_reflection.h>
#include <google/protobuf/reflection_ops.h>
#include <google/protobuf/wire_format.h>
// @@protoc_insertion_point(includes)

namespace Proto {
namespace Core {

namespace {


}  // namespace


void protobuf_AssignDesc_proto_2fcore_2fextensions_2eproto() {
  protobuf_AddDesc_proto_2fcore_2fextensions_2eproto();
  const ::google::protobuf::FileDescriptor* file =
    ::google::protobuf::DescriptorPool::generated_pool()->FindFileByName(
      "proto/core/extensions.proto");
  GOOGLE_CHECK(file != NULL);
}

namespace {

GOOGLE_PROTOBUF_DECLARE_ONCE(protobuf_AssignDescriptors_once_);
inline void protobuf_AssignDescriptorsOnce() {
  ::google::protobuf::GoogleOnceInit(&protobuf_AssignDescriptors_once_,
                 &protobuf_AssignDesc_proto_2fcore_2fextensions_2eproto);
}

void protobuf_RegisterTypes(const ::std::string&) {
  protobuf_AssignDescriptorsOnce();
}

}  // namespace

void protobuf_ShutdownFile_proto_2fcore_2fextensions_2eproto() {
}

void protobuf_AddDesc_proto_2fcore_2fextensions_2eproto() {
  static bool already_here = false;
  if (already_here) return;
  already_here = true;
  GOOGLE_PROTOBUF_VERIFY_VERSION;

  ::google::protobuf::protobuf_AddDesc_google_2fprotobuf_2fdescriptor_2eproto();
  ::google::protobuf::DescriptorPool::InternalAddGeneratedFile(
    "\n\033proto/core/extensions.proto\022\nProto.Cor"
    "e\032 google/protobuf/descriptor.proto:0\n\005m"
    "_pks\022\037.google.protobuf.MessageOptions\030\320\206"
    "\003 \001(\t:3\n\010m_divkey\022\037.google.protobuf.Mess"
    "ageOptions\030\321\206\003 \001(\t:3\n\010m_divnum\022\037.google."
    "protobuf.MessageOptions\030\322\206\003 \001(\r:/\n\004m_cn\022"
    "\037.google.protobuf.MessageOptions\030\323\206\003 \001(\t"
    ":1\n\006m_desc\022\037.google.protobuf.MessageOpti"
    "ons\030\324\206\003 \001(\t:3\n\010m_relchk\022\037.google.protobu"
    "f.MessageOptions\030\325\206\003 \001(\t:4\n\tm_autoinc\022\037."
    "google.protobuf.MessageOptions\030\326\206\003 \001(\t:-"
    "\n\004f_cn\022\035.google.protobuf.FieldOptions\030\320\206"
    "\003 \001(\t:/\n\006f_desc\022\035.google.protobuf.FieldO"
    "ptions\030\321\206\003 \001(\t:0\n\007f_count\022\035.google.proto"
    "buf.FieldOptions\030\322\206\003 \001(\t:1\n\010f_length\022\035.g"
    "oogle.protobuf.FieldOptions\030\323\206\003 \001(\t:1\n\004e"
    "_cn\022!.google.protobuf.EnumValueOptions\030\320"
    "\206\003 \001(\t:3\n\006e_desc\022!.google.protobuf.EnumV"
    "alueOptions\030\321\206\003 \001(\t", 739);
  ::google::protobuf::MessageFactory::InternalRegisterGeneratedFile(
    "proto/core/extensions.proto", &protobuf_RegisterTypes);
  ::google::protobuf::internal::ExtensionSet::RegisterExtension(
    &::google::protobuf::MessageOptions::default_instance(),
    50000, 9, false, false);
  ::google::protobuf::internal::ExtensionSet::RegisterExtension(
    &::google::protobuf::MessageOptions::default_instance(),
    50001, 9, false, false);
  ::google::protobuf::internal::ExtensionSet::RegisterExtension(
    &::google::protobuf::MessageOptions::default_instance(),
    50002, 13, false, false);
  ::google::protobuf::internal::ExtensionSet::RegisterExtension(
    &::google::protobuf::MessageOptions::default_instance(),
    50003, 9, false, false);
  ::google::protobuf::internal::ExtensionSet::RegisterExtension(
    &::google::protobuf::MessageOptions::default_instance(),
    50004, 9, false, false);
  ::google::protobuf::internal::ExtensionSet::RegisterExtension(
    &::google::protobuf::MessageOptions::default_instance(),
    50005, 9, false, false);
  ::google::protobuf::internal::ExtensionSet::RegisterExtension(
    &::google::protobuf::MessageOptions::default_instance(),
    50006, 9, false, false);
  ::google::protobuf::internal::ExtensionSet::RegisterExtension(
    &::google::protobuf::FieldOptions::default_instance(),
    50000, 9, false, false);
  ::google::protobuf::internal::ExtensionSet::RegisterExtension(
    &::google::protobuf::FieldOptions::default_instance(),
    50001, 9, false, false);
  ::google::protobuf::internal::ExtensionSet::RegisterExtension(
    &::google::protobuf::FieldOptions::default_instance(),
    50002, 9, false, false);
  ::google::protobuf::internal::ExtensionSet::RegisterExtension(
    &::google::protobuf::FieldOptions::default_instance(),
    50003, 9, false, false);
  ::google::protobuf::internal::ExtensionSet::RegisterExtension(
    &::google::protobuf::EnumValueOptions::default_instance(),
    50000, 9, false, false);
  ::google::protobuf::internal::ExtensionSet::RegisterExtension(
    &::google::protobuf::EnumValueOptions::default_instance(),
    50001, 9, false, false);
  ::google::protobuf::internal::OnShutdown(&protobuf_ShutdownFile_proto_2fcore_2fextensions_2eproto);
}

// Force AddDescriptors() to be called at static initialization time.
struct StaticDescriptorInitializer_proto_2fcore_2fextensions_2eproto {
  StaticDescriptorInitializer_proto_2fcore_2fextensions_2eproto() {
    protobuf_AddDesc_proto_2fcore_2fextensions_2eproto();
  }
} static_descriptor_initializer_proto_2fcore_2fextensions_2eproto_;
const ::std::string m_pks_default("");
::google::protobuf::internal::ExtensionIdentifier< ::google::protobuf::MessageOptions,
    ::google::protobuf::internal::StringTypeTraits, 9, false >
  m_pks(kMPksFieldNumber, m_pks_default);
const ::std::string m_divkey_default("");
::google::protobuf::internal::ExtensionIdentifier< ::google::protobuf::MessageOptions,
    ::google::protobuf::internal::StringTypeTraits, 9, false >
  m_divkey(kMDivkeyFieldNumber, m_divkey_default);
::google::protobuf::internal::ExtensionIdentifier< ::google::protobuf::MessageOptions,
    ::google::protobuf::internal::PrimitiveTypeTraits< ::google::protobuf::uint32 >, 13, false >
  m_divnum(kMDivnumFieldNumber, 0u);
const ::std::string m_cn_default("");
::google::protobuf::internal::ExtensionIdentifier< ::google::protobuf::MessageOptions,
    ::google::protobuf::internal::StringTypeTraits, 9, false >
  m_cn(kMCnFieldNumber, m_cn_default);
const ::std::string m_desc_default("");
::google::protobuf::internal::ExtensionIdentifier< ::google::protobuf::MessageOptions,
    ::google::protobuf::internal::StringTypeTraits, 9, false >
  m_desc(kMDescFieldNumber, m_desc_default);
const ::std::string m_relchk_default("");
::google::protobuf::internal::ExtensionIdentifier< ::google::protobuf::MessageOptions,
    ::google::protobuf::internal::StringTypeTraits, 9, false >
  m_relchk(kMRelchkFieldNumber, m_relchk_default);
const ::std::string m_autoinc_default("");
::google::protobuf::internal::ExtensionIdentifier< ::google::protobuf::MessageOptions,
    ::google::protobuf::internal::StringTypeTraits, 9, false >
  m_autoinc(kMAutoincFieldNumber, m_autoinc_default);
const ::std::string f_cn_default("");
::google::protobuf::internal::ExtensionIdentifier< ::google::protobuf::FieldOptions,
    ::google::protobuf::internal::StringTypeTraits, 9, false >
  f_cn(kFCnFieldNumber, f_cn_default);
const ::std::string f_desc_default("");
::google::protobuf::internal::ExtensionIdentifier< ::google::protobuf::FieldOptions,
    ::google::protobuf::internal::StringTypeTraits, 9, false >
  f_desc(kFDescFieldNumber, f_desc_default);
const ::std::string f_count_default("");
::google::protobuf::internal::ExtensionIdentifier< ::google::protobuf::FieldOptions,
    ::google::protobuf::internal::StringTypeTraits, 9, false >
  f_count(kFCountFieldNumber, f_count_default);
const ::std::string f_length_default("");
::google::protobuf::internal::ExtensionIdentifier< ::google::protobuf::FieldOptions,
    ::google::protobuf::internal::StringTypeTraits, 9, false >
  f_length(kFLengthFieldNumber, f_length_default);
const ::std::string e_cn_default("");
::google::protobuf::internal::ExtensionIdentifier< ::google::protobuf::EnumValueOptions,
    ::google::protobuf::internal::StringTypeTraits, 9, false >
  e_cn(kECnFieldNumber, e_cn_default);
const ::std::string e_desc_default("");
::google::protobuf::internal::ExtensionIdentifier< ::google::protobuf::EnumValueOptions,
    ::google::protobuf::internal::StringTypeTraits, 9, false >
  e_desc(kEDescFieldNumber, e_desc_default);

// @@protoc_insertion_point(namespace_scope)

}  // namespace Core
}  // namespace Proto

// @@protoc_insertion_point(global_scope)
