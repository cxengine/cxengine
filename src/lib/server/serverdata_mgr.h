#ifndef __CXENGINE_SERVER_SERVERINFOMGR_H__
#define __CXENGINE_SERVER_SERVERINFOMGR_H__
#include "core/event/tcp_session.h"
#include "core/common/types.h"
#include "core/common/singleton.h"
#include "proto/core/msg.pb.h"

namespace Server {
	class ServerData {
	public:
		ServerData():session(nullptr){}
		Proto::Core::ServerInfo info;
		CXEngine::Event::TcpSession *session;
	};

	class ServerDataMgr : public Common::Singleton<ServerDataMgr>
	{
	public:
		ServerDataMgr();

		/*
		*set my server id
		*/
		void SetMyServerID(Common::uint64_t serverid);

		/*
		*get my server data
		*/
		ServerData &GetMyServerData();

		/*
		*get server data by id
		*if data is not exist, create it
		*/
		ServerData &GetServerData(Common::uint64_t serverid);

		/*
		*get server data by id
		*if data is not exist, return nullptr
		*/
		ServerData *GetServerDataPtr(Common::uint64_t serverid);

		/*
		*get all server data
		*/
		const std::unordered_map<Common::uint64_t, ServerData *> &GetAllServerData();
		

	private:
		std::unordered_map<Common::uint64_t, ServerData *> _allDatas;
		ServerData *_myData;
	};

}
#endif