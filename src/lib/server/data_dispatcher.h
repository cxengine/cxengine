#ifndef __CXENGINE_SERVER_DATADISPATCHER_H__
#define __CXENGINE_SERVER_DATADISPATCHER_H__
#include "core/event/data_handler.h"
#include "core/event/tcp_session.h"
#include "core/common/types.h"
#include "core/common/dispatch.h"
#include "server/param_def.h"
#include "proto/core/msg.pb.h"

namespace Server {
	typedef std::function<bool(const DisParam&)> EventHandlerCallback;
	class DataDispatcher : public CXEngine::Event::DataHandler, public Common::Dispatcher<Proto::Core::MsgBody, DisParam>
	{
	public:
		DataDispatcher();
		~DataDispatcher();

		/*
		*tcpsession active
		*/
		virtual void SessionActive(CXEngine::Event::TcpSession *session);

		/*
		*tcpsession inactive
		*/
		virtual void SessionInactive(CXEngine::Event::TcpSession *session);

		/*
		*tcpsession have data to read
		*
		* |....4 byte....|....20 byte....|....n byte....|
		*    data size       head data      body data
		*/
		virtual void DataArrival(CXEngine::Event::TcpSession *session);

		/*
		*Add data prehandler
		*/
		void AddPreHandler(const EventHandlerCallback &handler);

		/*
		*Channel Active Handler
		*/
		void AddActiveHandler(const EventHandlerCallback &handler);

		/*
		*Channel InActive Handler
		*/
		void AddInactiveHandler(const EventHandlerCallback &handler);

		/*
		*Invalid data handler
		*/
		virtual void DefaultHandler(const Proto::Core::MsgBody& msgbody, DisParam& channel);

	private:
		char buf[SESSIONBUFSIZE];
		EventHandlerCallback _preHandler;
		EventHandlerCallback _activeHandler;
		EventHandlerCallback _inactiveHandler;
	};
}
#endif

