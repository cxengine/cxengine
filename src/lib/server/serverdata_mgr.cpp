#include "server/serverdata_mgr.h"

namespace Server {
	ServerDataMgr::ServerDataMgr()
	{
		_myData = new ServerData;
	}
	/*
	*set my server id
	*/
	void ServerDataMgr::SetMyServerID(Common::uint64_t serverid)
	{
		_allDatas[serverid] = _myData;
		_myData->info.set_server_id(serverid);
	}

	/*
	*get my server data
	*/
	ServerData &ServerDataMgr::GetMyServerData()
	{
		return *_myData;
	}

	/*
	*get server data by id
	*if data is not exist, create it
	*/
	ServerData &ServerDataMgr::GetServerData(Common::uint64_t serverid)
	{
		auto iter = _allDatas.find(serverid);
		if (iter != _allDatas.end()) {
			return *(iter->second);
		}
		
		auto data = new ServerData();
		data->info.set_server_id(serverid);
		_allDatas[serverid] = data;

		return *data;
	}

	/*
	*get server data by id
	*if data is not exist, return nullptr
	*/
	ServerData *ServerDataMgr::GetServerDataPtr(Common::uint64_t serverid)
	{
		auto iter = _allDatas.find(serverid);
		if (iter != _allDatas.end()) {
			return iter->second;
		}
		return nullptr;
	}

	/*
	*get all server data
	*/
	const std::unordered_map<Common::uint64_t, ServerData *> &ServerDataMgr::GetAllServerData()
	{
		return _allDatas;
	}
}