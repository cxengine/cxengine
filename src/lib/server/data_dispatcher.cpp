#include "server/data_dispatcher.h"
#include "server/channel.h"


namespace Server {
	DataDispatcher::DataDispatcher()
		:_preHandler(nullptr)
		, _activeHandler(nullptr)
		, _inactiveHandler(nullptr)
	{
	}


	DataDispatcher::~DataDispatcher()
	{
	}

	/*
	*tcpsession active
	*/
	void DataDispatcher::SessionActive(CXEngine::Event::TcpSession *session)
	{
		if (_activeHandler) {
			DisParam param; Channel ch(session);
			param.channel = &ch;
			param.session = session;
			_activeHandler(param);
		}
	}

	/*
	*tcpsession inactive
	*/
	void DataDispatcher::SessionInactive(CXEngine::Event::TcpSession *session)
	{
		if (_activeHandler) {
			DisParam param; Channel ch(session);
			param.channel = &ch;
			param.session = session;
			_inactiveHandler(param);
		}
	}


	/*
	*tcpsession have data to read
	*
	* |....4 byte....|....20 byte....|....n byte....|
	*    data size       head data      body data
	*/
	void DataDispatcher::DataArrival(CXEngine::Event::TcpSession *session)
	{
		DisParam param;
		Channel ch(session);
		while (true) {
			Common::uint32_t alllen = 0;
			session->PeekData(&alllen, Proto::Core::MsgLenFieldSize);
			alllen = ntohl(alllen);
			if (alllen >= Proto::Core::MsgLenFieldSize + Proto::Core::MsgHeadLen && (int)session->DataSize() >= alllen) {
				session->ReadData(buf, alllen);
				int datalen = alllen - Proto::Core::MsgLenFieldSize - Proto::Core::MsgHeadLen;
				Proto::Core::MsgHead msgHead;  
				msgHead.ParseFromArray(buf + Proto::Core::MsgLenFieldSize, Proto::Core::MsgHeadLen);

				param.channel = &ch;
				param.session = session;
				param.type = msgHead.cmd() >> 16;
				param.code = msgHead.cmd() & 0xffff;
				param.mr.msg = buf;
				param.mr.len = alllen;

				bool ret = true;
				if (_preHandler) ret = _preHandler(param);
				if (ret) {
					Proto::Core::MsgBody msgBody; if (datalen > 0)msgBody.ParseFromArray(buf + Proto::Core::MsgLenFieldSize + Proto::Core::MsgHeadLen, datalen);
					Dispatch(param.type, param.code, msgBody, param);
				}
			}
			else {
				break;
			}
		}
	}

	/*
	*Add data prehandler
	*/
	void DataDispatcher::AddPreHandler(const EventHandlerCallback &handler)
	{
		_preHandler = handler;
	}


	/*
	*Channel Active Handler
	*/
	void DataDispatcher::AddActiveHandler(const EventHandlerCallback &handler)
	{
		_activeHandler = handler;
	}

	/*
	*Channel InActive Handler
	*/
	void DataDispatcher::AddInactiveHandler(const EventHandlerCallback &handler)
	{
		_inactiveHandler = handler;
	}

	/*
	*Invalid data handler
	*/
	void DataDispatcher::DefaultHandler(const Proto::Core::MsgBody& msgbody, DisParam& param)
	{
		printf("[ERROR] type = %d, code = %d, not defined data\n", param.type, param.code);
	}
}
