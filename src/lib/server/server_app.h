#ifndef __CXENGINE_SERVER_SERVERAPP_H__
#define __CXENGINE_SERVER_SERVERAPP_H__
#include "proto/core/msg.pb.h"
#include "core/event/time_heap.h"
#include "core/event/tcp_client.h"
#include "core/app/app_base.h"
#include "server/data_dispatcher.h"
#include "server/serverdata_mgr.h"

#ifdef WIN32
#pragma comment(lib, "libres_server.lib")
#pragma comment(lib, "libres_mysqlcache.lib")
#pragma comment(lib, "libres_proto.lib")
#pragma comment(lib, "libprotobuf.lib")
#if _MSC_VER >= 1900
#pragma comment (lib, "libmysql32_vs140.lib")
#pragma comment (lib, "mysqlclient32_vs140.lib")
#else
#pragma comment (lib, "libmysql32.lib")
#pragma comment (lib, "mysqlclient32.lib")
#endif
#endif

#define StartBindPort		4000
#define EndBindPort			9000

namespace Server {

	class ServerApp : public CXEngine::App::AppBase
	{
	public:
		ServerApp(int type);
		virtual ~ServerApp();

		/*
		*��ʼ��ServerApp
		*/
		virtual bool InitApp();

		/*
		*Init Server
		*/
		virtual bool InitServer() = 0;


	protected:
		/*
		*Connect Node Server
		*/
		bool ConnectNodeServer(std::string &ip, Common::uint16_t port);

		/*
		*node connection active
		*/
		bool NodeActiveCallback(const DisParam& dp);

		/*
		*node connection inactive
		*/
		bool NodeInactiveCallback(const DisParam& dp);

	protected:
		CXEngine::Event::TcpClient *_nodeClient;
		DataDispatcher *_nodeDis;

	};
}
#endif

