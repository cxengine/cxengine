#include "channel.h"


namespace Server {
	Channel::Channel(CXEngine::Event::TcpSession *session)
		:_session(session)
		, _clear(true)
	{
	}


	Channel::~Channel()
	{
	}

	Proto::Core::MsgBody &Channel::MutableMsgBody()
	{
		if (!_clear) printf("[ERROR] msgbody is not clear\n");
		_clear = false;
		return _msgBody;
	}

	void Channel::Clear()
	{
		_msgBody.Clear();
		_clear = true;
	}

	void Channel::SendMsg(short type, short code)
	{
		SendMsg(type, code, 0);
	}

	void Channel::SendMsg(short type, short code, int errorcode)
	{
		_msgBody.set_err_code(errorcode);
		SendMsg(type, code, _msgBody);
	}

	void Channel::SendMsg(short type, short code, Proto::Core::MsgBody &msg)
	{
		Proto::Core::MsgHead head;
		head.set_cmd(type << 16 | code);
		head.set_seq(0x111111);
		head.set_obj_id(0x111111);

		char buf[SESSIONBUFSIZE];
		int size = SerializeMsg(head, msg, buf);

		SendMsg(buf, size);
		Clear();
	}

	void Channel::SendMsg(const char *buf, int len)
	{
		if (_session)
			_session->SendData(buf, len);
	}


	/*
	* |....4 byte....|....20 byte....|....n byte....|
	*    data size       head data      body data  
	*/
	int Channel::SerializeMsg(Proto::Core::MsgHead &head, Proto::Core::MsgBody &msg, char *buf)
	{
		int bodylen = msg.ByteSize();
		int allsize = Proto::Core::MsgLenFieldSize + Proto::Core::MsgHeadLen + bodylen;

		Common::uint32_t netlen = htonl(allsize);
		memcpy(buf, &netlen, Proto::Core::MsgLenFieldSize);
		head.SerializeToArray(buf + Proto::Core::MsgLenFieldSize, Proto::Core::MsgHeadLen);
		msg.SerializeToArray(buf + Proto::Core::MsgLenFieldSize + Proto::Core::MsgHeadLen, bodylen);

		return allsize;
	}
}
