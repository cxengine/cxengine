#include "core/event/tcp_server.h"
#include "core/event/tcp_session.h"
#include "core/event/event_loop.h"
#include "core/common/types.h"
#include "core/common/rand.h"
#include "core/common/network.h"
#include "core/common/except.h"
#include "server/server_app.h"
#include "server/channel.h"
#include "proto/ss/ss_def.pb.h"
#include "proto/ss/ss_session.pb.h"

namespace Server {
//	Proto::Core::ServerInfo ServerInfo::g_serverInfo;

	ServerApp::ServerApp(int type)
		:_nodeClient(nullptr)
		,_nodeDis(nullptr)
	{
		ServerDataMgr::Instance()->SetMyServerID(Common::GetRankU64());
		ServerData &data = ServerDataMgr::Instance()->GetMyServerData();
		data.info.set_type(type);
		printf("[LOG] server id: %lld\n", data.info.server_id());
		printf("[LOG] server type: %d\n", type);
	}

	ServerApp::~ServerApp()
	{
	}

	/*
	*��ʼ��ServerApp
	*/
	bool ServerApp::InitApp()
	{
		ServerData &data = ServerDataMgr::Instance()->GetMyServerData();
		CXEngine::App::AppConfig &config = GetAppConfig();

		data.info.set_name(config.GetStrValue("server_name"));
		printf("[LOG] server name: %s\n", data.info.name().c_str());

		std::string nodeIp = config.GetStrValue("node_server.ip");
		Common::uint16_t nodePort = (Common::uint16_t)config.GetIntValue("node_server.port");
		if (!ConnectNodeServer(nodeIp, nodePort))
			return false;

		if (!InitServer())
			return false;

		return true;
	}

	/*
	*Connect Node Server
	*/
	bool ServerApp::ConnectNodeServer(std::string &ip, Common::uint16_t port)
	{
		ServerData &data = ServerDataMgr::Instance()->GetMyServerData();
		if (data.info.type() == Proto::Core::STMgr) return true;

		_nodeClient = new CXEngine::Event::TcpClient(GetEventLoop());
		_nodeDis = new DataDispatcher();
		_nodeDis->AddActiveHandler(BIND_CALLBACK_1(ServerApp::NodeActiveCallback, this));
		_nodeDis->AddInactiveHandler(BIND_CALLBACK_1(ServerApp::NodeInactiveCallback, this));

		try {
			_nodeClient->ConnectServer(ip, port, _nodeDis);
		}catch(Common::NormalExcept e){
			delete _nodeClient; delete _nodeDis;
			printf("[ERROR] connect node server error, %s\n", e.ToString().c_str());
			return false;
		}

		printf("[LOG] connect node server %s:%d sucess\n", ip.c_str(), port);
		return true;
	}

	/*
	*Node connection active
	*/
	bool ServerApp::NodeActiveCallback(const DisParam& dp)
	{
		Proto::Core::MsgBody &msg = dp.channel->MutableMsgBody();
		Proto::SS::ServerAddReq *req = msg.MutableExtension(Proto::SS::server_add_req);
		Proto::Core::ServerInfo *newInfo = req->mutable_info();

		ServerData &data = ServerDataMgr::Instance()->GetMyServerData();
		newInfo->CopyFrom(data.info);
		dp.channel->SendMsg(Proto::SS::CTSession, Proto::SS::CCSServerAddReq);

		return true;
	}

	/*
	*Node connection inactive
	*/
	bool ServerApp::NodeInactiveCallback(const DisParam& dp)
	{
		return true;
	}
}