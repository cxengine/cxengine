#ifndef __CXENGINE_SERVER_CHANNEL_H__
#define __CXENGINE_SERVER_CHANNEL_H__
#include "core/event/tcp_session.h"
#include "core/common/types.h"
#include "server/param_def.h"
#include "proto/core/msg.pb.h"


namespace Server {
	class Channel
	{
	public:
		Channel(CXEngine::Event::TcpSession *session);
		~Channel();

		Proto::Core::MsgBody &MutableMsgBody();

		void SendMsg(short type, short code);
		void SendMsg(short type, short code, int errorcode);
		void SendMsg(short type, short code, Proto::Core::MsgBody &msg);
		void SendMsg(const char *buf, int len);

		/*
		* |....4 byte....|....20 byte....|....n byte....|
		*    data size       head data      body data
		*/
		int SerializeMsg(Proto::Core::MsgHead &head, Proto::Core::MsgBody &msg, char *buf);
	private:
		void Clear();

	private:
		CXEngine::Event::TcpSession *_session;

		bool _clear;
		Proto::Core::MsgBody _msgBody;
		char buf[SESSIONBUFSIZE];
	};
}
#endif

