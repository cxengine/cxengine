#ifndef __CXENGINE_SERVER_PARAMDEF_H__
#define __CXENGINE_SERVER_PARAMDEF_H__
#include "core/event/time_heap.h"
#include "core/app/app_base.h"

namespace Server {
	class Channel;
	struct MsgRef {
		MsgRef():msg(nullptr),len(0){}
		char *msg;
		int len;
	};

	class DisParam {
    public:
		DisParam():type(0), code(0), channel(nullptr), session(nullptr){}
		short type;
		short code;
		Channel *channel;
		CXEngine::Event::TcpSession *session;
		struct MsgRef mr;
	};
}
#endif
