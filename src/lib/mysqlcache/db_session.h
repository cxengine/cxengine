#ifndef __MYSQLCACHE_DBSESSION_H__
#define __MYSQLCACHE_DBSESSION_H__
#include "core/common/types.h"
#include "core/common/lru_cache.h"
#include "google/protobuf/message.h"
#include "proto/ss/ss_cache.pb.h"
#include "mysqlcache/cache_def.h"
#include "mysqlcache/mysql_meta.h"
#include "mysqlcache/mysql_client.h"
#include "mysqlcache/proto_cache.h"

namespace MysqlCache
{
	class CacheServer;
	class DbSession
	{
	public:
		DbSession(CacheServer *cacheServer);
		~DbSession();

		bool Init(const CacheParam &cacheP, const std::string &metapath);

		bool GetCache(const Proto::SS::GetReq &req, std::vector<google::protobuf::Message *> &msgVec);
		bool SetCache(const Proto::SS::SetReq &req);

	private:
		bool Select(MessageMeta* msgMeta, std::vector<google::protobuf::Message*>& vecMessage);
		bool Modify(MessageMeta* msgMeta);

	private:
		MysqlClient *_sqlClient;
		MysqlMeta *_sqlMeta;
		CacheServer *_cacheServer;
		std::unordered_map<std::string, ProtoCache *> _dbCache;
	};
}
#endif
