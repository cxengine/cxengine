#ifndef __MYSQLCACHE_MYSQLCLIENT_H__
#define __MYSQLCACHE_MYSQLCLIENT_H__

#include <vector>
#include <string>
namespace MysqlCache
{
	struct MYSQL_ROW_FIELD
	{
		std::vector<std::string> data;
	};

	struct MYSQL_TABLE_ROW
	{
		std::string		table_name;
		size_t			fields_count;
		std::vector<std::string> fields_names;
		size_t			row_total;
		std::vector<MYSQL_ROW_FIELD> row_data;
		MYSQL_TABLE_ROW() : fields_count(0), row_total(0)
		{
		}
	};

	struct MYSQL_CONN_CONF
	{
		std::string		ip;
		std::string		uname;
		std::string		passwd;
		std::string		dbname;
		std::string		unisock;
		std::string		char_set;
		bool			auto_commit;
		bool			auto_reconnect;
		int				port;
		int				wait_timeout;
		int				intr_timeout;
		int				ping_chkgap;
		long			cliflag;
		MYSQL_CONN_CONF()
		{
			port = cliflag = wait_timeout = intr_timeout = 0;
			ip = uname = passwd = dbname = unisock = "";
			auto_commit = auto_reconnect = true;
			char_set = "utf8";
			ping_chkgap = 10;//10s
		}
	};

	class MysqlClient
	{
	private:
		void* handle;
		std::string escaped_buffer;
		//////////////////////////////////
	public:
		MysqlClient();
		~MysqlClient();
		int Init(const MYSQL_CONN_CONF& conf);
		bool Execute(const std::string& sql, MYSQL_TABLE_ROW* table_row = nullptr);
		size_t Affects();
		int Commit();
		int Ping();
		bool Result(MYSQL_TABLE_ROW* table_row);
		int ErrNo();
		const char* ErrMsg();
		void* mysql_handle();
		bool EscapeString(std::string& result, const std::string& input);
	};
}
#endif