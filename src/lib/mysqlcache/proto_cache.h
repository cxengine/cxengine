#ifndef __MYSQLCACHE_PROTOCAHHE_H__
#define __MYSQLCACHE_PROTOCAHHE_H__
#include "core/common/types.h"
#include "core/common/lru_cache.h"
#include "google/protobuf/message.h"
#include "mysqlcache/proto_meta.h"

namespace MysqlCache
{
	typedef std::unordered_map<std::string, google::protobuf::Message *> CACHE_MESSAGE;
	class ProtoCache : public Common::LRUCache<std::string, CACHE_MESSAGE>
	{
	public:
		ProtoCache(ProtoMeta *metaProto, const std::string &name, int size = 1024);
		~ProtoCache();

		/*
		*get cache by key1 and key2
		*/
		bool GetCache(const std::string& dbKey1, const std::string& dbKey2, std::vector<google::protobuf::Message*>& vecMessage);

		/*
		*add message to cache
		*/
		bool AddCache(const std::string& dbKey1, std::string& dbKey2, google::protobuf::Message* msg);


	private:
		google::protobuf::Message *NewMessage();
		virtual bool CanDelete(CACHE_MESSAGE* data);
		virtual void DeleteNodeData(CACHE_MESSAGE *data);

		/*
		*Copy Data
		*partial copying message
		*/
		virtual void CopyData(google::protobuf::Message *to_msg, google::protobuf::Message *from_msg);
		bool CopyMsgRepeatedFieldToOther(google::protobuf::Message& from_msg, const google::protobuf::FieldDescriptor * from_field,
			google::protobuf::Message& to_msg, const google::protobuf::FieldDescriptor * to_field);
		bool CopyMsgNormalFieldToOther(google::protobuf::Message& to_msg, const google::protobuf::FieldDescriptor * to_field,
			google::protobuf::Message& from_msg, const google::protobuf::FieldDescriptor * from_field);

	private:
		ProtoMeta *_protoMeta;
		std::string _msgName;
	};
}
#endif

