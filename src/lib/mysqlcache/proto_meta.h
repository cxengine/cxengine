#ifndef __MYSQLCACHE_PROTOMETA_H__
#define __MYSQLCACHE_PROTOMETA_H__
#include "core/common/types.h"
#include "core/common/lru_cache.h"
#include "google/protobuf/message.h"
#include "google/protobuf/dynamic_message.h"
#include "google/protobuf/compiler/importer.h"


namespace MysqlCache
{
	class MetaUtil
	{
	public:
		static std::string GetStructName(const google::protobuf::Descriptor * desc)
		{
			std::string sname = desc->name();
			sname += "_ST";
			return sname;
		}

		// 获取自定义参数的值
		template<class DESC>
		static int32_t GetEnumValue(const char* name_or_number, DESC desc_)
		{
			if (!name_or_number || !name_or_number[0])
			{
				return 0;
			}
			// 数字开头则按数字转换
			if (name_or_number[0] >= '0' && name_or_number[0] <= '9')
			{
				return atoi(name_or_number);
			}
			// 其他开头则按枚举名在文件中查找
			auto f_desc = desc_->file();
			auto ev_desc = f_desc->FindEnumValueByName(name_or_number);
			if (ev_desc)
			{
				return ev_desc->number();
			}
			return 0;
		}
	};
	class FieldMeta
	{
	public:
		int AttachDesc(const google::protobuf::FieldDescriptor * fd);
		std::string GetScalarTypeName();
		std::string GetTypeName();
		std::string GetVarName();
		std::string GetScalarConvToMeth(const char* convtomsg_, const std::string& st_var_name, const std::string& msg_var_name);
		std::string GetScalarConvFromMeth(const char* convtomsg_, const std::string& st_var_name, const std::string& msg_var_name);
		std::string GetMysqlFieldType();
	//	void GetMetaField(Herm::CC::MetaField& metaField);
	public:
		const google::protobuf::FieldDescriptor * _fieldDesc;
		std::string f_count;
		std::string f_length;
		std::string f_cn;
		std::string f_desc;
		int32_t _z_length;
		int32_t _z_count;
	};
//////////////////////////////////////////////////////////////////////////////////////////////////////
	class MessageMeta
	{
	public:
		int AttachDesc(google::protobuf::Message* msg);
		void construct();
	private:
		// 解析Message中对应的DB列
		int ParseSubFields();
		// 解析Message中对应的DB主键
		bool ParsePKS();
	public:
		std::string m_pks;
		std::string m_divkey;
		std::string m_cn;
		std::string m_desc;
		std::string m_relchk;
		std::string m_autoinc;
		std::vector<FieldMeta*> _pks_fields; //if no , then all
		std::vector<FieldMeta> _sub_fields;
		std::vector<std::string> _pks_name;
		google::protobuf::Message *_msg;
	};

//////////////////////////////////////////////////////////////////////////////////////////////////////
	struct ProtoMetaErrorCollector : google::protobuf::compiler::MultiFileErrorCollector
	{
		void AddError(const std::string & filename, int line, int column, const std::string & message){
			std::cout << "file name[" << filename << "]\n\tline[" << line << "], column[" << column << "]\n\terror:[" << message << "]" << std::endl;
		}
	};
	class ProtoMeta
	{
	public:
		ProtoMeta();
		~ProtoMeta();
		/*
		*Init MetaProto by proto paths
		*/
		void Init(const std::vector<std::string> &pathVec);

		/*
		*Load proto file
		*/
		const google::protobuf::FileDescriptor * LoadFile(const std::string &protofile);

		/*
		*get message descriptro by message name
		*/
		const google::protobuf::Descriptor* GetMsgDesc(const std::string &msgname);

		/*
		*get messagemeta by message full name
		*/
		MessageMeta *GetMessageMeta(std::string msg_name);

		/*
		*new message by message name
		*if buffer is not null, parse message by buffer data
		*/
		google::protobuf::Message* NewDynMessage(const std::string &msgname, const char* buffer = nullptr, size_t buffer_len = 0);

		/*
		*free message
		*/
		void FreeDynMessage(google::protobuf::Message* pMsg);

	private:
		google::protobuf::compiler::DiskSourceTree* _diskSrcTree;
		ProtoMetaErrorCollector* _mfec;
		google::protobuf::compiler::Importer* _importer;
		google::protobuf::DynamicMessageFactory* _dynMsgFactory;

		std::unordered_map<std::string, MessageMeta *> _msgMetaMap;

		std::vector<std::string> _protofiles;
	};
}
#endif


