#include "mysql_meta.h"
#include "google/protobuf/wire_format.h"
#include "google/protobuf/wire_format_lite_inl.h"
#include "google/protobuf/io/coded_stream.h"
#include "google/protobuf/io/zero_copy_stream_impl.h"


namespace MysqlCache
{
	// 判断Message是否本文件中其他Message引用
	bool FindMessgeInAllField(const std::string& messageType, const google::protobuf::FileDescriptor* fileDsp)
	{
		if (nullptr == fileDsp)
			return false;

		// 遍历文件中的所有Message
		for (int msgIndex = 0; msgIndex < fileDsp->message_type_count(); msgIndex++){
			const google::protobuf::Descriptor* msgDsp = fileDsp->message_type(msgIndex);
			if (nullptr == msgDsp || msgDsp->name().compare(messageType))
				continue;
			
			for (int filedIndex = 0; filedIndex < msgDsp->field_count(); ++filedIndex){
				// 遍历Message中的所有Field
				const google::protobuf::FieldDescriptor* fieldDsp = msgDsp->field(filedIndex);
				if (nullptr == fieldDsp)
					continue;

				if (fieldDsp->message_type() && fieldDsp->message_type()->name().compare(messageType) == 0)
					return true;
			}
		}
		return false;
	}


	MysqlMeta::MysqlMeta(MysqlClient *sqlClient)
		:_protoMeta(nullptr)
		,_slqClient(sqlClient)
	{
	}


	MysqlMeta::~MysqlMeta()
	{
	}

	bool MysqlMeta::Init(MysqlClient *slqclient, const std::vector<std::string> &protoVec, const std::string &protofile)
	{
		if (_protoMeta) return true;

		if (!slqclient) {
			printf("[ERROR] MysqlClient is null\n");
			return false;
		}

		_protoMeta = new ProtoMeta;
		_protoMeta->Init(protoVec);
		_fileDes = _protoMeta->LoadFile(protofile);
		if (!_fileDes) {
			printf("[ERROR] Load protofile error\n");
			return false;
		}
	}

	/*
	*获取protofile中所有Message全字
	*/
	void MysqlMeta::GetMessageNames(std::vector<std::string> &msgNames)
	{
		if (!_fileDes) return;
		for (int i = 0; i < _fileDes->message_type_count(); i++) {
			const google::protobuf::Descriptor *msgDes = _fileDes->message_type(i);
			if (!msgDes || FindMessgeInAllField(msgDes->name(), _fileDes))
				continue;
			msgNames.push_back(msgDes->full_name());
		}
	}

	/*
	*获取ProtoMesta
	*/
	ProtoMeta *MysqlMeta::GetProtoMeta()
	{
		return _protoMeta;
	}

	/*
	*get new message copy
	*/
	google::protobuf::Message* MysqlMeta::GetMsgCopy(google::protobuf::Message* message)
	{
		if (nullptr == message)
		{
			return nullptr;
		}
		std::string msg_type = message->GetDescriptor()->full_name();
		google::protobuf::Message* new_message = _protoMeta->NewDynMessage(msg_type.c_str());
		if (nullptr == new_message)
		{
			return nullptr;
		}
		new_message->CopyFrom(*message);
		return new_message;
	}

	/*
	*get message meta and parse message
	*/
	MessageMeta* MysqlMeta::GetMsgMetaFromString(const std::string& msg_type, const std::string& msg_buffer)
	{
		// 获取消息元数据
		MessageMeta* message_meta = _protoMeta->GetMessageMeta(msg_type);
		if (nullptr == message_meta || nullptr == message_meta->_msg)
			return false;

		google::protobuf::Message* metaMessage = message_meta->_msg;
		const google::protobuf::Descriptor* msg_desc = metaMessage->GetDescriptor();
		if (nullptr == msg_desc)
			return false;

		// 解析消息
		if (!metaMessage->ParsePartialFromString(msg_buffer))
		{
			return false;
		}
		return message_meta;
	}

	/*
	*get message key value
	*/
	bool MysqlMeta::GetKeyVal(MessageMeta *msgMeta, google::protobuf::Message* msg, std::string &dbKey1, std::string &dbKey2)
	{
		if (!msgMeta || msgMeta->_pks_name.size() < 1) return false;

		std::string key = msgMeta->_pks_name[0];
		const google::protobuf::Descriptor* msg_desc = msg->GetDescriptor();
		if (!msg_desc) return false;


		const google::protobuf::FieldDescriptor* field = msg_desc->FindFieldByName(msgMeta->_pks_name[0]);
		if (!field) return false;
		if (!SerializeMsgNormalFieldToString(*msg, field, dbKey1))
			return false;

		if (msgMeta->_pks_name.size() < 2) dbKey2 = "";
		else {
			const google::protobuf::FieldDescriptor* field = msg_desc->FindFieldByName(msgMeta->_pks_name[1]);
			if (!field) return false;
			if (!SerializeMsgNormalFieldToString(*msg, field, dbKey2))
				return false;
		}

		return true;
	}

	/*
	*serialize proto normal field to string
	*/
	bool MysqlMeta::SerializeMsgNormalFieldToString(const google::protobuf::Message& msg,
		const google::protobuf::FieldDescriptor* pField, std::string& outString)
	{
		const google::protobuf::Reflection * pReflection = msg.GetReflection();
		if (nullptr == pReflection || nullptr == pField)
			return false;

		if (pField->is_repeated() || !pReflection->HasField(msg, pField))
			return false;

		std::stringstream strStrem;
		std::string tmpString;
		bool need_escape = false;
		switch (pField->cpp_type())
		{
		case google::protobuf::FieldDescriptor::CPPTYPE_FLOAT:
		{
			strStrem << pReflection->GetFloat(msg, pField);
			strStrem >> tmpString;
		}
		break;
		case google::protobuf::FieldDescriptor::CPPTYPE_DOUBLE:
		{
			strStrem << pReflection->GetDouble(msg, pField);
			strStrem >> tmpString;
		}
		break;
		case google::protobuf::FieldDescriptor::CPPTYPE_INT32:
		{
			strStrem << pReflection->GetInt32(msg, pField);
			strStrem >> tmpString;
		}
		break;
		case google::protobuf::FieldDescriptor::CPPTYPE_INT64:
		{
			strStrem << pReflection->GetInt64(msg, pField);
			strStrem >> tmpString;
		}
		break;
		case google::protobuf::FieldDescriptor::CPPTYPE_UINT32:
		{
			strStrem << pReflection->GetUInt32(msg, pField);
			strStrem >> tmpString;
		}
		break;
		case google::protobuf::FieldDescriptor::CPPTYPE_UINT64:
		{
			strStrem << pReflection->GetUInt64(msg, pField);
			strStrem >> tmpString;
		}
		break;
		case google::protobuf::FieldDescriptor::CPPTYPE_ENUM:
		{
			strStrem << pReflection->GetEnum(msg, pField)->number();
			strStrem >> tmpString;
		}
		break;
		case google::protobuf::FieldDescriptor::CPPTYPE_BOOL:
		{
			strStrem << (pReflection->GetBool(msg, pField) ? 1 : 0);
			strStrem >> tmpString;
		}
		break;
		case google::protobuf::FieldDescriptor::CPPTYPE_STRING:
		{
			strStrem << pReflection->GetString(msg, pField);
			strStrem >> tmpString;
			need_escape = true;
		}
		break;
		case google::protobuf::FieldDescriptor::CPPTYPE_MESSAGE:
		{
			const google::protobuf::Message & _tmpmsg = pReflection->GetMessage(msg, pField);
			_tmpmsg.SerializePartialToString(&tmpString);
			need_escape = true;
		}
		break;
		default:
		{
			printf("[ERROR] Message Normal Filed Can't Serialize! Field:%s Type:%d\n", pField->name().c_str(), pField->cpp_type());
			return false;
		}
		break;
		}
		//need escape
		if (need_escape)
		{
			outString.append("\'");
			if (!_slqClient->EscapeString(outString, tmpString))
			{
				return false;
			}
			outString.append("\'");
		}
		else
		{
			outString.append(tmpString);
		}
		return true;
	}

	/*
	*serialize proto repeated field to string
	*/
	bool MysqlMeta::SerializeMsgRepeatedFieldToString(google::protobuf::Message& msg,
		const google::protobuf::FieldDescriptor* pField, std::string& outString)
	{
		if (nullptr == pField)
		{
			return false;
		}
		const google::protobuf::Reflection* pReflection = msg.GetReflection();
		if (nullptr == pReflection)
		{
			return false;
		}
		int repeatedCount = pReflection->FieldSize(msg, pField);
		if (pField->is_repeated() && 0 == repeatedCount)
		{
			return false;
		}
		std::string tmpString;
		tmpString.resize(msg.ByteSize());
		Common::uint8_t* begin = reinterpret_cast<Common::uint8_t*>(tmpString.empty() ? NULL : &*tmpString.begin());
		if (nullptr == begin)
		{
			return false;
		}
		google::protobuf::FieldDescriptor::CppType cppType = pField->cpp_type();
		for (int index = 0; index < repeatedCount; index++)
		{
			switch (cppType)
			{
			case google::protobuf::FieldDescriptor::CPPTYPE_FLOAT:
			{
				begin = google::protobuf::internal::WireFormatLite::WriteFloatToArray(pField->number(),
					pReflection->GetRepeatedFloat(msg, pField, index),
					begin);
			}
			break;
			case google::protobuf::FieldDescriptor::CPPTYPE_DOUBLE:
			{
				begin = google::protobuf::internal::WireFormatLite::WriteDoubleToArray(pField->number(),
					pReflection->GetRepeatedDouble(msg, pField, index),
					begin);
			}
			break;
			case google::protobuf::FieldDescriptor::CPPTYPE_INT32:
			{
				begin = google::protobuf::internal::WireFormatLite::WriteInt32ToArray(pField->number(),
					pReflection->GetRepeatedInt32(msg, pField, index),
					begin);
			}
			break;
			case google::protobuf::FieldDescriptor::CPPTYPE_INT64:
			{
				begin = google::protobuf::internal::WireFormatLite::WriteInt64ToArray(pField->number(),
					pReflection->GetRepeatedInt64(msg, pField, index),
					begin);
			}
			break;
			case google::protobuf::FieldDescriptor::CPPTYPE_UINT32:
			{
				begin = google::protobuf::internal::WireFormatLite::WriteUInt32ToArray(pField->number(),
					pReflection->GetRepeatedUInt32(msg, pField, index),
					begin);
			}
			break;
			case google::protobuf::FieldDescriptor::CPPTYPE_UINT64:
			{
				begin = google::protobuf::internal::WireFormatLite::WriteUInt64ToArray(pField->number(),
					pReflection->GetRepeatedUInt64(msg, pField, index),
					begin);
			}
			break;
			case google::protobuf::FieldDescriptor::CPPTYPE_ENUM:
			{
				begin = google::protobuf::internal::WireFormatLite::WriteEnumToArray(pField->number(),
					pReflection->GetRepeatedEnum(msg, pField, index)->number(),
					begin);
			}
			break;
			case google::protobuf::FieldDescriptor::CPPTYPE_BOOL:
			{
				begin = google::protobuf::internal::WireFormatLite::WriteBoolToArray(pField->number(),
					pReflection->GetRepeatedBool(msg, pField, index),
					begin);
			}
			break;
			case google::protobuf::FieldDescriptor::CPPTYPE_STRING:
			{
				google::protobuf::internal::WireFormat::VerifyUTF8String(
					pReflection->GetRepeatedString(msg, pField, index).data(),
					pReflection->GetRepeatedString(msg, pField, index).length(),
					google::protobuf::internal::WireFormat::SERIALIZE);
				begin = google::protobuf::internal::WireFormatLite::WriteStringToArray(pField->number(),
					pReflection->GetRepeatedString(msg, pField, index),
					begin);
			}
			break;
			case google::protobuf::FieldDescriptor::CPPTYPE_MESSAGE:
			{
				auto tmpMsg = pReflection->MutableRepeatedMessage(&msg, pField, index);
				int len = tmpMsg->ByteSize();
				begin = google::protobuf::internal::WireFormatLite::WriteMessageToArray(pField->number(),
					*tmpMsg,
					begin);
			}
			break;
			default:
				printf("[ERROR] Message Repeated Filed Can't Serialize! Field:%s Type:%d\n", pField->name().c_str(), pField->cpp_type());
				return false;
			}
		}
		outString.append("\'");
		if (!_slqClient->EscapeString(outString, tmpString))
		{
			return false;
		}
		outString.append("\'");
		return true;
	}

	/*
	*create select sql
	*/
	bool MysqlMeta::CreateSelectSql(MessageMeta* message_meta, std::string & sql)
	{
		// 获取消息元数据
		if (nullptr == message_meta || nullptr == message_meta->_msg)
			return false;

		google::protobuf::Message* metaMessage = message_meta->_msg;
		const google::protobuf::Descriptor* msg_desc = metaMessage->GetDescriptor();
		if (nullptr == msg_desc)
			return false;

		sql.append("SELECT * FROM `");
		sql.append(msg_desc->name());
		sql.append("` WHERE ");

		std::vector<std::pair<std::string, std::string>> kvlist;
		bool ret = this->SerializeMsgToKeyValueVector(message_meta, kvlist);
		if (!ret)
			return false;

		bool is_first = true;
		for (auto &kv : kvlist){
			bool is_pk = false;
			for (auto &pk : message_meta->_pks_name){
				if (kv.first.compare(pk) == 0){
					is_pk = true;
					break;
				}
			}
			//if (!is_pk) continue; // 只允许按主键查询
			if (!is_first) sql.append(" AND ");
			is_first = false;
			sql.append("`");
			sql.append(kv.first);
			sql.append("` = ");
			sql.append(kv.second);
		}
		sql.append(";");
		return true;
	}

	/*
	*create inserver and update sql
	*/
	bool MysqlMeta::CreateInsertAndUpdateSql(MessageMeta* message_meta, std::string & sql)
	{
		if (nullptr == message_meta || nullptr == message_meta->_msg)
		{
			return false;
		}
		google::protobuf::Message* metaMessage = message_meta->_msg;
		const google::protobuf::Descriptor* msg_desc = metaMessage->GetDescriptor();
		if (nullptr == msg_desc)
		{
			return false;
		}

		std::vector<std::pair<std::string, std::string>> kvlist;
		bool ret = this->SerializeMsgToKeyValueVector(message_meta, kvlist);
		if (!ret)
		{
			return false;
		}

		sql.append("INSERT INTO `");
		sql.append(msg_desc->name());
		sql.append("` (");
		for (int i = 0; i < (int)kvlist.size(); ++i)
		{
			if (i != 0)
			{
				sql.append(",");
			}
			sql.append("`");
			sql.append(kvlist[i].first);
			sql.append("`");
		}
		if (!message_meta->m_autoinc.empty())
		{
			if (!kvlist.empty())
			{
				sql.append(",");
			}
			sql.append("`");
			sql.append(message_meta->m_autoinc);
			sql.append("`");
		}
		sql.append(") VALUES (");
		//VALUES(vs)
		for (int i = 0; i < (int)kvlist.size(); ++i)
		{
			if (0 != i)
			{
				sql.append(",");
			}
			sql.append(kvlist[i].second);
		}
		if (!message_meta->m_autoinc.empty())
		{
			sql.append(",0");
		}
		sql.append(") ON DUPLICATE KEY UPDATE ");

		int is_first = 0;
		int is_pk = 0;
		for (auto & kv : kvlist)
		{
			is_pk = 0;
			for (int j = 0; j < (int)message_meta->_pks_name.size(); ++j)
			{
				if (kv.first == message_meta->_pks_name[j])
				{
					is_pk = true;
					break;
				}
			}
			if (is_pk || kv.first == message_meta->m_autoinc)
			{
				continue;
			}
			if (is_first != 0)
			{
				sql.append(" , ");
			}
			is_first = 1;

			sql.append('`' + kv.first + '`');
			sql.append(" = ");
			sql.append(kv.second);
		}
		if (!message_meta->m_autoinc.empty())
		{
			if (is_first != 0)
			{
				sql.append(" , ");
			}
			sql.append('`' + message_meta->m_autoinc + '`');
			sql.append(" = ");
			sql.append(message_meta->m_autoinc);
			sql.append("+1");
		}
		sql.append(";");
		return true;
	}

	/*
	* serialize message to (key-value) vector
	*/
	bool MysqlMeta::SerializeMsgToKeyValueVector(MessageMeta* message_meta, std::vector<std::pair<std::string, std::string>>& values)
	{
		values.clear();
		if (nullptr == message_meta || nullptr == _slqClient)
			return false;

		google::protobuf::Message* metaMessage = message_meta->_msg;
		if (nullptr == metaMessage)
			return false;

		const google::protobuf::Descriptor* msg_desc = metaMessage->GetDescriptor();
		if (nullptr == msg_desc)
			return false;

		for (int i = 0; i < msg_desc->field_count(); ++i)
		{
			const google::protobuf::FieldDescriptor * pField = msg_desc->field(i);
			if (!pField->is_repeated() && !metaMessage->GetReflection()->HasField(*metaMessage, pField))
			{
				// 非repeated字段如果没赋值则无法获取
				continue;
			}
			if (pField->is_repeated() && 0 == metaMessage->GetReflection()->FieldSize(*metaMessage, pField))
			{
				// repeated字段如果数量为0则无法获取
				continue;
			}
			std::pair<std::string, std::string> kv;
			kv.first = pField->name();
			if (pField->is_repeated())
			{
				// 处理repeated字段
				if (!SerializeMsgRepeatedFieldToString(*metaMessage, pField, kv.second))
				{
					printf("[ERROR] get field kv error ! field name:%s Type:%s\n", pField->name().c_str(), metaMessage->GetTypeName().c_str());
					return false;
				}
			}
			else
			{
				// 处理非repeated字段
				if (!SerializeMsgNormalFieldToString(*metaMessage, pField, kv.second))
				{
					printf("[ERROR] get field kv error ! field name:%s Type:%s\n", pField->name().c_str(), metaMessage->GetTypeName().c_str());
					return false;
				}
			}
			values.push_back(kv);
		}
		return true;
	}


	/*
	*Parse proto Message from mysql return data
	*/
	bool MysqlMeta::ParseMsgFromDbArray(MessageMeta* message_meta, const MYSQL_TABLE_ROW& table_row,
		std::vector<google::protobuf::Message*>& vecMessage)
	{
		vecMessage.clear();
		if (table_row.fields_count <= 0)
		{
			printf("[ERROR] errror number fields:%d\n", table_row.fields_count);
			return false;
		}
		if (table_row.row_total <= 0)
		{
			return true;
		}
		if (nullptr == message_meta)
		{
			return false;
		}
		google::protobuf::Message* metaMessage = message_meta->_msg;
		if (nullptr == metaMessage)
		{
			return false;
		}
		std::string msg_name = metaMessage->GetDescriptor()->name();
		std::string msg_type = metaMessage->GetDescriptor()->full_name();
		if (msg_name.compare(table_row.table_name) != 0)
		{
			printf("[ERROR] type not matched ! expect type:%s\n", msg_name.c_str());
			return false;
		}
		int fields_count = table_row.fields_count;
		for (std::vector<MYSQL_ROW_FIELD>::const_iterator itRow = table_row.row_data.begin(); itRow != table_row.row_data.end(); itRow++)
		{
			if (itRow->data.size() != fields_count)
			{
				continue;
			}
			google::protobuf::Message* newMessage = _protoMeta->NewDynMessage(msg_type.c_str());
			if (nullptr == newMessage)
			{
				// 失败时清理所有分配的内存
				std::vector<google::protobuf::Message*>::iterator iter = vecMessage.begin();
				for (; iter != vecMessage.end(); iter++)
				{
					_protoMeta->FreeDynMessage(*iter);
				}
				vecMessage.clear();
				return false;
			}
			const google::protobuf::Descriptor* msg_desc = newMessage->GetDescriptor();
			for (int i = 0; i < fields_count; i++)
			{
				const std::string key(table_row.fields_names[i]);
				const char* value = (itRow->data)[i].c_str();
				size_t value_length = (itRow->data)[i].size();
				if (0 == value || 0 == value_length)
				{
					continue;
				}
				const google::protobuf::FieldDescriptor* pField = msg_desc->FindFieldByName(key);
				if (!pField)
				{
					printf("[ERROR] not found field in meta desc key:%s msg type:%s\n", key.c_str(), msg_desc->name().c_str());
					return false;
				}
				if (pField->is_repeated())
				{
					ParseMsgRepeatedFieldFromArray(*newMessage, pField, value, value_length);
				}
				else
				{
					ParseMsgNormalFieldFromArray(*newMessage, pField, value, value_length);
				}
			}
			vecMessage.push_back(newMessage);
		}
		return true;
	}

	bool MysqlMeta::ParseMsgNormalFieldFromArray(google::protobuf::Message& msg, 
		const google::protobuf::FieldDescriptor * pField, const char * value, size_t value_length)
	{
		auto msg_desc = msg.GetDescriptor();
		const google::protobuf::Reflection * pReflection = msg.GetReflection();
		if (nullptr == pReflection)
			return false;
		if (nullptr == value || 0 == value_length)
			return false;
		if (!pField || pField->is_repeated())
			return false;

		switch (pField->cpp_type())
		{
		case google::protobuf::FieldDescriptor::CPPTYPE_FLOAT:
			pReflection->SetFloat(&msg, pField, atof(value));
			break;
		case google::protobuf::FieldDescriptor::CPPTYPE_DOUBLE:
			pReflection->SetDouble(&msg, pField, atof(value));
			break;
		case google::protobuf::FieldDescriptor::CPPTYPE_INT32:
			pReflection->SetInt32(&msg, pField, atoi(value));
			break;
		case google::protobuf::FieldDescriptor::CPPTYPE_INT64:
			pReflection->SetInt64(&msg, pField, atoll(value));
			break;
		case google::protobuf::FieldDescriptor::CPPTYPE_UINT32:
			pReflection->SetUInt32(&msg, pField, atoi(value));
			break;
		case google::protobuf::FieldDescriptor::CPPTYPE_UINT64:
			pReflection->SetUInt64(&msg, pField, atoll(value));
			break;
		case google::protobuf::FieldDescriptor::CPPTYPE_ENUM:
		{
			auto evdesc = pField->enum_type()->FindValueByNumber(atoi(value));
			if (evdesc)
			{
				pReflection->SetEnum(&msg, pField, evdesc);
			}
			else
			{
				printf("[ERROR] not found the enum value:%s field name:%s msg type:%s\n", 
					value, pField->name().c_str(), msg_desc->name().c_str());
				return false;
			}
		}
		break;
		case google::protobuf::FieldDescriptor::CPPTYPE_BOOL:
			pReflection->SetBool(&msg, pField, atoi(value) != 0 ? true : false);
			break;
		case google::protobuf::FieldDescriptor::CPPTYPE_STRING:
			pReflection->SetString(&msg, pField, std::string(value, value_length));
			break;
		case google::protobuf::FieldDescriptor::CPPTYPE_MESSAGE:
		{
			google::protobuf::Message* tmpMsg = pReflection->MutableMessage(&msg, pField);
			if (!tmpMsg)
			{
				printf("[ERROR] MutableMessage error! field name:%s msg type:%s\n", pField->name().c_str(), msg_desc->name().c_str());
				return false;
			}
			tmpMsg->ParsePartialFromArray(value, value_length);
		}
		break;
		default:
		{
			printf("[ERROR] not found cpp type:%d field name:%s msg type:%s\n", pField->cpp_type(), pField->name().c_str(), msg_desc->name().c_str());
			return false;
		}
		break;
		}
		return true;
	}

#define PARSE_REPEATED_VARINT_FIELD(PB_VAL_TYPE, WRITE_LITE_TYPE) \
if (google::protobuf::internal::WireFormatLite::GetTagWireType(tag) == google::protobuf::internal::WireFormatLite::WIRETYPE_VARINT){ \
google::protobuf::RepeatedField<PB_VAL_TYPE>* pRepeated = pReflection->MutableRepeatedField<PB_VAL_TYPE>(&msg, pField); \
PB_VAL_TYPE* addVal = pRepeated->Add(); \
DO_((google::protobuf::internal::WireFormatLite::ReadPrimitive<PB_VAL_TYPE, WRITE_LITE_TYPE>(&codedOutputi, addVal)));} \
else{goto handle_uninterpreted;}

#define PARSE_REPEATED_ENUM_FIELD() \
if (google::protobuf::internal::WireFormatLite::GetTagWireType(tag) == google::protobuf::internal::WireFormatLite::WIRETYPE_VARINT){ \
int value; \
DO_((google::protobuf::internal::WireFormatLite::ReadPrimitive<int, google::protobuf::internal::WireFormatLite::TYPE_ENUM>(&codedOutputi, &value))); \
if (pField->enum_type()->FindValueByNumber(value)) { \
google::protobuf::RepeatedField<google::protobuf::int32>* pRepeated = pReflection->MutableRepeatedField<google::protobuf::int32>(&msg, pField); \
google::protobuf::int32* addEnum = pRepeated->Add(); \
*addEnum = value;} \
else { goto handle_uninterpreted; }} \
else { goto handle_uninterpreted; }

#define PARSE_REPEATED_STRING_FIELD() \
if (google::protobuf::internal::WireFormatLite::GetTagWireType(tag) == google::protobuf::internal::WireFormatLite::WIRETYPE_LENGTH_DELIMITED){ \
google::protobuf::RepeatedPtrField<std::string>* pRepeated = pReflection->MutableRepeatedPtrField<std::string>(&msg, pField); \
std::string* addString = pRepeated->Add(); \
DO_(google::protobuf::internal::WireFormatLite::ReadString(&codedOutputi, addString)); \
google::protobuf::internal::WireFormat::VerifyUTF8String(addString->data(), addString->length(), google::protobuf::internal::WireFormat::PARSE);} \
else{goto handle_uninterpreted;}

#define PARSE_REPEATED_MESSAGE_FIELD() \
if (google::protobuf::internal::WireFormatLite::GetTagWireType(tag) == google::protobuf::internal::WireFormatLite::WIRETYPE_LENGTH_DELIMITED){ \
google::protobuf::Message* addMsg = pReflection->AddMessage(&msg, pField); \
DO_(google::protobuf::internal::WireFormatLite::ReadMessageNoVirtual(&codedOutputi, addMsg));} \
else{goto handle_uninterpreted;}

	/*
	*parse proto repeated field from string
	*/
	bool MysqlMeta::ParseMsgRepeatedFieldFromArray(google::protobuf::Message& msg,
		const google::protobuf::FieldDescriptor * pField,const char * value, size_t value_length)
	{
#define DO_(EXPRESSION) if (!(EXPRESSION)) return false;
		const google::protobuf::Reflection* pReflection = msg.GetReflection();
		if (nullptr == pReflection)
			return false;

		if (nullptr == value || 0 == value_length)
			return false;

		if (!pField || !pField->is_repeated())
		{
			return false;
		}
		std::stringstream strStream(std::string(value, value_length));
		google::protobuf::io::IstreamInputStream istreamInput(&strStream);
		google::protobuf::io::CodedInputStream codedOutputi(&istreamInput);
		google::protobuf::uint32 tag = 0;
		while ((tag = codedOutputi.ReadTag()) != 0)
		{
			switch (pField->cpp_type())
			{
			case google::protobuf::FieldDescriptor::CPPTYPE_FLOAT:
				PARSE_REPEATED_VARINT_FIELD(float, google::protobuf::internal::WireFormatLite::TYPE_FLOAT);
				break;
			case google::protobuf::FieldDescriptor::CPPTYPE_DOUBLE:
				PARSE_REPEATED_VARINT_FIELD(double, google::protobuf::internal::WireFormatLite::TYPE_DOUBLE);
				break;
			case google::protobuf::FieldDescriptor::CPPTYPE_INT32:
				PARSE_REPEATED_VARINT_FIELD(google::protobuf::int32, google::protobuf::internal::WireFormatLite::TYPE_INT32);
				break;
			case google::protobuf::FieldDescriptor::CPPTYPE_INT64:
				PARSE_REPEATED_VARINT_FIELD(google::protobuf::int64, google::protobuf::internal::WireFormatLite::TYPE_INT64);
				break;
			case google::protobuf::FieldDescriptor::CPPTYPE_UINT32:
				PARSE_REPEATED_VARINT_FIELD(google::protobuf::uint32, google::protobuf::internal::WireFormatLite::TYPE_UINT32);
				break;
			case google::protobuf::FieldDescriptor::CPPTYPE_UINT64:
				PARSE_REPEATED_VARINT_FIELD(google::protobuf::uint64, google::protobuf::internal::WireFormatLite::TYPE_UINT64);
				break;
			case google::protobuf::FieldDescriptor::CPPTYPE_ENUM:
				PARSE_REPEATED_ENUM_FIELD();
				break;
			case google::protobuf::FieldDescriptor::CPPTYPE_BOOL:
				PARSE_REPEATED_VARINT_FIELD(bool, google::protobuf::internal::WireFormatLite::TYPE_BOOL);
				break;
			case google::protobuf::FieldDescriptor::CPPTYPE_STRING:
				PARSE_REPEATED_STRING_FIELD();
				break;
			case google::protobuf::FieldDescriptor::CPPTYPE_MESSAGE:
				PARSE_REPEATED_MESSAGE_FIELD();
				break;
			default:
			{
			handle_uninterpreted:
				if (google::protobuf::internal::WireFormatLite::GetTagWireType(tag) ==
					google::protobuf::internal::WireFormatLite::WIRETYPE_END_GROUP)
				{
					printf("[ERROR] not found cpp type:%d field name:%s msg type:%s\n", pField->cpp_type(), pField->name().c_str(), msg.GetTypeName().c_str());
					return true;
				}
				DO_(google::protobuf::internal::WireFormat::SkipField(&codedOutputi, tag, pReflection->MutableUnknownFields(&msg)))
					printf("[ERROR] not found cpp type:%d field name:%s msg type:%s\n", pField->cpp_type(), pField->name().c_str(), msg.GetTypeName().c_str());
				break;
			}
			}
		}
		return true;
#undef DO_
	}

}
