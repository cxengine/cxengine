#ifndef __MYSQLCACHE_CACHEDEF_H__
#define __MYSQLCACHE_CACHEDEF_H__
#include <iostream>
#include <vector>

namespace MysqlCache
{
	struct CacheParam
	{
		int port;
		std::string ip;
		std::string user;
		std::string pwd;
		std::string dbname;
		std::string protofile;
	};

	struct ProtoParam
	{
		std::vector<std::string> paths;
		std::vector<std::string> files;
	};
}
#endif
