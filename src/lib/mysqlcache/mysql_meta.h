#ifndef __MYSQLCACHE_MYSQLMETA_H__
#define __MYSQLCACHE_MYSQLMETA_H__
#include "core/common/types.h"
#include "core/common/lru_cache.h"
#include "mysqlcache/proto_meta.h"
#include "mysqlcache/mysql_client.h"


namespace MysqlCache
{
	class MysqlMeta
	{
	public:
		MysqlMeta(MysqlClient *sqlClient);
		~MysqlMeta();
		bool Init(MysqlClient *slqclient, const std::vector<std::string> &protoVec, const std::string &protofile);

		/*
		*获取protofile中所有Message全字
		*/
		void GetMessageNames(std::vector<std::string> &msgNames);

		/*
		*获取ProtoMesta
		*/
		ProtoMeta *GetProtoMeta();

		/*
		*get new message copy
		*/
		google::protobuf::Message* GetMsgCopy(google::protobuf::Message* message);

		/*
		*get message meta and parse message
		*/
		MessageMeta* GetMsgMetaFromString(const std::string& msg_type, const std::string& msg_buffer);

		/*
		*get message key value
		*/
		bool GetKeyVal(MessageMeta *msgMeta, google::protobuf::Message* msg, std::string &dbKey1, std::string &dbKey2);

		/*
		*create select sql
		*/
		bool CreateSelectSql(MessageMeta* message_meta, std::string & sql);

		/*
		*create inserver and update sql
		*/
		bool CreateInsertAndUpdateSql(MessageMeta* message_meta, std::string & sql);

		/*
		*Parse proto Message from mysql return data
		*/
		bool ParseMsgFromDbArray(MessageMeta* message_meta, const MYSQL_TABLE_ROW& table_row,
			std::vector<google::protobuf::Message*>& vecMessage);

	private:
		/*
		*serialize proto normal field to string
		*/
		bool SerializeMsgNormalFieldToString(const google::protobuf::Message& msg,
			const google::protobuf::FieldDescriptor* pField, std::string& outString);

		/*
		*serialize proto repeated field to string
		*/
		bool SerializeMsgRepeatedFieldToString(google::protobuf::Message& msg,
			const google::protobuf::FieldDescriptor* pField, std::string& outString);

		/*
		* serialize message to (key-value) vector
		*/
		bool SerializeMsgToKeyValueVector(MessageMeta* message_meta,
			std::vector<std::pair<std::string, std::string>>& values);

		/*
		*parse proto normal field from string
		*/
		bool ParseMsgNormalFieldFromArray(google::protobuf::Message& msg,
			const google::protobuf::FieldDescriptor * pField, const char * value, size_t value_length);

		/*
		*parse proto repeated field from string
		*/
		bool ParseMsgRepeatedFieldFromArray(google::protobuf::Message& msg,
			const google::protobuf::FieldDescriptor * pField, const char * value, size_t value_length);

		
	private:
		ProtoMeta *_protoMeta;
		const google::protobuf::FileDescriptor *_fileDes;
		MysqlClient *_slqClient;
	};
}
#endif

