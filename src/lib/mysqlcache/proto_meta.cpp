#include "proto_meta.h"
#include "mysqlcache/extensions_option.h"


namespace MysqlCache
{
#define DIM_ARRAY(a)	(sizeof((a))/sizeof((a)[0]))

	template<class D>
	static inline std::string descriptor_option(D desc, const std::string& opt);
	template<>
	inline std::string descriptor_option(const google::protobuf::Descriptor * desc, const std::string & opt)
	{
		return proto_msg_opt(desc, opt.c_str());
	}
	template<>
	inline std::string descriptor_option(const google::protobuf::FieldDescriptor * desc, const std::string & opt)
	{
		return proto_field_opt(desc, opt.c_str());
	}
	template<>
	inline std::string descriptor_option(const google::protobuf::EnumValueDescriptor * desc, const std::string & opt)
	{
		return proto_enum_opt(desc, opt.c_str());
	}

	template<class D>
	int GetDescOption(std::string& value, D desc, const std::string& option)
	{
		value = descriptor_option(desc, option);
		if (!value.empty()) {
			return 0;
		}
		return -1;
	}

#define GET_DESC_STR_OPTION(opt_name, desc)	GetDescOption(opt_name, desc, #opt_name)
#define GET_DESC_INT_OPTION(opt_name, desc)	do{string _value_opt_str;GetDescOption(_value_opt_str, desc, #opt_name);opt_name = std::stoi(_value_opt_str);}while(false)
////////////////////////////////////////////////////////////////////////////////
	int FieldMeta::AttachDesc(const google::protobuf::FieldDescriptor * desc)
	{
		_fieldDesc = desc;

		GET_DESC_STR_OPTION(f_cn, _fieldDesc);
		GET_DESC_STR_OPTION(f_desc, _fieldDesc);
		GET_DESC_STR_OPTION(f_count, _fieldDesc);
		GET_DESC_STR_OPTION(f_length, _fieldDesc);

		_z_length = MetaUtil::GetEnumValue(f_length.c_str(), _fieldDesc);
		_z_count = MetaUtil::GetEnumValue(f_count.c_str(), _fieldDesc);
		if (desc->is_repeated() ||
			desc->cpp_type() == google::protobuf::FieldDescriptor::CPPTYPE_MESSAGE)
		{
			if (_z_count <= 0)
			{
				// repeated列或Message数据默认按BLOB存储
				_z_count = 0;
				return 0;
			}
		}
		if (desc->cpp_type() == google::protobuf::FieldDescriptor::CPPTYPE_STRING)
		{
			if (_z_length <= 0)
			{
				// sting或bytes类型默认按VarChar(32)存储
				_z_length = 32;
				return 0;
			}
		}
		return 0;
	}

	std::string FieldMeta::GetScalarTypeName()
	{
		static const char * s_var_type_names[] = {
			"",
			"int32_t", "int64_t", "uint32_t",
			"uint64_t", "double", "float",
			"bool", "invalidate type(enum)", "invalidate type(const char *)",
			"invalidate type(message)",""
		};
		const char * pszTypeName = s_var_type_names[_fieldDesc->cpp_type()];
		if (_fieldDesc->cpp_type() == google::protobuf::FieldDescriptor::CPPTYPE_ENUM)
		{
			return _fieldDesc->enum_type()->name();
		}
		else if (_fieldDesc->cpp_type() == google::protobuf::FieldDescriptor::CPPTYPE_MESSAGE)
		{
			return MetaUtil::GetStructName(_fieldDesc->message_type());
		}
		return pszTypeName;
	}

	std::string FieldMeta::GetTypeName()
	{
		const char * pszTypeName = GetScalarTypeName().c_str();
		if (_fieldDesc->cpp_type() == google::protobuf::FieldDescriptor::CPPTYPE_STRING)
		{
			if (_fieldDesc->type() == google::protobuf::FieldDescriptor::TYPE_STRING)
			{
				std::string buffer_type_name = "struct { char data[";
				buffer_type_name += f_length;
				buffer_type_name += "]; }";
				return buffer_type_name;
			}
			else
			{
				std::string buffer_type_name = "struct { size_t length; uint8_t data[";
				buffer_type_name += f_length;
				buffer_type_name += "]; }";
				return buffer_type_name;
			}
		}
		else
		{
			return pszTypeName;
		}
	}

	std::string FieldMeta::GetVarName()
	{
		std::string lc_name = _fieldDesc->lowercase_name();
		return lc_name;
	}

	std::string FieldMeta::GetScalarConvToMeth(const char * convtomsg_, const std::string & st_var_name, const std::string & msg_var_name)
	{
		auto fmt = _fieldDesc->message_type();
		std::string meth = "";
		std::string mutable_meth = "set_";
		if (fmt)
		{
			mutable_meth = "mutable_";
		}
		if (_fieldDesc->is_repeated())
		{
			mutable_meth = "add_";
		}
		if (fmt)
		{
			meth += st_var_name;
			meth += ".convto(*";
			meth += convtomsg_;
			meth += "." + mutable_meth;
			meth += msg_var_name;
			meth += "()";
		}
		else
		{
			meth += convtomsg_;
			meth += "." + mutable_meth;
			meth += msg_var_name;
			meth += "(";
			meth += st_var_name;
			if (_fieldDesc->type() == google::protobuf::FieldDescriptor::TYPE_STRING)
			{
				meth += ".data";
			}
			else if (_fieldDesc->type() == google::protobuf::FieldDescriptor::TYPE_BYTES)
			{
				meth += ".data,";
				meth += st_var_name;
				meth += ".length";
			}
		}
		meth += ")";
		return meth;
	}

	std::string FieldMeta::GetScalarConvFromMeth(const char * convtomsg_, const std::string & st_var_name, const std::string & msg_var_name)
	{
		auto fmt = _fieldDesc->message_type();
		std::string meth = "";
		if (fmt)
		{
			meth += st_var_name;
			meth += ".convfrom(";
			meth += convtomsg_;
			meth += ".";
			meth += msg_var_name;
			meth += ")";
		}
		else
		{
			if (_fieldDesc->type() == google::protobuf::FieldDescriptor::TYPE_STRING)
			{
				meth = "strncpy(";
				meth += st_var_name.c_str();
				meth += ".data, ";
				meth += convtomsg_;
				meth += ".";
				meth += msg_var_name;
				meth += ".data(), ";
				meth += f_length.c_str();
				meth += "-1)";
			}
			else if (_fieldDesc->type() == google::protobuf::FieldDescriptor::TYPE_BYTES)
			{
				meth = "memcpy(";
				meth += st_var_name.c_str();
				meth += ".data, ";
				meth += convtomsg_;
				meth += ".";
				meth += msg_var_name;
				meth += ".data(), std::min((size_t)";
				meth += f_length.c_str();
				meth += ", (size_t)";
				meth += convtomsg_;
				meth += ".";
				meth += msg_var_name + ".length()))";
			}
			else
			{
				meth = st_var_name;
				meth += " = ";
				meth += convtomsg_;
				meth += ".";
				meth += msg_var_name;
			}
		}
		return meth;
	}

	std::string FieldMeta::GetMysqlFieldType()
	{
		google::protobuf::FieldDescriptor::CppType cpp_type = _fieldDesc->cpp_type();
		if (_fieldDesc->is_repeated())
		{
			cpp_type = google::protobuf::FieldDescriptor::CPPTYPE_MESSAGE;
		}
		switch (cpp_type)
		{
		case google::protobuf::FieldDescriptor::CPPTYPE_INT32:// 1,     // TYPE_INT32, TYPE_SINT32, TYPE_SFIXED32
		case google::protobuf::FieldDescriptor::CPPTYPE_ENUM:// 8,     // TYPE_ENUM
			return "INT";
		case google::protobuf::FieldDescriptor::CPPTYPE_UINT32:// 3,     // TYPE_UINT32, TYPE_FIXED32
			return "INT UNSIGNED";
		case google::protobuf::FieldDescriptor::CPPTYPE_INT64:// 2,     // TYPE_INT64, TYPE_SINT64, TYPE_SFIXED64
			return "BIGINT";
		case google::protobuf::FieldDescriptor::CPPTYPE_UINT64:// 4,     // TYPE_UINT64, TYPE_FIXED64
			return "BIGINT UNSIGNED";
		case google::protobuf::FieldDescriptor::CPPTYPE_DOUBLE:// 5,     // TYPE_DOUBLE
			return "DOUBLE";
		case google::protobuf::FieldDescriptor::CPPTYPE_FLOAT:// 6,     // TYPE_FLOAT
			return "FLOAT";
		case google::protobuf::FieldDescriptor::CPPTYPE_BOOL:// 7,     // TYPE_BOOL
			return "TINYINT";
		case google::protobuf::FieldDescriptor::CPPTYPE_STRING:// 9,     // TYPE_STRING, TYPE_BYTES
			if (_z_length <= 0XFF)
			{
				char str[64] = "";
				sprintf(str, "VARCHAR(%d)", _z_length);
				std::string varcharReturn(str);
				return varcharReturn; //255
			}
			else if (_z_length <= 0XFFFF)
			{
				return "TEXT";//64K
			}
			else if (_z_length <= 0XFFFFFF)
			{
				return "MEDIUMTEXT"; //16MB
			}
			else
			{
				return "LONGTEXT";//4GB
			}
		case google::protobuf::FieldDescriptor::CPPTYPE_MESSAGE:// 10,    // TYPE_MESSAGE, TYPE_GROUP	}
			if (_z_count <= 0)
			{
				return "BLOB";	//64K
			}
			else if (_z_count <= 1)
			{
				return "MEDIUMBLOB";	//16MB
			}
			else
			{
				return "LOGNGBLOB"; //4GB
			}
		default:
			return "BLOB";
		}
	}

// 	void FieldMeta::GetMetaField(Herm::CC::MetaField& metaField)
// 	{
// 
// 	}
///////////////////////////////////////////////////////////////////////
	void MessageMeta::construct()
	{
		_pks_fields.clear();
		_sub_fields.clear();
		_pks_name.clear();
	}

	int MessageMeta::AttachDesc(google::protobuf::Message* msg)
	{
		if (nullptr == msg)
			return -1;
		_msg = msg;
		construct();

		const google::protobuf::Descriptor* msgDesc = _msg->GetDescriptor();
		GET_DESC_STR_OPTION(m_pks, msgDesc);
		GET_DESC_STR_OPTION(m_divkey, msgDesc);
		GET_DESC_STR_OPTION(m_cn, msgDesc);
		GET_DESC_STR_OPTION(m_desc, msgDesc);
		GET_DESC_STR_OPTION(m_relchk, msgDesc);
		GET_DESC_STR_OPTION(m_autoinc, msgDesc);

		if (ParseSubFields()) 
			return -1;

		if (!ParsePKS())
			return -1;

		return 0;
	}

	int MessageMeta::ParseSubFields()
	{
		if (nullptr == _msg)
			return -1;

		int ret = 0;
		const google::protobuf::Descriptor* msgDesc = _msg->GetDescriptor();
		for (int i = 0; i < msgDesc->field_count(); ++i)
		{
			FieldMeta sfm;
			ret = sfm.AttachDesc(msgDesc->field(i));
			if (ret){
				std::cout << "parse field errro in message :" << msgDesc->full_name() << std::endl;
				return ret;
			}
			_sub_fields.push_back(sfm);
		}
		return ret;
	}

	bool MessageMeta::ParsePKS()
	{
		std::string::size_type bpos = 0, fpos = 0;
		while (true && !m_pks.empty())
		{
			// m_pks中的主键用,隔开
			fpos = m_pks.find(',', bpos);
			std::string f_field_name = "";
			if (fpos != std::string::npos && fpos > bpos)
			{
				f_field_name = m_pks.substr(bpos, fpos - bpos);
			}
			else
			{
				if (fpos != bpos)
				{
					f_field_name = m_pks.substr(bpos);
				}
			}
			// 填充主键
			for (auto & suf : _sub_fields)
			{
				if (suf._fieldDesc->name() == f_field_name)
				{
					_pks_name.push_back(f_field_name);
					_pks_fields.push_back(&suf);
				}
			}
			if (fpos == std::string::npos) {
				break;
			}
			bpos = fpos + 1;
		}

#if GOOGLE_PROTOBUF_VERSION < 3000000
		// 如果m_pks中未定义主键或全无效, 则使用required列做主键
		if (_pks_fields.empty())
		{
			_pks_name.clear();
			for (auto & suf : _sub_fields)
			{
				if (suf._fieldDesc->is_required())
				{
					_pks_name.push_back(suf._fieldDesc->name());
					_pks_fields.push_back(&suf);
				}
			}
		}
#endif

		// 必须有主键 且 只支持1-2主键
		if (_pks_name.size() < 1 || _pks_name.size() > 2){
			_pks_name.clear();
			_pks_fields.clear();
			return false;
		}

		return true;
	}
//////////////////////////////////////////////////////////////////////////////////////////////////////
	ProtoMeta::ProtoMeta()
	{
		_diskSrcTree = new google::protobuf::compiler::DiskSourceTree();
		_importer = nullptr;
		_dynMsgFactory = nullptr;
	}


	ProtoMeta::~ProtoMeta()
	{
		if (_importer) delete _importer;
		if (_diskSrcTree) delete _diskSrcTree;
		if (_dynMsgFactory) delete _dynMsgFactory;
		if (_mfec) delete _mfec;
	}

	/*
	*Init MetaProto by proto paths
	*/
	void ProtoMeta::Init(const std::vector<std::string> &pathVec)
	{
		if (_importer)
			return;
		
		for (std::vector<std::string>::const_iterator iter = pathVec.begin(); iter != pathVec.end(); iter++)
		{
			_diskSrcTree->MapPath("", *iter);
		}
		_mfec = new ProtoMetaErrorCollector;
		_importer = new google::protobuf::compiler::Importer(_diskSrcTree, _mfec);
		_dynMsgFactory = new google::protobuf::DynamicMessageFactory(_importer->pool());
	}

	/*
	*Load proto file
	*/
	const google::protobuf::FileDescriptor * ProtoMeta::LoadFile(const std::string &protofile)
	{
		const google::protobuf::FileDescriptor *fdp = _importer->Import(protofile.c_str());
		if (!fdp){
			std::cerr << "error import file:" << protofile << std::endl;
			return nullptr;
		}
		_protofiles.push_back(protofile);

		for (int i = 0; i < fdp->message_type_count(); i++) {
			const google::protobuf::Descriptor* msgDesc = fdp->message_type(i);
			if(!msgDesc) continue;
			MessageMeta *msgMeta = new MessageMeta();
			msgMeta->AttachDesc(NewDynMessage(msgDesc->full_name()));
			_msgMetaMap.insert(std::make_pair(msgDesc->full_name(), msgMeta));
		}
		return fdp;
	}

	/*
	*get message descriptro by message name
	*/
	const google::protobuf::Descriptor* ProtoMeta::GetMsgDesc(const std::string &msgname)
	{
		return _importer->pool()->FindMessageTypeByName(msgname.c_str());
	}

	/*
	*get messagemeta by message type
	*/
	MessageMeta *ProtoMeta::GetMessageMeta(std::string msg_name)
	{
		auto iter = _msgMetaMap.find(msg_name);
		if (iter == _msgMetaMap.end())
			return nullptr;
		return iter->second;
	}

	/*
	*new message by message name
	*if buffer is not null, parse message by buffer data
	*/
	google::protobuf::Message* ProtoMeta::NewDynMessage(const std::string &msgname, const char* buffer, size_t buffer_len)
	{
		auto msg_desc = GetMsgDesc(msgname.c_str());
		if (!msg_desc){
			std::cerr << "get msg desc error ! msg_type:" << msgname << std::endl;
			return nullptr;
		}

		auto pProtMsg = _dynMsgFactory->GetPrototype(msg_desc);
		if (!pProtMsg){
			std::cerr << "get proto msg error ! msg_type:" << msgname << std::endl;
			return nullptr;
		}

		google::protobuf::Message * pMsg = pProtMsg->New();
		if (!pMsg) {
			std::cerr << "new msg from proto error ! msg_type:" << msgname << std::endl;
			return nullptr;
		}

		if (buffer_len == 0 || !buffer){
			return pMsg;
		}
		if (!pMsg->ParseFromArray(buffer, (int)buffer_len)){
			std::cerr << "unpack msg error !" << msgname << std::endl;
			FreeDynMessage(pMsg);
			return nullptr;
		}
		return pMsg;
	}

	/*
	*free message
	*/
	void ProtoMeta::FreeDynMessage(google::protobuf::Message* pMsg)
	{
		delete pMsg;
	}
}
