#include "core/common/string.h"
#include "mysqlcache/db_session.h"
#include "mysqlcache/cache_server.h"

namespace MysqlCache {
	DbSession::DbSession(CacheServer *cacheServer)
		:_sqlClient(nullptr)
		,_sqlMeta(nullptr)
		,_cacheServer(cacheServer)
	{}

	DbSession::~DbSession()
	{}

	bool DbSession::Init(const CacheParam &cacheP, const std::string &metapath)
	{
		if (_sqlClient) return true;

		_sqlClient = new MysqlClient;
		if (nullptr == _sqlClient){
			printf("[RRROR] DbSession::alloc mysql_client == null\n");
			return false;
		}

		MYSQL_CONN_CONF sqlconf;
		sqlconf.ip = cacheP.ip;
		sqlconf.port = cacheP.port;
		sqlconf.uname = cacheP.user;
		sqlconf.passwd = cacheP.pwd;
		sqlconf.dbname = cacheP.dbname;
		if (_sqlClient->Init(sqlconf)){
			printf("[RRROR] DbSession::Init mysql error !\n");
			return false;
		}

		std::vector<std::string> vecPaths = Common::SplitStr(metapath, ":");
		_sqlMeta = new MysqlMeta(_sqlClient);
		if (!_sqlMeta->Init(_sqlClient, vecPaths, cacheP.protofile)) {
			printf("[RRROR] DbSession::Init mysql meta error !\n");
			return false;
		}

		std::vector<std::string> msgNames;
		_sqlMeta->GetMessageNames(msgNames);
		for (int i = 0; i < msgNames.size(); i++)
		{
			ProtoCache *cache = new ProtoCache(_sqlMeta->GetProtoMeta(), msgNames[i], 1024);
			_dbCache.insert(std::make_pair(msgNames[i], cache));
			_cacheServer->AddMessageMap(msgNames[i], this);
		}
		return true;
	}

	bool DbSession::GetCache(const Proto::SS::GetReq &req, std::vector<google::protobuf::Message *> &msgVec)
	{
		const std::string &msg_name = req.type_name();
		auto iter = _dbCache.find(msg_name);
		if (iter == _dbCache.end())
			return false;
		ProtoCache *protoCache = iter->second;

		MessageMeta *msgMeta = _sqlMeta->GetMsgMetaFromString(msg_name, req.entry_msg());
		std::string dbKey1, dbKey2;
		if (!_sqlMeta->GetKeyVal(msgMeta ,msgMeta->_msg, dbKey1, dbKey2)) {
			printf("[ERROR] DbSession::GetCache:Get key error\n");
			return false;
		}

		bool ret = protoCache->GetCache(dbKey1, dbKey2, msgVec);
		if (!ret) { //data not exit in cache
			if (!Select(msgMeta, msgVec))
				return false;
			for (auto iter = msgVec.begin(); iter != msgVec.end(); ++iter) {
				_sqlMeta->GetKeyVal(msgMeta, *iter, dbKey1, dbKey2);
				protoCache->AddCache(dbKey1, dbKey2, *iter);
			}
		}

		return true;
	}

	bool DbSession::SetCache(const Proto::SS::SetReq &req)
	{
		const std::string &msg_name = req.type_name();
		auto iter = _dbCache.find(msg_name);
		if (iter == _dbCache.end())
			return false;
		ProtoCache *protoCache = iter->second;

		MessageMeta *msgMeta = _sqlMeta->GetMsgMetaFromString(msg_name, req.entry_msg());
		std::string dbKey1, dbKey2;
		if (!_sqlMeta->GetKeyVal(msgMeta, msgMeta->_msg, dbKey1, dbKey2)) {
			printf("[ERROR] DbSession::SetCache:Get key error\n");
			return false;
		}

		std::vector<google::protobuf::Message*> msgVec;
		bool ret = protoCache->GetCache(dbKey1, dbKey2, msgVec);
		if (!ret) { //data not exit in cache
			if (!Select(msgMeta, msgVec))
				return false;
			for (auto iter = msgVec.begin(); iter != msgVec.end(); ++iter) {
				_sqlMeta->GetKeyVal(msgMeta, *iter, dbKey1, dbKey2);
				protoCache->AddCache(dbKey1, dbKey2, *iter);
			}
		}

		ret = protoCache->GetCache(dbKey1, dbKey2, msgVec);
		if (ret) {
			protoCache->AddCache(dbKey1, dbKey2, msgMeta->_msg);
		}else {
			protoCache->AddCache(dbKey1, dbKey2, _sqlMeta->GetMsgCopy(msgMeta->_msg));
		}

		if (!Modify(msgMeta)) {
			printf("[ERROR] DbSession::SetCache:save database error\n");
			return false;
		}

		return true;
	}

	bool DbSession::Select(MessageMeta* msgMeta, std::vector<google::protobuf::Message*>& vecMessage)
	{
		if (nullptr == msgMeta)
			return false;

		std::string sql;
		if (!_sqlMeta->CreateSelectSql(msgMeta, sql))
			return false;

		MYSQL_TABLE_ROW table_row;
		if (!_sqlClient->Execute(sql, &table_row))
			return false;

		_sqlMeta->ParseMsgFromDbArray(msgMeta, table_row, vecMessage);

		return true;
	}

	bool DbSession::Modify(MessageMeta* msgMeta)
	{
		if (nullptr == msgMeta)
			return false;

		std::string sql;
		if (!_sqlMeta->CreateInsertAndUpdateSql(msgMeta, sql))
			return false;

		if (!_sqlClient->Execute(sql))
			return false;

		return true;
	}
}