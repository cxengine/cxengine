#include "proto/core/msg.pb.h"
#include "proto/ss/ss_def.pb.h"
#include "proto/ss/ss_cache.pb.h"
#include "server/channel.h"
#include "mysqlcache/cache_server.h"


namespace MysqlCache
{
	CacheServer::CacheServer()
		:_dis(nullptr)
	{
	}


	CacheServer::~CacheServer()
	{
	}

	bool CacheServer::InitCache(Server::DataDispatcher *dis, const std::string &protometa, const std::vector<struct CacheParam> &cacheParams)
	{
		_dis = dis;
		InitHandle(dis);

		for (int i = 0; i < cacheParams.size(); i++) {
			DbSession *session = new DbSession(this);
			_dbSessions.push_back(session);
			if (!session->Init(cacheParams[i], protometa)) {
				printf("[ERROR] DbSession init error\n");
				return false;
			}
				
		}
		return true;
	}

	/*
	*Init message handle
	*/
	void CacheServer::InitHandle(Server::DataDispatcher *dis)
	{
		dis->Register(Proto::SS::CTCache, Proto::SS::CCGetMetaReq, BIND_CALLBACK_2(CacheServer::GetMetaReq, this));
		dis->Register(Proto::SS::CTCache, Proto::SS::CCGetReq, BIND_CALLBACK_2(CacheServer::GetReq, this));
		dis->Register(Proto::SS::CTCache, Proto::SS::CCSetReq, BIND_CALLBACK_2(CacheServer::SetReq, this));
	}

	/*
	*Get proto meta req
	*/
	void CacheServer::GetMetaReq(const Proto::Core::MsgBody& msg, Server::DisParam& dp)
	{
		printf("[LOG] CacheInstance::GetMetaReq\n");
	}
	/*
	*Get date frome cache
	*/
	void CacheServer::GetReq(const Proto::Core::MsgBody& msg, Server::DisParam& dp)
	{
		const Proto::SS::GetReq &req = msg.GetExtension(Proto::SS::get_req);
		std::string msg_name = req.type_name();

		auto iter = _dbSessionMap.find(msg_name);
 		if (iter == _dbSessionMap.end()) {
 			printf("[ERROR] CacheServer::GetReq message:%s cache is not exists\n", msg_name.c_str());
			return;
		}

		std::vector<google::protobuf::Message *> msgVec;
 		DbSession *dbsession = iter->second;

		bool ret = dbsession->GetCache(req, msgVec);
		if (!ret) {
			printf("[ERROR] get cache error\n");
			return;
		}

		Proto::Core::MsgBody &res_msg = dp.channel->MutableMsgBody();
		res_msg.mutable_base()->CopyFrom(msg.base());
		Proto::SS::GetRes *res = res_msg.MutableExtension(Proto::SS::get_res);
		for (auto iter = msgVec.begin(); iter != msgVec.end(); ++iter) {
			res->add_entry_msgs((*iter)->SerializeAsString());
		}

		dp.channel->SendMsg(dp.type, dp.code);
		
	}
	/*
	*save data to cache
	*/
	void CacheServer::SetReq(const Proto::Core::MsgBody& msg, Server::DisParam& dp)
	{
		const Proto::SS::SetReq &req = msg.GetExtension(Proto::SS::set_req);
		std::string msg_name = req.type_name();

		auto iter = _dbSessionMap.find(msg_name);
		if (iter == _dbSessionMap.end()) {
			printf("[ERROR] CacheServer::GetReq message:%s cache is not exists\n", msg_name.c_str());
			return;
		}

		DbSession *dbsession = iter->second;
		dbsession->SetCache(req);
	}

	/*
	*Add Pair Message DbSession
	*/
	void CacheServer::AddMessageMap(std::string msgName, DbSession *session)
	{
		_dbSessionMap[msgName] = session;
	}
}
