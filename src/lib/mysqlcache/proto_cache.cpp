#include "mysqlcache/proto_cache.h"


namespace MysqlCache
{
	ProtoCache::ProtoCache(ProtoMeta *metaProto, const std::string &name, int size)
		:_protoMeta(metaProto)
		,_msgName(name)
		,Common::LRUCache<std::string, CACHE_MESSAGE>(size)
	{
		InitLRUCache();
	}


	ProtoCache::~ProtoCache()
	{
	}

	/*
	*get cache by key1 and key2
	*/
	bool ProtoCache::GetCache(const std::string& dbKey1, const std::string& dbKey2, std::vector<google::protobuf::Message*>& vecMessage)
	{
		CACHE_MESSAGE *cache_message = this->Get(dbKey1);
		if (cache_message == nullptr)
			return false;

		if (dbKey2.empty()) {
			for (auto iter = cache_message->begin(); iter != cache_message->end(); ++iter)
				vecMessage.push_back(iter->second);
			return true;
		}

		auto iter = cache_message->find(dbKey2);
		if (iter == cache_message->end())
			return false;

		vecMessage.push_back(iter->second);
		return true;
	}

	/*
	*add message to cache
	*/
	bool ProtoCache::AddCache(const std::string& dbKey1, std::string& dbKey2, google::protobuf::Message* msg)
	{
		CACHE_MESSAGE *cache_message = this->Get(dbKey1);
		if (cache_message == nullptr) {
			cache_message = new CACHE_MESSAGE;
			this->Put(dbKey1, cache_message);
		}
		if (cache_message == nullptr)
			return false;

		if (dbKey2.empty())
			dbKey2 = dbKey1;

		auto iter = cache_message->find(dbKey2);
		if (iter == cache_message->end()){
			cache_message->insert(make_pair(dbKey2, msg));
		}else{
			CopyData(iter->second, msg);
		}
		return true;


	}


	google::protobuf::Message *ProtoCache::NewMessage()
	{
		return _protoMeta->NewDynMessage(_msgName);
	}

	bool ProtoCache::CanDelete(CACHE_MESSAGE* data)
	{
		return true;
	}

	void ProtoCache::DeleteNodeData(CACHE_MESSAGE *data)
	{
		for (auto iter = data->begin(); iter != data->end(); ++iter) {
			delete iter->second;
		}
		delete data;
	}

	/*
	*Copy Data
	*partial copying message
	*/
	void ProtoCache::CopyData(google::protobuf::Message *to_msg, google::protobuf::Message *from_msg)
	{
		if (nullptr == to_msg || nullptr == from_msg)
			return;

		const google::protobuf::Descriptor* from_desc = from_msg->GetDescriptor();
		const google::protobuf::Descriptor* to_desc = to_msg->GetDescriptor();
		if (nullptr == from_desc || nullptr == to_desc)
			return;

		for (int i = 0; i < from_desc->field_count(); i++)
		{
			const google::protobuf::FieldDescriptor* from_field = from_desc->field(i);
			if (nullptr == from_field)
				return;

			const google::protobuf::FieldDescriptor* to_field = to_desc->FindFieldByName(from_field->name());
			if (nullptr == to_field)
				return;

			if (from_field->is_repeated())
			{
				this->CopyMsgRepeatedFieldToOther(*to_msg, to_field, *from_msg, from_field);
			}
			else
			{
				this->CopyMsgNormalFieldToOther(*to_msg, to_field, *from_msg, from_field);
			}
		}
	}


#define COPY_REPEATED_FIELD(PB_VAL_TYPE) \
google::protobuf::RepeatedField<PB_VAL_TYPE>* to_repeated_field = to_reflection->MutableRepeatedField<PB_VAL_TYPE>(&to_msg, to_field); \
const google::protobuf::RepeatedField<PB_VAL_TYPE>* from_repeated_field = from_reflection->MutableRepeatedField<PB_VAL_TYPE>(&from_msg, from_field); \
if (nullptr == to_repeated_field || nullptr == from_repeated_field) { return false; } \
to_repeated_field->CopyFrom(*from_repeated_field)

	bool ProtoCache::CopyMsgRepeatedFieldToOther(google::protobuf::Message& to_msg, const google::protobuf::FieldDescriptor * to_field,
		google::protobuf::Message& from_msg, const google::protobuf::FieldDescriptor * from_field)
	{
		const google::protobuf::Reflection* from_reflection = from_msg.GetReflection();
		const google::protobuf::Reflection* to_reflection = to_msg.GetReflection();
		if (nullptr == to_reflection)
		{
			return false;
		}
		if (0 == from_reflection->FieldSize(from_msg, from_field))
		{
			return true;
		}
		switch (from_field->cpp_type())
		{
		case google::protobuf::FieldDescriptor::CPPTYPE_FLOAT:
		{
			COPY_REPEATED_FIELD(float);
		}
		break;
		case google::protobuf::FieldDescriptor::CPPTYPE_DOUBLE:
		{
			COPY_REPEATED_FIELD(double);
		}
		break;
		case google::protobuf::FieldDescriptor::CPPTYPE_INT32:
		{
			COPY_REPEATED_FIELD(google::protobuf::int32);
		}
		break;
		case google::protobuf::FieldDescriptor::CPPTYPE_INT64:
		{
			COPY_REPEATED_FIELD(google::protobuf::int64);
		}
		break;
		case google::protobuf::FieldDescriptor::CPPTYPE_UINT32:
		{
			COPY_REPEATED_FIELD(google::protobuf::uint32);
		}
		break;
		case google::protobuf::FieldDescriptor::CPPTYPE_UINT64:
		{
			COPY_REPEATED_FIELD(google::protobuf::uint64);
		}
		break;
		case google::protobuf::FieldDescriptor::CPPTYPE_BOOL:
		{
			COPY_REPEATED_FIELD(bool);
		}
		break;
		case google::protobuf::FieldDescriptor::CPPTYPE_ENUM:
		{
			COPY_REPEATED_FIELD(google::protobuf::int32);
		}
		break;
		case google::protobuf::FieldDescriptor::CPPTYPE_STRING:
		{
			to_reflection->ClearField(&to_msg, to_field);
			int size = from_reflection->FieldSize(from_msg, from_field);
			for (int i = 0; i < size; i++)
			{
				const std::string& get_str = from_reflection->GetRepeatedString(from_msg, from_field, i);
				to_reflection->AddString(&to_msg, to_field, get_str);
			}
		}
		break;
		case google::protobuf::FieldDescriptor::CPPTYPE_MESSAGE:
		{
			to_reflection->ClearField(&to_msg, to_field);
			int size = from_reflection->FieldSize(from_msg, from_field);
			for (int i = 0; i < size; i++)
			{
				google::protobuf::Message* message = from_reflection->MutableRepeatedMessage(&from_msg, from_field, i);
				if (nullptr == message)
				{
					return false;
				}
				google::protobuf::Message* to_message = to_reflection->AddMessage(&to_msg, to_field);
				if (nullptr == to_message)
				{
					return false;
				}
				to_message->CopyFrom(*message);
			}
		}
		break;
		default:
			return false;
		}
		return true;
	}

	bool ProtoCache::CopyMsgNormalFieldToOther(google::protobuf::Message& to_msg,const google::protobuf::FieldDescriptor * to_field,
		google::protobuf::Message& from_msg, const google::protobuf::FieldDescriptor * from_field)
	{
		const google::protobuf::Reflection* from_reflection = from_msg.GetReflection();
		const google::protobuf::Reflection* to_reflection = to_msg.GetReflection();
		if (nullptr == to_reflection)
		{
			return false;
		}
		if (false == from_reflection->HasField(from_msg, from_field))
		{
			return true;
		}
		switch (from_field->cpp_type())
		{
		case google::protobuf::FieldDescriptor::CPPTYPE_FLOAT:
		{
			to_reflection->SetFloat(&to_msg, to_field, from_reflection->GetFloat(from_msg, from_field));
		}
		break;
		case google::protobuf::FieldDescriptor::CPPTYPE_DOUBLE:
		{
			to_reflection->SetDouble(&to_msg, to_field, from_reflection->GetDouble(from_msg, from_field));
		}
		break;
		case google::protobuf::FieldDescriptor::CPPTYPE_INT32:
		{
			to_reflection->SetInt32(&to_msg, to_field, from_reflection->GetInt32(from_msg, from_field));
		}
		break;
		case google::protobuf::FieldDescriptor::CPPTYPE_INT64:
		{
			to_reflection->SetInt64(&to_msg, to_field, from_reflection->GetInt64(from_msg, from_field));
		}
		break;
		case google::protobuf::FieldDescriptor::CPPTYPE_UINT32:
		{
			to_reflection->SetUInt32(&to_msg, to_field, from_reflection->GetUInt32(from_msg, from_field));
		}
		break;
		case google::protobuf::FieldDescriptor::CPPTYPE_UINT64:
		{
			to_reflection->SetUInt64(&to_msg, to_field, from_reflection->GetUInt64(from_msg, from_field));
		}
		break;
		case google::protobuf::FieldDescriptor::CPPTYPE_BOOL:
		{
			to_reflection->SetBool(&to_msg, to_field, from_reflection->GetBool(from_msg, from_field));
		}
		break;
		case google::protobuf::FieldDescriptor::CPPTYPE_ENUM:
		{
			to_reflection->SetEnum(&to_msg, to_field, from_reflection->GetEnum(from_msg, from_field));
		}
		break;
		case google::protobuf::FieldDescriptor::CPPTYPE_STRING:
		{
			to_reflection->SetString(&to_msg, to_field, from_reflection->GetString(from_msg, from_field));
		}
		break;
		case google::protobuf::FieldDescriptor::CPPTYPE_MESSAGE:
		{
			google::protobuf::Message* msg = to_reflection->MutableMessage(&to_msg, to_field);
			if (nullptr == msg)
			{
				return false;
			}
			msg->CopyFrom(from_reflection->GetMessage(from_msg, from_field));
		}
		break;
		default:
			return false;
		}
		return true;
	}
}
