#include "core/common/types.h"
#include "core/dep/mysql/mysql.h"
#include "mysqlcache/mysql_client.h"

static const int MAX_MYSQL_ERR_MSG_SZ = 1024;

namespace MysqlCache
{
	struct mysqlclient_impl_t
	{
		MYSQL*						mysql_conn;
		MYSQL_CONN_CONF				conf;
		std::string					error_msg;
		time_t						last_ping_time;
		mysqlclient_impl_t()
		{
			mysql_conn = NULL;
			last_ping_time = 0;
			error_msg.reserve(MAX_MYSQL_ERR_MSG_SZ);
		}
	};

	size_t strprintf(const char * format, ...)
	{
		char str[1024] = { 0 };
		size_t ncvt = 0;
		va_list	ap;
		va_start(ap, format);
		ncvt = vsnprintf(str, 1024, format, ap);
		va_end(ap);
		std::cout << str << std::endl;
		return ncvt;
	}

#ifndef LOGSTR
#define LOGSTR(format,...)	\
do{\
	strprintf(format, ##__VA_ARGS__); \
} while (0)
#endif

#define _THIS_HANDLE	((mysqlclient_impl_t*)(handle))
#define LOG_S(format, ...)	LOGSTR(format, ##__VA_ARGS__)

	MysqlClient::MysqlClient() {
		handle = new mysqlclient_impl_t();
	}

	inline	void	mysqlclient_cleanup(void *handle) {
		if (_THIS_HANDLE) {
			if (_THIS_HANDLE->mysql_conn) {
				mysql_close(_THIS_HANDLE->mysql_conn);
				_THIS_HANDLE->mysql_conn = NULL;
			}
		}
	}

	MysqlClient::~MysqlClient() {
		mysqlclient_cleanup(handle);
		if (_THIS_HANDLE)
			delete _THIS_HANDLE;
	}
	//
	int MysqlClient::Init(const MYSQL_CONN_CONF& conf) {
		mysqlclient_cleanup(handle); //for reinit
		auto conn = mysql_init(NULL);
		if (!conn) {
			LOG_S("mysql client init error ");
			return -1;
		}
		char tmpset[255] = "";
		if (conf.wait_timeout > 0) {
			sprintf(tmpset, "set wait_timeout=%d", conf.wait_timeout);
			mysql_options(conn, MYSQL_INIT_COMMAND, tmpset);
			mysql_options(conn, MYSQL_OPT_CONNECT_TIMEOUT, &conf.wait_timeout);
			mysql_options(conn, MYSQL_OPT_READ_TIMEOUT, &conf.wait_timeout);
			mysql_options(conn, MYSQL_OPT_WRITE_TIMEOUT, &conf.wait_timeout);
		}
		if (conf.intr_timeout > 0)
		{
			sprintf(tmpset, "set interactive_timeout=%d", conf.intr_timeout);
			mysql_options(conn, MYSQL_INIT_COMMAND, tmpset);
		}
		//auto reconnect
		bool auto_reconnect = conf.auto_reconnect ? 1 : 0;
		mysql_options(conn, MYSQL_OPT_RECONNECT, &auto_reconnect);

		//connect to server
		if (!mysql_real_connect(conn, conf.ip.c_str(), conf.uname.c_str(),
			conf.passwd.c_str(), conf.dbname.c_str(), conf.port,
			conf.unisock.c_str(), conf.cliflag)) {
			LOG_S("mysql_real_connectn error ");
			goto FAIL_CONN;
		}

		if (mysql_set_character_set(conn, conf.char_set.c_str())) {
			LOG_S("set charset error ");
			goto FAIL_CONN;
		}
		if (mysql_autocommit(conn, conf.auto_commit ? 1 : 0)) {
			LOG_S("auto commit set error ");
			goto FAIL_CONN;
		}
		////////////////////////////////////////////////
		_THIS_HANDLE->conf = conf;
		_THIS_HANDLE->mysql_conn = conn;
		return 0;
	FAIL_CONN:
		mysql_close(conn);
		return -2;
	}

	size_t	MysqlClient::Affects()
	{
		return mysql_affected_rows(_THIS_HANDLE->mysql_conn);
	}

	bool MysqlClient::Execute(const std::string& sql, MYSQL_TABLE_ROW* table_row)
	{

		int query_ret = mysql_real_query(_THIS_HANDLE->mysql_conn, sql.c_str(), sql.size());
		if (0 != query_ret)
		{
			printf("[ERROR]exec faild, sql = \n %s \n", sql.c_str());
			return false;
		}
		else
		{
			printf("[ERROR]exec success, sql = \n %s \n", sql.c_str());
		}
		if (table_row)
		{
			return this->Result(table_row);
		}
		return true;
	}

	int MysqlClient::Commit() {//if not auto commit
		if (mysql_commit(_THIS_HANDLE->mysql_conn)) {
			LOG_S("commit error ");
		}
		return 0;
	}

	int	 MysqlClient::Ping() {
		time_t tnow = time(NULL);
		if (tnow < _THIS_HANDLE->last_ping_time + _THIS_HANDLE->conf.ping_chkgap)
			return 0;
		_THIS_HANDLE->last_ping_time = tnow;
		unsigned long mtid1 = mysql_thread_id(_THIS_HANDLE->mysql_conn);
		int ret = mysql_ping(_THIS_HANDLE->mysql_conn);
		if (ret) {
			LOG_S("mysql server ping error !");
			return -1;
		}
		unsigned long mtid2 = mysql_thread_id(_THIS_HANDLE->mysql_conn);
		if (mtid2 != mtid1)	return 1;
		return 0;
	}

	bool MysqlClient::Result(MYSQL_TABLE_ROW* table_row)
	{
		if (nullptr == table_row)
		{
			return false;
		}
		MYSQL_RES* res_set = mysql_store_result(_THIS_HANDLE->mysql_conn);
		if (nullptr == res_set)
		{
			return false;
		}
		table_row->fields_count = mysql_field_count(_THIS_HANDLE->mysql_conn);
		if (0 == table_row->fields_count)
		{
			// 查询错误
			mysql_free_result(res_set);
			return false;
		}
		table_row->row_total = mysql_num_rows(res_set);
		if (0 == table_row->row_total)
		{
			// 数据集为0
			mysql_free_result(res_set);
			return true;
		}
		// 加载表名、字段名
		MYSQL_FIELD * fields_all = mysql_fetch_fields(res_set);
		for (size_t i = 0; i < table_row->fields_count; ++i)
		{
			table_row->fields_names.push_back(fields_all[i].name);
			if (table_row->table_name.empty())
			{
				table_row->table_name = fields_all[i].table;
			}
		}

		// 装填数据
		for (int row_index = 0; row_index < table_row->row_total; ++row_index)
		{
			const char** row_data = (const char **)mysql_fetch_row(res_set);
			size_t* row_length = (size_t*)mysql_fetch_lengths(res_set);
			MYSQL_ROW_FIELD row_data_vec;
			for (int i = 0; i < table_row->fields_count; i++)
			{
				row_data_vec.data.push_back(std::string(row_data[i], row_length[i]));
			}
			table_row->row_data.push_back(row_data_vec);
		}
		mysql_free_result(res_set);
		return true;
	}

	int MysqlClient::ErrNo()
	{
		return mysql_errno(_THIS_HANDLE->mysql_conn);
	}

	const char*	MysqlClient::ErrMsg()
	{
		return _THIS_HANDLE->error_msg.c_str();
	}

	void* MysqlClient::mysql_handle()
	{
		return _THIS_HANDLE->mysql_conn;
	}

	bool MysqlClient::EscapeString(std::string& result, const std::string& input)
	{
		if (input.empty())
		{
			return true;
		}
		if (nullptr == (_THIS_HANDLE->mysql_conn))
		{
			return false;
		}
		int len = input.size();
		escaped_buffer.resize(len * 2);

		char* begin = reinterpret_cast<char*>(escaped_buffer.empty() ? NULL : &*escaped_buffer.begin());
		if (nullptr == begin)
		{
			return false;
		}
		int escape_len = mysql_real_escape_string((st_mysql*)(_THIS_HANDLE->mysql_conn), begin, input.data(), len);
		result.append(escaped_buffer.data(), escape_len);
		return true;
	}
}