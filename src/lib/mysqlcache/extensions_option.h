#ifndef __HERM_CACHE_CACHE_DB_EXTENSIONS_OPTION_H__
#define __HERM_CACHE_CACHE_DB_EXTENSIONS_OPTION_H__

#include <string>

namespace google {
	namespace protobuf {
		class Descriptor;
		class FieldDescriptor;
		class EnumValueDescriptor;
	}
}

namespace MysqlCache
{
	std::string proto_msg_opt(const google::protobuf::Descriptor* desc, const char* name);
	std::string proto_field_opt(const google::protobuf::FieldDescriptor* desc, const char* name);
	std::string proto_enum_opt(const google::protobuf::EnumValueDescriptor* desc, const char* name);
}
#endif