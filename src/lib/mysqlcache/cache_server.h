#ifndef __MYSQLCACHE_CACHESERVER_H__
#define __MYSQLCACHE_CACHESERVER_H__
#include "server/data_dispatcher.h"
#include "mysqlcache/cache_def.h"
#include "mysqlcache/db_session.h"

namespace MysqlCache
{
	class CacheServer
	{
	public:
		CacheServer();
		~CacheServer();

		bool InitCache(Server::DataDispatcher *dis, const std::string &protometa, const std::vector<struct CacheParam> &cacheParams);

		/*
		*Add Pair Message DbSession
		*/
		void AddMessageMap(std::string msgName, DbSession *session);

	private:
		/*
		*Init message handle
		*/
		void InitHandle(Server::DataDispatcher *dis);

		/*
		*Get proto meta req
		*/
		void GetMetaReq(const Proto::Core::MsgBody& msg, Server::DisParam& dp);
		/*
		*Get date frome cache
		*/
		void GetReq(const Proto::Core::MsgBody& msg, Server::DisParam& dp);
		/*
		*save data to cache
		*/
		void SetReq(const Proto::Core::MsgBody& msg, Server::DisParam& dp);

	private:
		Server::DataDispatcher *_dis;
		std::vector<DbSession *> _dbSessions;
		std::unordered_map<std::string, DbSession *> _dbSessionMap;
	};
}
#endif



