#include "core/common/network.h"
#include "server/serverdata_mgr.h"
#include "log/node_handle.h"
#include "log/app.h"

namespace Log {
	static App _app;
	App::App()
		:ServerApp(Proto::Core::STLog)
	{
	}


	App::~App()
	{
	}

	/*
	*Init Server
	*/
	bool App::InitServer()
	{
		CXEngine::App::AppConfig &config = GetAppConfig();
		Server::ServerData &data = Server::ServerDataMgr::Instance()->GetMyServerData();

		std::string ip = Common::GetInterfaceAddressStr();
		_logServer = new CXEngine::Event::TcpServer(GetEventLoop());
		_logDis = new Server::DataDispatcher();
		Common::uint16_t port = _logServer->CreateServer(ip, StartBindPort, EndBindPort, _logDis);
		if (port == 0) {
			printf("[ERROR] create log server failed\n");
			return false;
		}

		data.info.set_ip(Common::Ip4ToAddr(ip));
		data.info.set_port(htons(port));
		printf("[LOG] create log server %s:%d sucess!\n", ip.c_str(), port);

		NodeHandle::Instance()->Init(_nodeDis);

		return true;
	}
}
