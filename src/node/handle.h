#ifndef __MGR_HANDLE_H__
#define __MGR_HANDLE_H__
#include "core/common/singleton.h"
#include "server/data_dispatcher.h"

namespace Node {
	class Handle : public Common::Singleton<Handle>
	{
	public:
		void Init(Server::DataDispatcher *dp);

		void ServerAddReq(const Proto::Core::MsgBody& msg, Server::DisParam& dp);
	};
}
#endif

