#include "core/common/network.h"
#include "server/serverdata_mgr.h"
#include "node/app.h"
#include "node/handle.h"

namespace Node {
	static App _app;
	App::App()
		:ServerApp(Proto::Core::STMgr)
	{
	}


	App::~App()
	{
	}

	/*
	*Init Server
	*/
	bool App::InitServer()
	{
		CXEngine::App::AppConfig &config = GetAppConfig();
		Server::ServerData &data = Server::ServerDataMgr::Instance()->GetMyServerData();

		std::string mgrIp = config.GetStrValue("node_server.ip");
		Common::uint16_t mgrPort = (Common::uint16_t)config.GetIntValue("node_server.port");
		data.info.set_ip(Common::Ip4ToAddr(mgrIp));
		data.info.set_port(htons(mgrPort));

		_datDis = new Server::DataDispatcher;
		_nodeServer = new CXEngine::Event::TcpServer(GetEventLoop());
		bool ret = _nodeServer->CreateServer(mgrIp, mgrPort, _datDis);
		if (!ret) {
			printf("[ERROR] create mgr server failed\n");
			return false;
		}
		printf("[LOG] create mgr server %s:%d success.\n", mgrIp.c_str(), mgrPort);

		Handle::Instance()->Init(_datDis);



		return true;
	}
}
