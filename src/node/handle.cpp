#include "proto/ss/ss_def.pb.h"
#include "proto/ss/ss_session.pb.h"
#include "proto/core/msg.pb.h"
#include "core/common/types.h"
#include "server/serverdata_mgr.h"
#include "server/channel.h"
#include "node/handle.h"


namespace Node {
	void Handle::Init(Server::DataDispatcher *dp)
	{
		dp->Register(Proto::SS::CTSession, Proto::SS::CCSServerAddReq, BIND_CALLBACK_2(Handle::ServerAddReq, this));
	}

	void Handle::ServerAddReq(const Proto::Core::MsgBody& msg, Server::DisParam& dp)
	{
		const Proto::SS::ServerAddReq &req = msg.GetExtension(Proto::SS::server_add_req);
		const Proto::Core::ServerInfo &info = req.info();

		Server::ServerData &data = Server::ServerDataMgr::Instance()->GetServerData(info.server_id());
		data.info.CopyFrom(info);
		data.session = dp.session;

		Proto::SS::ServerAddRes *res = dp.channel->MutableMsgBody().MutableExtension(Proto::SS::server_add_res);
		Server::ServerData &mydata = Server::ServerDataMgr::Instance()->GetMyServerData();
		const std::unordered_map<Common::uint64_t, Server::ServerData *> &alldata = Server::ServerDataMgr::Instance()->GetAllServerData();
		for (auto iter = alldata.begin(); iter != alldata.end(); ++iter) {
			Server::ServerData *sd = iter->second;
			if (sd->info.server_id() != info.server_id()) {
				Proto::Core::ServerInfo *newinfo = res->add_infos();
				newinfo->CopyFrom(sd->info);
				if (sd->info.server_id() != mydata.info.server_id()) {
					Server::Channel ch(sd->session);
					Proto::SS::ServerAddTrap *trap = ch.MutableMsgBody().MutableExtension(Proto::SS::server_add_trap);
					Proto::Core::ServerInfo *trapinfo = trap->mutable_info();
					trapinfo->CopyFrom(sd->info);
					ch.SendMsg(Proto::SS::CTSession, Proto::SS::CCSServerAddTrap);
				}

			}
		}
		dp.channel->SendMsg(Proto::SS::CTSession, Proto::SS::CCSServerAddRes);
	}
}
