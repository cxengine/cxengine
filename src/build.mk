#common.mk

DEBUG = yes

# directories
CX_EXE_DIR = $(CX_HOME)/out
CX_BUILD_DIR = $(CX_HOME)/out/build
CX_OUT_LIB_DIR = $(CX_HOME)/out/lib
CX_INC_DIR = $(CX_HOME)
CX_CORE_LIB_DIR = $(CX_HOME)/core/lib

# compiler and link name
CXX = g++
EXE_LD = g++
STATICLIB_LD = ar -crs


# CXX compiler option
 CXXFLAGS = -c
 ifeq "$(DEBUG)" "no"
 	CXXFLAGS += -O2
 else
 	CXXFLAGS += -g
 endif

 CXXFLAGS += -std=c++11 -Wall -Wno-unused-parameter -Wextra -pipe -D_NEW_LIC -D_GNU_SOURCE -D_REENTRANT 
 CXXFLAGS += -fPIC -fpermissive

 CXXFLAGS += -I$(CX_INC_DIR)
 CXXFLAGS += -I$(CX_HOME)/src
 CXXFLAGS += -I$(CX_HOME)/core/dep/protobuf/src

# link option 
LDFLAGS += -L$(CX_CORE_LIB_DIR) 
LDFLAGS += -L$(CX_OUT_LIB_DIR) 

BASE_LIBS += -lres_server -lres_proto
BASE_LIBS += -lcore_app -lcore_event -lcore_common
BASE_LIBS += -lpthread -lprotobuf

