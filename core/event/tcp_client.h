#ifndef __CXENGINE_EVENT_TCPCLIENT_H__
#define __CXENGINE_EVENT_TCPCLIENT_H__
#include "core/common/types.h"
#include "core/event/tcp_session.h"
#include <iostream>

namespace CXEngine {
namespace Event {
	class EventLoop;
	class TcpClient : public TcpSession
	{
	public:
		TcpClient(EventLoop *loop);
		~TcpClient();

		/*
		*Connect Server
		*/
		void ConnectServer(const std::string &ip, Common::uint16_t port, DataHandler *handler);
		
	};
}}
#endif

