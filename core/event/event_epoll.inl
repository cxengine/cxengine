#ifndef __CXENGINE_NETWORK_SELECTNET_H__
#define __CXENGINE_NETWORK_SELECTNET_H__
namespace CXEngine {
	namespace Event {
		struct ApiState {
            int epfd;
            struct epoll_event *events;
		};

		bool EventLoop::ApiCreate()
		{
			struct ApiState *state = (struct ApiState *)malloc(sizeof(struct ApiState));
			if (!state) return false;

            state->events = (struct epoll_event *)malloc(sizeof(struct epoll_event)*_setsize);
            if (!state->events) {
                free(state);
                return false;            
            }

            state->epfd = epoll_create(1024); /* 1024 is just a hint for the kernel */
            if (state->epfd == -1) {
                free(state->events);
                free(state);
                return false;                  
            }

			this->_apidata = state;
			return true;
		}

		bool EventLoop::aeApiResize(int size)
		{
            _apidata->events = (struct epoll_event *)realloc(_apidata->events, sizeof(struct epoll_event)*size);
			return true;
		}

		void EventLoop::ApiFree()
		{
            close(_apidata->epfd);
            free(_apidata->events);
			free(_apidata);
		}

		int EventLoop::ApiAddEvent(int fd, int mask)
		{
            struct ApiState *state = _apidata;
            int op = _events[fd].mask == FileEventNone ? EPOLL_CTL_ADD : EPOLL_CTL_MOD;
            mask |= _events[fd].mask;

            struct epoll_event ee; 
            ee.events = 0;
            if (mask & FileEventReadable) ee.events |= EPOLLIN;
            if (mask & FileEventWritable) ee.events |= EPOLLOUT;
            ee.data.fd = fd;
            if (epoll_ctl(state->epfd,op,fd,&ee) == -1) {
                printf("EventLoop::ApiAddEvent: epoll_ctl error\n");
                return -1;
            }
			return 0;
		}

		void EventLoop::ApiDelEvent(int fd, int delmask)
		{
            struct ApiState *state = _apidata;
            int mask = _events[fd].mask & (~delmask);
            
            struct epoll_event ee;         
            ee.events = 0;
            if (mask & FileEventReadable) ee.events |= EPOLLIN;
            if (mask & FileEventWritable) ee.events |= EPOLLOUT;
            ee.data.fd = fd;
            if (mask != FileEventNone) {
                epoll_ctl(state->epfd,EPOLL_CTL_MOD,fd,&ee);
            } else {
                /* Note, Kernel < 2.6.9 requires a non null event pointer even for EPOLL_CTL_DEL. */
                epoll_ctl(state->epfd,EPOLL_CTL_DEL,fd,&ee);
            }
		}

		int EventLoop::ApiPoll(struct timeval *tvp)
		{
            struct ApiState *state = _apidata;
            int retval, numevents = 0;

            retval = epoll_wait(state->epfd,state->events,_setsize, tvp ? (tvp->tv_sec*1000 + tvp->tv_usec/1000) : -1);
            if (retval > 0) {
                int j;
                numevents = retval;
                for (j = 0; j < numevents; j++) {
                    int mask = 0;
                    struct epoll_event *e = state->events+j;
                    if (e->events & EPOLLIN) mask |= FileEventReadable;
                    if (e->events & EPOLLOUT) mask |= FileEventWritable;
                    if (e->events & EPOLLERR) mask |= FileEventWritable;                    
                    if (e->events & EPOLLHUP) mask |= FileEventWritable;
                    _fired[j].fd = e->data.fd;
                    _fired[j].mask = mask;
                }
                                            
            }
            return numevents;
		}
	}
}
#endif
