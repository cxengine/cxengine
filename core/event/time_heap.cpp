#include "time_heap.h"


namespace CXEngine {
namespace Event {
	void TimeHeap::ElementInit(TimeEvent *element)
	{
		element->min_heap_idx = -1;
	}

	int TimeHeap::ElementIndex(TimeEvent *element)
	{
		return element->min_heap_idx;
	}

	void TimeHeap::ElementSetIndex(TimeEvent *element, int index)
	{
		element->min_heap_idx = index;
	}

	bool TimeHeap::ElementCmp(TimeEvent *a, TimeEvent *b)
	{
		return a->when_ms > b->when_ms;
	}
}}