#ifndef __CXENGINE_EVENT_TCPSESSION_H__
#define __CXENGINE_EVENT_TCPSESSION_H__
#include "core/event/event_handler.h"
#include "core/event/tcp_buffer.h"
#include "core/common/types.h"
#include "core/common/objpool.h"
#include "core/common/singleton.h"
#include <iostream>

#define SESSIONBUFSIZE		(1<<13) //8k

namespace CXEngine {
namespace Event {
	class TcpServer;
	class EventLoop;
	class TcpSession : public EventHandler
	{
	public:
		TcpSession();
		~TcpSession();
		/*
		*socket have data to read callback
		*/
		virtual void EventReadCallback(EventLoop *netloop, int fd, void *arg);
		/*
		*socket can write data callback
		*/
		virtual void EventWriteCallback(EventLoop *netloop, int fd, void *arg);
		/*
		*Send Data
		*/
		unsigned int SendData(const void *data, unsigned int len);
		/*
		*Read Data
		*/
		unsigned int ReadData(void *data, unsigned int len);
		/*
		*Peek data
		*/
		unsigned int PeekData(void *data, unsigned int len);
		/*
		*Data size
		*/
		inline unsigned int DataSize() {
			return _inbuffer.Used();
		}
		/*
		*enable session
		*/
		void EnableSession();
		/*
		*close session, then add session to disconnect buffer
		*/
		void CloseSession();

		/*
		*reset session
		*/
		void ResetSession();


		void SetNetLoop(EventLoop *loop) { _netloop = loop; }
		void SetServer(TcpServer *server) { _server = server; }
		TcpServer *GetServer() { return _server; }

	protected:
		TcpServer *_server;
		TcpBuffer _inbuffer;
		TcpBuffer _outbuffer;
	};

	class TcpSessionPool : public Common::ObjPool<TcpSession>, public Common::Singleton<TcpSessionPool>
	{
	public:
		virtual void OnRelease(TcpSession& obj) 
		{
			obj.ResetSession();
		}
	};
}}
#endif

