#ifndef __CXENGINE_EVENT_ENDPOINT_H__
#define __CXENGINE_EVENT_ENDPOINT_H__
#include "core/event/address.h"
#include "core/common/types.h"

struct sockaddr_in;

namespace CXEngine {
namespace Event {

	enum {
		SocketFlagNonBlocking = 1,
		SocketFlagNoDelay = 2,
		SocketFlagBroadcast = 4,
		SocketFlagReuseAddr = 8,
		SocketFlagKeepAlive = 16,
	};

	class EndPoint
	{
	public:
		EndPoint();
		~EndPoint();

		/*
		*Create Socket by type
		*type : SOCK_STREAM(Tcp), SOCK_DGRAM(Udp)
		*/
		void Socket(int type);

		/*
		*Endpoint valid
		*/
		bool Good();

		/*
		* Close socket
		*/
		void Close();
		static void Close(int fd);

		/*
		*Get socket
		*/
		inline int GetSocket() { return _socket; }
		inline Address &GetAddress() { return _address; }


		/*
		*bind port
		*/
		int bind(const std::string &ip, Common::uint16_t port);
		int bind(Common::uint32_t networkAddr, Common::uint16_t networkPort);

		/*
		*Listen socket
		*/
		int Listen(int backlog);

		/*
		*Connect Server
		*autosetflags is true, will set nonblocking nodelay
		*/
		int Connect(const std::string &ip, Common::uint16_t port);
		int Connect(Common::uint32_t networkAddr, Common::uint16_t networkPort);

		/*
		*Accept connect
		*/
		int Accept(EndPoint &newpoint);

		/*
		*Set socket flag
		*/
		void SetFlag(int flag);

		/*
		*Set socket no delay
		*/
		static int SetNoDelay(int fd, bool nodelay);

		/*
		*Set socket no blocking
		*/
		static int SetNonBlocking(int fd, bool nonblocking);

		/*
		*Set Broadcast enable
		*/
		static int SetBroadcast(int fd, bool broadcast);

		/*
		*Set socket Reuse enable
		*/
		static int SetReuseAddr(int fd, bool reuseaddr);

		/*
		*Set socket keep alive
		*/
		static int SetKeepAlive(int fd, bool keepalive);

		/*
		*Create socket pair
		*/
		static int SocketPair(int domain, int type, int protocol, int fd[2]);

		/*
		*Send data by tcp
		*/
		int Send(const void * data, int len);
		/*
		*recive data by tcp
		*/
		int Recv(void * data, int len);

		/*
		*Send data to addr, use udp
		*/
		int Sendto(const void * data, int len, const std::string &ip, Common::uint16_t port);
		int Sendto(const void * data, int len, Common::uint32_t networkAddr, Common::uint16_t networkPort);
		int Sendto(const void * data, int len, struct sockaddr_in &sin);

		/*
		*resv data from addr, use 
		*/
		int Recvfrom(void * data, int len, std::string &outip, Common::uint16_t &outport);
		int Recvfrom(void * data, int len, Common::uint32_t &outaddr, Common::uint16_t &outport);
		int Recvfrom(void * data, int len, struct sockaddr_in & sin);

		/*
		*Init network env
		*/
		static void InitNetWork();

	private:
		int _socket;
		int _type;
		Address _address;
	};
}}

#endif

