#ifndef __CXENGINE_EVENT_ADDRESS_H__
#define __CXENGINE_EVENT_ADDRESS_H__
#include "core/common/types.h"
#include <iostream>

namespace CXEngine {
namespace Event {
	class Address
	{
	public:
		Address();
		Address(Common::uint32_t ip_, Common::uint16_t port_);
		Address(const std::string &ip_, Common::uint16_t port_);
		~Address();

		//ip string
		std::string GetIpStr();

		//Host Byte Order
		uint16_t GetPort();

		//ip:port
		std::string &GetAddrDes();

		void ResetAddress();


	public:
		Common::uint32_t ip;
		Common::uint16_t port;//Network Byte Order
		std::string desc;
	};

}}

#endif