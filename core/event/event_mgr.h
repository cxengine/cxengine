#ifndef __CXENGINE_EVENT_EVENTMGR_H__
#define __CXENGINE_EVENT_EVENTMGR_H__
#include "core/common/types.h"
#include "core/common/singleton.h"
#include "core/common/container/ring_buffer.h"
#include <iostream>

namespace CXEngine {
namespace Event {
	class TcpSession;
	class TimeEvent;
	class UdpSession;
	class EventMgr : public Common::Singleton<EventMgr>
	{
	public:
		EventMgr();
		~EventMgr();

		/*
		*Add new connect session
		*/
		bool AddConnectSession(TcpSession *session);

		/*
		*Get new connect session
		*/
		TcpSession *GetConnectSession();

		/*
		*Add new disconnect session
		*/
		bool AddDisconnectSession(TcpSession *session);

		/*
		*Get new disconnect session
		*/
		TcpSession *GetDisconnectSession();

		/*
		*Add readable session
		*/
		bool AddReadableSession(TcpSession *session);

		/*
		*Get readable session
		*/
		TcpSession *GetReadableSession();

		/*
		*Add readable UdpSession
		*/
		bool AddUdpSession(UdpSession *session);

		/*
		*Get readable UdpSession
		*/
		UdpSession *GetUdpSession();

		/*
		*Add timeevent
		*/
		bool AddTimeEvent(TimeEvent *te);

		/*
		*Get timeevent
		*/
		TimeEvent *GetTimeEvent();

	private:
		Common::RingBuffer _connectBuf;
		Common::RingBuffer _disconnectBuf;
		Common::RingBuffer _readableBuf;
		Common::RingBuffer _udpBuf;
		Common::RingBuffer _timerBuf;
	};
}}
#endif

