#ifndef __CXENGINE_EVENT_DATAHANDLER_H__
#define __CXENGINE_EVENT_DATAHANDLER_H__
namespace CXEngine {
namespace Event {
	class TcpSession;
	class UdpBuffer;
	class EventHandler;
	class DataHandler
	{
	public:
		/*
		*tcpsession active
		*/
		virtual void SessionActive(CXEngine::Event::TcpSession *session){}

		/*
		*tcpsession inactive
		*/
		virtual void SessionInactive(CXEngine::Event::TcpSession *session) {}

		/*
		*tcpsession have data to read
		*/
		virtual void DataArrival(CXEngine::Event::TcpSession *session) {}
	};
}}
#endif

