#ifndef __CXENGINE_EVENT_TIMEHEAP_H__
#define __CXENGINE_EVENT_TIMEHEAP_H__
#include "core/common/min_heap.h"
#include "core/common/objpool.h"
#include "core/common/singleton.h"

#define TIMER_STATUS_NORMAL 0
#define TIMER_STATUS_DEL    -1

namespace CXEngine {
namespace Event {

	class TimeEvent;
	class TimeHeap;
	class EventLoop;
	typedef std::function<void(CXEngine::Event::EventLoop *, CXEngine::Event::TimeEvent *, void *)> TimeEventCallback;
	class TimeEvent {
		friend TimeHeap;
		friend EventLoop;
	public:
		void CallHandler(){
			if(callback) callback(loop, this, clientData);
		}
		bool IsRepeat() { return repeat; }
	private:
		bool repeat;
		int time_ms;
		long long when_ms;
		int status;
		void *clientData;
		EventLoop *loop;
		TimeEventCallback callback;
		int min_heap_idx;
	};
	class TimeEventPool: public Common::ObjPool<TimeEvent> , public Common::Singleton<TimeEventPool>{};

	class TimeHeap : public Common::MinHeap<TimeEvent>
	{
	private:
		virtual void ElementInit(TimeEvent *element);
		virtual int ElementIndex(TimeEvent *element);
		virtual void ElementSetIndex(TimeEvent *element, int index);
		virtual bool ElementCmp(TimeEvent *a, TimeEvent *b);
	};
}}
#endif

