#ifndef __CXENGINE_EVENT_TCPBUFFER_H__
#define __CXENGINE_EVENT_TCPBUFFER_H__
#include "core/common/container/ring_buffer.h"

namespace CXEngine {
namespace Event {
	class TcpBuffer : public Common::RingBuffer
	{
	public:
		TcpBuffer(unsigned int size = RINGBUF_MINSIZE);
		~TcpBuffer();

		int Send(int fd);
		int Recv(int fd);
	};
}}
#endif

