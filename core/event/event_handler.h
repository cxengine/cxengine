#ifndef __CXENGINE_EVENT_EVENTHANDLER_H__
#define __CXENGINE_EVENT_EVENTHANDLER_H__
#include "core/event/end_point.h"
#include "core/common/types.h"
#include <iostream>

namespace CXEngine {
namespace Event {
	class EndPoint;
	class EventLoop;
	class DataHandler;
	class EventHandler
	{
	public:
		EventHandler(EventLoop *loop);
		virtual ~EventHandler();
		virtual void EventReadCallback(EventLoop *netloop, int fd, void *arg) {}
		virtual void EventWriteCallback(EventLoop *netloop, int fd, void *arg) {}

		EventLoop *GetNetLoop() { return _netloop; }
		EndPoint &GetEndPoint() { return _endpoint; }
		void SetDataHandler(DataHandler *handler) { _datahandler = handler; }
		DataHandler *GetDataHandler() { return _datahandler; }

	protected:
		EndPoint _endpoint;
		EventLoop *_netloop;
		DataHandler *_datahandler;
	};
}}
#endif

