#ifndef __CXENGINE_NETWORK_SELECTNET_H__
#define __CXENGINE_NETWORK_SELECTNET_H__
namespace CXEngine {
	namespace Event {
		struct ApiState {
			fd_set rfds, wfds;
			fd_set _rfds, _wfds;
		};

		bool EventLoop::ApiCreate()
		{
			struct ApiState *state = new ApiState;
			if (!state) return false;
			FD_ZERO(&state->rfds);
			FD_ZERO(&state->wfds);

			this->_apidata = state;
			return true;
		}

		bool EventLoop::aeApiResize(int size)
		{
			if (size >= FD_SETSIZE) return false;
			return true;
		}

		void EventLoop::ApiFree()
		{
			delete _apidata;
		}

		int EventLoop::ApiAddEvent(int fd, int mask)
		{
			if (mask & FileEventReadable) FD_SET(fd, &_apidata->rfds);
			if (mask & FileEventWritable) FD_SET(fd, &_apidata->wfds);
			return 0;
		}

		void EventLoop::ApiDelEvent(int fd, int mask)
		{
			if (mask & FileEventReadable) FD_CLR(fd, &_apidata->rfds);
			if (mask & FileEventWritable) FD_CLR(fd, &_apidata->wfds);
		}

		int EventLoop::ApiPoll(struct timeval *tvp)
		{
			int retval, j, numevents = 0;

			memcpy(&_apidata->_rfds, &_apidata->rfds, sizeof(fd_set));
			memcpy(&_apidata->_wfds, &_apidata->wfds, sizeof(fd_set));

			retval = select(_maxfd + 1, &_apidata->_rfds, &_apidata->_wfds, NULL, tvp);
			if (retval > 0) {
				for (j = 0; j <= _maxfd; j++) {
					int mask = 0;
					struct FileEvent *fe = &_events[j];

					if (fe->mask == FileEventNone) continue;
					if (fe->mask & FileEventReadable && FD_ISSET(j, &_apidata->_rfds))
						mask |= FileEventReadable;
					if (fe->mask & FileEventWritable && FD_ISSET(j, &_apidata->_wfds))
						mask |= FileEventWritable;
					_fired[numevents].fd = j;
					_fired[numevents].mask = mask;
					numevents++;
				}
			}
			return numevents;
		}
	}
}
#endif
