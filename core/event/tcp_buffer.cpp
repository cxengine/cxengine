#include "core/event/tcp_buffer.h"
#include "core/common/types.h"

namespace CXEngine {
namespace Event {
	TcpBuffer::TcpBuffer(unsigned int size)
		:Common::RingBuffer(size)
	{
	}

	TcpBuffer::~TcpBuffer()
	{
	}

	int TcpBuffer::Send(int fd)
	{
		int alen = 0, len = 0, outlen = 0, off = 0;
		alen = Used();
		off =_out & _mask;
		len = MIN((unsigned int)alen,_size - off);

		outlen += ::send(fd, (const char *)_data + off, len, 0);
		if (outlen == len && alen - len > 0)
			outlen += ::send(fd, (const char *)_data, alen - len, 0);

		if (outlen > 0) {
			smp_wmb();
			_out += outlen;
		}
		return outlen;
	}

	int TcpBuffer::Recv(int fd)
	{
		int alen = 0, len = 0, outlen = 0, off = 0;
		alen = Unused();
		off = _in & _mask;
		len = MIN((unsigned int)alen, _size - off);

		outlen += ::recv(fd, (char *)_data + off, len, 0);
		if (outlen == len && alen - len > 0)
			outlen += ::recv(fd, (char *)_data, alen - len, 0);

		if (outlen > 0) {
			smp_wmb();
			_in += outlen;
		}
		return outlen;
	}
}}
