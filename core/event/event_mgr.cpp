#include "core/event/event_mgr.h"
#include "core/common/except.h"


namespace CXEngine {
namespace Event {
	EventMgr::EventMgr()
	{
	}


	EventMgr::~EventMgr()
	{
	}

	/*
	*Add new connect session
	*/
	bool EventMgr::AddConnectSession(TcpSession *session)
	{
		if (_connectBuf.Unused() >= sizeof(TcpSession *)) {
			_connectBuf.In(&session, sizeof(TcpSession *));
			return true;
		}
		throw NORMAL_EXCEPT("ConnectSession buf full!");
	}

	/*
	*Get new connect session
	*/
	TcpSession *EventMgr::GetConnectSession()
	{
		TcpSession *session = nullptr;
		if (_connectBuf.Used() >= sizeof(TcpSession *)) {
			_connectBuf.Out(&session, sizeof(TcpSession *));
		}
		return session;
	}

	/*
	*Add new disconnect session
	*/
	bool EventMgr::AddDisconnectSession(TcpSession *session)
	{
		if (_disconnectBuf.Unused() >= sizeof(TcpSession *)) {
			_disconnectBuf.In(&session, sizeof(TcpSession *));
			return true;
		}
		throw NORMAL_EXCEPT("DisconnectSession buf full!");
	}

	/*
	*Get new disconnect session
	*/
	TcpSession *EventMgr::GetDisconnectSession()
	{
		TcpSession *session = nullptr;
		if (_disconnectBuf.Used() >= sizeof(TcpSession *)) {
			_disconnectBuf.Out(&session, sizeof(TcpSession *));
		}
		return session;
	}

	/*
	*Add readable session
	*/
	bool EventMgr::AddReadableSession(TcpSession *session)
	{
		if (_readableBuf.Unused() >= sizeof(TcpSession *)) {
			_readableBuf.In(&session, sizeof(TcpSession *));
			return true;
		}
		throw NORMAL_EXCEPT("ReadableSession buf full!");
	}

	/*
	*Get readable session
	*/
	TcpSession *EventMgr::GetReadableSession()
	{
		TcpSession *session = nullptr;
		if (_readableBuf.Used() >= sizeof(TcpSession *)) {
			_readableBuf.Out(&session, sizeof(TcpSession *));
		}
		return session;
	}

	/*
	*Add readable UdpClient
	*/
	bool EventMgr::AddUdpSession(UdpSession *session)
	{
		if (_udpBuf.Unused() >= sizeof(UdpSession *)) {
			_udpBuf.In(&session, sizeof(UdpSession *));
			return true;
		}
		throw NORMAL_EXCEPT("UdpClient buf full!");
	}

	/*
	*Get readable UdpClient
	*/
	UdpSession *EventMgr::GetUdpSession()
	{
		UdpSession *session = nullptr;
		if (_udpBuf.Used() >= sizeof(UdpSession *)) {
			_udpBuf.Out(&session, sizeof(UdpSession *));
		}
		return session;
	}

	/*
	*Add timeevent
	*/
	bool EventMgr::AddTimeEvent(TimeEvent *te)
	{
		if (_timerBuf.Unused() >= sizeof(TimeEvent *)) {
			_timerBuf.In(&te, sizeof(TimeEvent *));
			return true;
		}
		throw NORMAL_EXCEPT("Timer buf full!");
	}

	/*
	*Get timeevent
	*/
	TimeEvent *EventMgr::GetTimeEvent()
	{
		TimeEvent *te = nullptr;
		if (_timerBuf.Used() >= sizeof(TimeEvent *)) {
			_timerBuf.Out(&te, sizeof(TimeEvent *));
		}
		return te;
	}
}}
