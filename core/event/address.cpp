#include "core/event/address.h"
#include "core/common/network.h"
#include "core/common/string.h"

namespace CXEngine {
namespace Event {
	Address::Address()
		:desc("")
	{
	}

	Address::Address(Common::uint32_t ip_, Common::uint16_t port_)
		: ip(ip_), port(port_), desc("")
	{
	}

	Address::Address(const std::string &ip_, Common::uint16_t port_)
		: ip(0), port(htons(port_)), desc("")
	{
		ip = Common::Ip4ToAddr(ip_);
	}

	Address::~Address()
	{
	}

	std::string Address::GetIpStr()
	{
		return Common::AddrToIp4(ip);
	}

	//Host Byte Order
	uint16_t Address::GetPort()
	{
		return ntohs(port);
	}

	std::string& Address::GetAddrDes()
	{
		if (desc == "") {
			desc = Common::Format("%s:%d", Common::AddrToIp4(ip).c_str(), ntohs(port));
		}
		return desc;
	}

	void Address::ResetAddress()
	{
		ip = 0;
		port = 0;
		desc = "";
	}

}}
