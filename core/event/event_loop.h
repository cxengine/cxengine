#ifndef __CXENGINE_EVENT_EVENTLOOP_H__
#define __CXENGINE_EVENT_EVENTLOOP_H__
#include <functional> 
#include "core/event/time_heap.h"

#define DEFAULT_FD_SIZE		(1<<14)

struct timeval;
namespace CXEngine {
namespace Event {
	struct ApiState;
	class EventLoop;
	typedef std::function<void(EventLoop *, int, void *)> FileCallback;
	enum {
		FileEventNone = 0,
		FileEventReadable = 1,
		FileEventWritable = 2,
	};
	struct FileEvent {
		int mask;
		void *data;
		FileCallback readCallback;
		FileCallback writeCallback;
	};
	struct FiredEvent {
		int fd;
		int mask;
	};
	class EventLoop
	{
	public:
		EventLoop(int size = DEFAULT_FD_SIZE);
		~EventLoop();

		/*
		*file event op
		*/
		bool AddFileEvent(int fd, const FileCallback &readCallback, const FileCallback &writeCallback, void *data);
		void EnableFileEvent(int fd, int mask);
		void DisableFileEvent(int fd, int mask);
		void DelFileEvent(int fd);

		TimeEvent *AddTimer(int ms, bool repeat, const TimeEventCallback &callback, void *data);
		void DelTimer(TimeEvent *te);

		/*
		*Start Net Loop,
		*blocking = true: will blocking current thread
		*blocking = flase: will blocking current thread 20us or less
		*/
		void StartLoop(bool blocking = true);
		void StopLoop();
		

	private:
		//wake up loop
		void WakeUpLoop();
		void WakeUpCallback(EventLoop *netloop, int fd, void *data);
		int ProcessEvents(struct timeval *tv);

		int ProcessTimer();

		// net api
		bool ApiCreate();
		bool aeApiResize(int size);
		void ApiFree();
		int ApiAddEvent(int fd, int mask);
		void ApiDelEvent(int fd, int mask);
		int ApiPoll(struct timeval *tvp);


	private:
		int _maxfd;
		int _setsize;
		struct FileEvent *_events;  /* Registered events */
		struct FiredEvent *_fired;
		TimeHeap *_timeHeap;
		struct ApiState *_apidata;
		bool _stop;
		bool _waiting;
		int _fd[2];
		
	};

}}
#endif
