#include "core/event/event_handler.h"
#include "core/event/end_point.h"


namespace CXEngine {
namespace Event {
	EventHandler::EventHandler(EventLoop *loop)
		:_netloop(loop)
		,_datahandler(nullptr)
	{
	}
	EventHandler::~EventHandler()
	{
	}
}}
