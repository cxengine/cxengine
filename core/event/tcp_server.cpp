#include "core/event/tcp_server.h"
#include "core/event/end_point.h"
#include "core/event/tcp_session.h"
#include "core/event/event_loop.h"
#include "core/event/event_mgr.h"
#include "core/event/data_handler.h"
#include "core/common/except.h"
#include "core/common/string.h"
#include "core/common/network.h"


namespace CXEngine {
namespace Event {
	TcpServer::TcpServer(EventLoop *loop)
		:EventHandler(loop)
	{
	}


	TcpServer::~TcpServer()
	{
	}

	bool TcpServer::CreateServer(const std::string &ip, Common::uint16_t port, DataHandler *handler)
	{
		return CreateServer(ip, port, port, handler) > 0;
	}

	Common::uint16_t TcpServer::CreateServer(const std::string &ip, Common::uint16_t startPort, Common::uint16_t endPort, DataHandler *handler)
	{
		if (_endpoint.Good()) return 0;
		_datahandler = handler;

		int ret = 0;
		_endpoint.Socket(SOCK_STREAM);
		if (!_endpoint.Good()) {
			printf("[ERROR] socket error\n");
			return 0;
		}

		ret = -1;
		Common::uint16_t port = 0;
		for (port = startPort; port <= endPort; port++) {
			ret = _endpoint.bind(ip, port);
			if (ret == 0) break;
		}
		if (ret < 0) {
			_endpoint.Close();
			printf("[ERROR] bind %s:[%d-%d] failed\n", ip.c_str(), startPort, endPort);
			return 0;
		}

		ret = _endpoint.Listen(64);
		if (ret < 0) {
			_endpoint.Close();
			printf("[ERROR] listen failed\n");
			return 0;
		}
		_endpoint.SetFlag(SocketFlagNoDelay | SocketFlagNonBlocking);

		if (!_netloop->AddFileEvent(_endpoint.GetSocket(), BIND_CALLBACK_3(TcpServer::EventReadCallback, this), nullptr, nullptr))
			throw NORMAL_EXCEPT("AddFileEvent failed!");
		_netloop->EnableFileEvent(_endpoint.GetSocket(), FileEventReadable);

		return port;
	}

	/*
	*new connect callback
	*/
	void TcpServer::EventReadCallback(EventLoop *netloop, int fd, void *arg)
	{
		TcpSession &session = TcpSessionPool::Instance()->AcquireObject();
		int ret = _endpoint.Accept(session.GetEndPoint());
		if (ret < 0) TcpSessionPool::Instance()->ReleaseObject(session);

		RegisterSession(&session);
		session.SetNetLoop(netloop);
		session.SetServer(this);
		session.SetDataHandler(_datahandler);
        session.GetEndPoint().SetFlag(SocketFlagNoDelay | SocketFlagNonBlocking);
		
		try {
			EventMgr::Instance()->AddConnectSession(&session);
		}catch (Common::NormalExcept e) {
			TcpSessionPool::Instance()->ReleaseObject(session);
			throw e;
		}
	}

	/*
	*add session to server
	*/
	void TcpServer::RegisterSession(TcpSession *session)
	{
		_sessionMap[session->GetEndPoint().GetSocket()] = session;
	}

	/*
	*remvoe session from server
	*/
	void TcpServer::RemoveSession(TcpSession *session)
	{
		auto iter = _sessionMap.find(session->GetEndPoint().GetSocket());
		if (iter != _sessionMap.end())
			_sessionMap.erase(iter);
	}

	/*
	*Get Session
	*/
	TcpSession *TcpServer::GetSession(int sessionIndex)
	{
		auto iter = _sessionMap.find(sessionIndex);
		if (iter != _sessionMap.end())
			return iter->second;
		return nullptr;
	}

}}
