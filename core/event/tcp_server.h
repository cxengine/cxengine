#ifndef __CXENGINE_EVENT_TCPSERVER_H__
#define __CXENGINE_EVENT_TCPSERVER_H__
#include "core/event/event_handler.h"
#include "core/common/container/ring_buffer.h"
#include "core/common/types.h"

namespace CXEngine {
namespace Event {
	class EventLoop;
	class TcpSession;
	class EndPoint;
	class DataHandler;
	class TcpServer : public EventHandler
	{
	public:
		TcpServer(EventLoop *loop);
		~TcpServer();

		/*
		*Create Server
		*@ip ip addr string
		*@port bind port(host)
		*@handler recive data handler
		*return if sucess return true, elese return false
		*/
		bool CreateServer(const std::string &ip, Common::uint16_t port, DataHandler *handler);

		/*
		*Create Server
		*@ip ip addr string
		*@startPort start to bind port(host)
		*@endPort end to bind port(host)
		*@handler recive data handler
		*return if sucess return bind port(host), elese return 0
		*/
		Common::uint16_t CreateServer(const std::string &ip, Common::uint16_t startPort, Common::uint16_t endPort, DataHandler *handler);

		/*
		*new connect callback
		*/
		virtual void EventReadCallback(EventLoop *netloop, int fd, void *arg);

		/*
		*add session to server
		*/
		void RegisterSession(TcpSession *session);

		/*
		*remvoe session from server
		*/
		void RemoveSession(TcpSession *session);

		/*
		*Get Session
		*/
		TcpSession *GetSession(int sessionIndex);

	private:
		std::unordered_map<int, TcpSession *> _sessionMap;
	};
}}
#endif

