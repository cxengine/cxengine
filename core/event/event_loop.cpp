#include "core/event/event_loop.h"
#include "core/event/event_mgr.h"
#include "core/event/end_point.h"
#include "core/event/time_heap.h"
#include "core/common/except.h"
#include "core/common/types.h"
#include "core/common/time.h"

#ifdef __linux__
#include "event_epoll.inl"
#else
#include "event_select.inl"
#endif

namespace CXEngine {
namespace Event {
	EventLoop::EventLoop(int size)
		: _maxfd(-1)
		,_events(nullptr)
		, _fired(nullptr)
		, _apidata(nullptr)
		, _stop(false)
		, _waiting(false)
	{
		if (size < 1024) size = DEFAULT_FD_SIZE;
		_setsize = size;

		EndPoint::InitNetWork();

		_events = new struct FileEvent[size];
		_fired = new struct FiredEvent[size];
		_timeHeap = new TimeHeap;
		ApiCreate();
		if (!_events || !_fired || !_apidata)
			throw NORMAL_EXCEPT("alloc mem failed");
		

		//内部通信
		int ret = EndPoint::SocketPair(AF_UNIX, SOCK_STREAM, 0, _fd);
		if (ret < 0) {
			throw NORMAL_EXCEPT("socketpair failed");
		}
		if (!AddFileEvent(_fd[1], BIND_CALLBACK_3(EventLoop::WakeUpCallback, this), nullptr, nullptr)) {
			EndPoint::Close(_fd[0]);
			EndPoint::Close(_fd[1]);
			throw NORMAL_EXCEPT("AddFileEvent failed");
		}
		EnableFileEvent(_fd[1], FileEventReadable);
		return;
	}


	EventLoop::~EventLoop()
	{
	}

	/*
	*file event op
	*/
	bool EventLoop::AddFileEvent(int fd, const FileCallback &readCallback, const FileCallback &writeCallback, void *data)
	{
		if (fd >= _setsize) {
			Common::SetError(ERANGE);
			return false;
		}
		FileEvent *fe = &_events[fd];
		fe->readCallback = readCallback;
		fe->writeCallback = writeCallback;
        fe->mask = FileEventNone;
		fe->data = data;
		if (fd > _maxfd)
			_maxfd = fd;

		return true;
	}

	void EventLoop::EnableFileEvent(int fd, int mask)
	{
		if (fd >= _setsize) return;
		ApiAddEvent(fd, mask);
		FileEvent *fe = &_events[fd];
		fe->mask |= mask;
		WakeUpLoop();
	}

	void EventLoop::DisableFileEvent(int fd, int mask)
	{
		if (fd >= _setsize) return;
		ApiDelEvent(fd, mask);
		FileEvent *fe = &_events[fd];
		fe->mask &= (~mask);
		WakeUpLoop();
	}

	void EventLoop::DelFileEvent(int fd)
	{
		if (fd >= _setsize) return;
		ApiDelEvent(fd, FileEventReadable | FileEventWritable);
		FileEvent *fe = &_events[fd];
		fe->mask = FileEventNone;
		WakeUpLoop();
	}

	TimeEvent *EventLoop::AddTimer(int ms, bool repeat, const TimeEventCallback &callback, void *data)
	{
		TimeEvent &te = TimeEventPool::Instance()->AcquireObject();
		te.repeat = repeat;
		te.time_ms = ms;
		te.status = TIMER_STATUS_NORMAL;
		te.callback = callback;
		te.clientData = data;
		te.loop = this;
		te.when_ms = Common::GetSystemTick() + ms;
		_timeHeap->HeapPush(&te);
		WakeUpLoop();

		return &te;
	}

	void EventLoop::DelTimer(TimeEvent *te)
	{
		if (te) te->status = TIMER_STATUS_DEL;
	}

	void EventLoop::WakeUpLoop()
	{
		if (_waiting)
			send(_fd[0], "0", 1, 0);
	}
	void EventLoop::WakeUpCallback(EventLoop *netloop, int fd, void *data)
	{
		char buf[1024];
		recv(fd, buf, 1024, 0);
	}

	/*
	*Start Net Loop,
	*blocking = true: will blocking current thread
	*blocking = flase: will blocking current thread 20us
	*/
	void EventLoop::StartLoop(bool blocking)
	{
		_stop = false;
		struct timeval tv = { 0, 20 };
		while (!_stop) {
			if (!blocking) {
				ProcessEvents(&tv);
				break;
			}
			else {
				ProcessEvents(nullptr);
			}
		}
	}

	void EventLoop::StopLoop()
	{
		_stop = true;
		WakeUpLoop();
	}

	int EventLoop::ProcessEvents(struct timeval *tv)
	{
		int processed = 0, numevents;
		struct timeval tmptv, *tvp = nullptr;

		TimeEvent *shortest = _timeHeap->HeapTop();
		while (shortest && shortest->status == TIMER_STATUS_DEL) {
			_timeHeap->HeapPop();
			TimeEventPool::Instance()->OnRelease(*shortest);
			shortest = _timeHeap->HeapTop();
		}
		if (shortest && !tv) {
			long long tick = Common::GetSystemTick();
			if (shortest->when_ms > tick) {
				tick = shortest->when_ms - tick;
				tmptv = { (long)tick / 1000, (long)(tick % 1000) * 1000 };
			}else{
				tmptv = { 0, 1 }; //修改系统时间导致
			}
			tvp = &tmptv;
		}else {
			tvp = tv;
		}

		_waiting = true;
		numevents = ApiPoll(tvp);
		_waiting = false;

		for (int i = 0; i < numevents; i++) {
			struct FileEvent *fe = &_events[_fired[i].fd];
			int mask = _fired[i].mask;
			int fd = _fired[i].fd;

			if (fe->mask & mask & FileEventReadable && fe->readCallback)
				fe->readCallback(this, fd, fe->data);
			if (fe->mask & mask & FileEventWritable && fe->writeCallback)
				fe->writeCallback(this, fd, fe->data);
			processed++;
		}

		processed += ProcessTimer();

		return processed;
	}

	int EventLoop::ProcessTimer()
	{
		long long tick = 0;
		int processed = 0;
		TimeEvent *shortest = _timeHeap->HeapTop();
		while (shortest) {
			if (shortest->status == TIMER_STATUS_DEL) {
				TimeEventPool::Instance()->OnRelease(*shortest);
			}else{
				if(tick == 0) tick = Common::GetSystemTick();
				if (tick > shortest->when_ms)
				{
					_timeHeap->HeapPop();
					EventMgr::Instance()->AddTimeEvent(shortest);
					if (shortest->repeat) {
						shortest->when_ms = tick + shortest->time_ms;
						_timeHeap->HeapPush(shortest);	
						processed++;
					}
				}
				else
					break;
			}
			shortest = _timeHeap->HeapTop();
		}
		return processed;
	}
}}
