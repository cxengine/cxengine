#include "core/event/tcp_client.h"
#include "core/event/event_loop.h"
#include "core/event/event_mgr.h"
#include "core/event/data_handler.h"
#include "core/common/except.h"
#include "core/common/string.h"

namespace CXEngine {
namespace Event {
	TcpClient::TcpClient(EventLoop *loop){
		_netloop = loop;
	}

	TcpClient::~TcpClient()
	{
	}

	/*
	*Connect Server
	*/
	void TcpClient::ConnectServer(const std::string &ip, Common::uint16_t port, DataHandler *handler)
	{
		if (_endpoint.Good()) return;
		_datahandler = handler;

		_endpoint.Socket(SOCK_STREAM);
		if (!_endpoint.Good())
			throw NORMAL_EXCEPT("socket failed!");

		int ret = _endpoint.Connect(ip, port);
		if (ret < 0) {
			_endpoint.Close();
			std::string des = Common::Format("connect %s:%d failed", ip.c_str(), port);
			throw NORMAL_EXCEPT(des);
		}

		_endpoint.SetFlag(SocketFlagNoDelay | SocketFlagNonBlocking);
		EventMgr::Instance()->AddConnectSession(this);
	}
}}
