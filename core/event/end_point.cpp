#include "core/event/end_point.h"
#include "core/common/except.h"
#include "core/common/network.h"

#ifndef INVALID_SOCKET
#define INVALID_SOCKET		-1
#endif

namespace CXEngine {
namespace Event {

	EndPoint::EndPoint()
		:_socket(INVALID_SOCKET)
		, _type(INVALID_SOCKET)
	{
	}


	EndPoint::~EndPoint()
	{
		Close();
	}

	/*
	*Create Socket by type
	*type : SOCK_STREAM(Tcp), SOCK_DGRAM(Udp)
	*/
	void EndPoint::Socket(int type)
	{
		if (Good()) return;
		_type = type;
		_socket = ::socket(AF_INET, type, 0);

#ifdef WIN32
		if ((_socket == INVALID_SOCKET) && (WSAGetLastError() == WSANOTINITIALISED)){
			InitNetWork();
			_socket = ::socket(AF_INET, type, 0);
		}
#endif
	}

	/*
	*Endpoint valid
	*/
	bool EndPoint::Good()
	{ 
		return (_socket != INVALID_SOCKET);
	}

	/*
	* Close socket
	*/
	void EndPoint::Close()
	{
		if (_socket != INVALID_SOCKET) {
			EndPoint::Close(_socket);
			_socket = INVALID_SOCKET;
		}
	}

	void EndPoint::Close(int fd)
	{
#ifdef WIN32
		closesocket(fd);
#else
		close(fd);
#endif
	}

	/*
	*bind port
	*/
	int EndPoint::bind(const std::string &ip, Common::uint16_t port)
	{
		return bind(Common::Ip4ToAddr(ip), htons(port));
	}
	int EndPoint::bind(Common::uint32_t networkAddr, Common::uint16_t networkPort)
	{
		struct sockaddr_in	sin;
		memset(&sin, 0, sizeof(struct sockaddr_in));
		sin.sin_family = AF_INET;
		sin.sin_port = networkPort;
		sin.sin_addr.s_addr = networkAddr;
		_address.ip = networkAddr;
		_address.port = networkPort;
		return ::bind(_socket, (struct sockaddr*)&sin, sizeof(sin));
	}

	/*
	*Listen socket
	*/
	int EndPoint::Listen(int backlog)
	{
		return ::listen(_socket, backlog);
	}

	/*
	*Connect Server
	*autosetflags is true, will set nonblocking nodelay
	*/
	int EndPoint::Connect(const std::string &ip, Common::uint16_t port)
	{
		return Connect(Common::Ip4ToAddr(ip), htons(port));
	}
	int EndPoint::Connect(Common::uint32_t networkAddr, Common::uint16_t networkPort)
	{
		struct sockaddr_in	sin;
		sin.sin_family = AF_INET;
		sin.sin_port = networkPort;
		sin.sin_addr.s_addr = networkAddr;
		_address.ip = networkAddr;
		_address.port = networkPort;
		return ::connect(_socket, (struct sockaddr*)&sin, sizeof(sin));
	}

	/*
	*Accept connect
	*/
	int EndPoint::Accept(EndPoint &newpoint)
	{
		struct sockaddr_in		sin;
		socklen_t sinLen = sizeof(sin);
		int ret = (int)::accept(_socket, (struct sockaddr*)&sin, &sinLen);
		if (ret < 0) return ret;

		newpoint._socket = ret;
		newpoint._address.port = sin.sin_port;
		newpoint._address.ip = sin.sin_addr.s_addr;
		return ret;
	}

	/*
	*Set socket flag
	*/
	void EndPoint::SetFlag(int flag)
	{
		if (flag & SocketFlagNonBlocking)
			SetNonBlocking(_socket, true);
		if (flag & SocketFlagNoDelay)
			SetNoDelay(_socket, true);
		if (flag & SocketFlagBroadcast)
			SetBroadcast(_socket, true);
		if (flag & SocketFlagReuseAddr)
			SetReuseAddr(_socket, true);
		if (flag & SocketFlagKeepAlive)
			SetKeepAlive(_socket, true);
	}

	/*
	*Set socket no delay
	*/
	int EndPoint::SetNoDelay(int fd, bool nodelay)
	{
		int arg = int(nodelay);
        return ::setsockopt(fd, IPPROTO_TCP, TCP_NODELAY, (char*)&arg, sizeof(int));
	}

	/*
	*Set socket no blocking
	*/
	int EndPoint::SetNonBlocking(int fd, bool nonblocking)
	{
#ifdef WIN32
		u_long val = nonblocking ? 1 : 0;
		return ::ioctlsocket(fd, FIONBIO, &val);
#else
		int val = nonblocking ? O_NONBLOCK : 0;
		return ::fcntl(fd, F_SETFL, val);
#endif
	}

	/*
	*Set Broadcast enable
	*/
	int EndPoint::SetBroadcast(int fd, bool broadcast)
	{
#ifdef WIN32
        bool val;
#else
		int val;
		if (broadcast)
		{
			val = 2;
			::setsockopt(fd, SOL_IP, IP_MULTICAST_TTL, &val, sizeof(int));
		}
#endif
		val = broadcast ? 1 : 0;
		return ::setsockopt(fd, SOL_SOCKET, SO_BROADCAST, (char*)&val, sizeof(val));
	}

	/*
	*Set socket Reuse enable
	*/
	int EndPoint::SetReuseAddr(int fd, bool reuseaddr)
	{
#ifdef WIN32
        bool val;
#else
		int val;
#endif
		val = reuseaddr ? 1 : 0;
		return ::setsockopt(fd, SOL_SOCKET, SO_REUSEADDR,
			(char*)&val, sizeof(val));
	}

	/*
	*Set socket keep alive
	*/
	int EndPoint::SetKeepAlive(int fd, bool keepalive)
	{
#ifdef WIN32 
        bool val;
#else
		int val;
#endif
		val = keepalive ? 1 : 0;
		return ::setsockopt(fd, SOL_SOCKET, SO_KEEPALIVE,
			(char*)&val, sizeof(val));
	}

	/*
	*Create socket pair
	*/
	int EndPoint::SocketPair(int domain, int type, int protocol, int fd[2])
	{
#ifdef WIN32
#define ERR(e) WSA##e
		int listener = -1;
		int connector = -1;
		int acceptor = -1;
		struct sockaddr_in listen_addr;
		struct sockaddr_in connect_addr;
		socklen_t size;
		int saved_errno = -1;

		if (protocol || (domain != AF_INET
#ifdef AF_UNIX
				&& domain != AF_UNIX
#endif
				)) {
			Common::SetError(ERR(EAFNOSUPPORT));
			return -1;
		}
		if (!fd) {
			Common::SetError(ERR(EINVAL));
			return -1;
		}

		listener = ::socket(AF_INET, type, 0);
		if (listener < 0)
			return -1;
		memset(&listen_addr, 0, sizeof(listen_addr));
		listen_addr.sin_family = AF_INET;
		listen_addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
		listen_addr.sin_port = 0;	/* kernel chooses port.	 */
		if (::bind(listener, (struct sockaddr *) &listen_addr, sizeof(listen_addr))
			== -1)
			goto tidy_up_and_fail;
		if (listen(listener, 1) == -1)
			goto tidy_up_and_fail;

		connector = socket(AF_INET, type, 0);
		if (connector < 0)
			goto tidy_up_and_fail;
		/* We want to find out the port number to connect to.  */
		size = sizeof(connect_addr);
		if (getsockname(listener, (struct sockaddr *) &connect_addr, &size) == -1)
			goto tidy_up_and_fail;
		if (size != sizeof(connect_addr))
			goto abort_tidy_up_and_fail;
		if (connect(connector, (struct sockaddr *) &connect_addr,
			sizeof(connect_addr)) == -1)
			goto tidy_up_and_fail;

		size = sizeof(listen_addr);
		acceptor = accept(listener, (struct sockaddr *) &listen_addr, &size);
		if (acceptor < 0)
			goto tidy_up_and_fail;
		if (size != sizeof(listen_addr))
			goto abort_tidy_up_and_fail;
		if (getsockname(connector, (struct sockaddr *) &connect_addr, &size) == -1)
			goto tidy_up_and_fail;
		if (size != sizeof(connect_addr)
			|| listen_addr.sin_family != connect_addr.sin_family
			|| listen_addr.sin_addr.s_addr != connect_addr.sin_addr.s_addr
			|| listen_addr.sin_port != connect_addr.sin_port)
			goto abort_tidy_up_and_fail;
		EndPoint::Close(listener);
		fd[0] = connector;
		fd[1] = acceptor;

		return 0;

	abort_tidy_up_and_fail:
		saved_errno = ERR(ECONNABORTED);
	tidy_up_and_fail:
		if (saved_errno < 0)
			saved_errno = Common::GetError();
		if (listener != -1)
			EndPoint::Close(listener);
		if (connector != -1)
			EndPoint::Close(connector);
		if (acceptor != -1)
			EndPoint::Close(acceptor);

		Common::SetError(saved_errno);
		return -1;
#undef ERR
#else
		return socketpair(domain, type, protocol, fd);
#endif
	}

	/*
	*Send data by tcp
	*/
	int EndPoint::Send(const void * data, int len)
	{
		return ::send(_socket, (char*)data, len, 0);
	}

	/*
	*recive data by tcp
	*/
	int EndPoint::Recv(void * data, int len)
	{
		return ::recv(_socket, (char*)data, len, 0);
	}

	/*
	*Send data to addr, use udp
	*/
	int EndPoint::Sendto(const void * data, int len, const std::string &ip, Common::uint16_t port)
	{
		return Sendto(data, len, Common::Ip4ToAddr(ip), htons(port));
	}
	int EndPoint::Sendto(const void * data, int len, Common::uint32_t networkAddr, Common::uint16_t networkPort)
	{
		struct sockaddr_in	sin;
		sin.sin_family = AF_INET;
		sin.sin_port = networkPort;
		sin.sin_addr.s_addr = networkAddr;

		return this->Sendto(data, len, sin);
	}
	int EndPoint::Sendto(const void * data, int len, struct sockaddr_in & sin)
	{
		return ::sendto(_socket, (const char*)data, len, 0, (struct sockaddr*)&sin, sizeof(sin));
	}

	/*
	*resv data from addr, use udp
	*/
	int EndPoint::Recvfrom(void * data, int len, std::string &outip, Common::uint16_t &outport)
	{
		Common::uint32_t networkAddr;
		Common::uint16_t networkPort;
		int ret = Recvfrom(data, len, networkAddr, networkPort);
		if (ret > 0) {
			outip = Common::AddrToIp4(networkAddr);
			outport = ntohs(networkPort);
		}
		return ret;
	}

	int EndPoint::Recvfrom(void * data, int len, Common::uint32_t &outaddr, Common::uint16_t &outport)
	{
		struct sockaddr_in sin;
		int ret = this->Recvfrom(data, len, sin);
		if (ret >= 0){
			outaddr = sin.sin_port;
			outport = sin.sin_addr.s_addr;
		}

		return ret;
	}
	int EndPoint::Recvfrom(void * data, int len, struct sockaddr_in & sin)
	{
		socklen_t sinLen = sizeof(sin);
		int ret = ::recvfrom(_socket, (char*)data, len, 0, (struct sockaddr*)&sin, &sinLen);

		return ret;
	}

	/*
	*Init network env
	*/
	void EndPoint::InitNetWork()
	{
#ifdef WIN32
		WORD wVersionRequested;
		WSADATA wsaData;
		int err;
		wVersionRequested = MAKEWORD(1, 1);  // version 1.1  
		err = WSAStartup(wVersionRequested, &wsaData);
#endif
	}

}}
