#include "core/event/tcp_session.h"
#include "core/event/tcp_server.h"
#include "core/event/event_loop.h"
#include "core/event/event_mgr.h"
#include "core/common/except.h"

namespace CXEngine {
namespace Event {
	TcpSession::TcpSession()
		:EventHandler(nullptr)
		,_server(nullptr)
		,_inbuffer(SESSIONBUFSIZE)
		,_outbuffer(SESSIONBUFSIZE)
	{
	}

	TcpSession::~TcpSession()
	{
	}

	/*
	*socket have data to read callback
	*/
	void TcpSession::EventReadCallback(EventLoop *netloop, int fd, void *arg)
	{
		if (_inbuffer.Unused() == 0) { //数据缓存已满，断开连接
			CloseSession();
		}

		int ret = _inbuffer.Recv(fd);
		if (ret == 0 || (ret < 0 && errno != EWOULDBLOCK)) { //连接断开
			CloseSession();
		}else if (ret > 0) {
			try {
				EventMgr::Instance()->AddReadableSession(this);
			}catch (Common::NormalExcept e) {
				CloseSession();
			}
		}
	}
	/*
	*socket can write data callback
	*/
	void TcpSession::EventWriteCallback(EventLoop *netloop, int fd, void *arg)
	{
		if (!_endpoint.Good()){
			_netloop->DelFileEvent(_endpoint.GetSocket());
			return;
		};

		if(_outbuffer.Used() > 0) _outbuffer.Send(fd);
		if (_outbuffer.Used() == 0) {
			_netloop->DisableFileEvent(_endpoint.GetSocket(), FileEventWritable);
		}
	}

	/*
	*Send Data
	*/
	unsigned int TcpSession::SendData(const void *data, unsigned int len)
	{
		if (_outbuffer.Unused() < len)
			throw NORMAL_EXCEPT("Send data failed");

		if (_endpoint.Good())
		{
			_outbuffer.In(data, len);
			_netloop->EnableFileEvent(_endpoint.GetSocket(), FileEventWritable);
		}
		return len;
	}

	/*
	*Read Data
	*/
	unsigned int TcpSession::ReadData(void *data, unsigned int len)
	{
		return _inbuffer.Out(data, len);
	}

	/*
	*Peek data
	*/
	unsigned int TcpSession::PeekData(void *data, unsigned int len)
	{
		return _inbuffer.Peek(data, len);
	}
	

	/*
	*enable session
	*/
	void TcpSession::EnableSession()
	{
		if (_endpoint.Good()) {
			_netloop->AddFileEvent(_endpoint.GetSocket(), BIND_CALLBACK_3(TcpSession::EventReadCallback, this),
				BIND_CALLBACK_3(TcpSession::EventWriteCallback, this), nullptr);
			_netloop->EnableFileEvent(_endpoint.GetSocket(), FileEventReadable);
		}
	}

	/*
	*close session, then add session to disconnect buffer
	*/
	void TcpSession::CloseSession()
	{
		_netloop->DelFileEvent(_endpoint.GetSocket());
		try {
			EventMgr::Instance()->AddDisconnectSession(this);
		}catch (Common::NormalExcept e) {
			_endpoint.Close();
		}
	}

	/*
	*reset session
	*/
	void TcpSession::ResetSession()
	{
		_endpoint.Close();
		_endpoint.GetAddress().ResetAddress();
		_inbuffer.Reset();
		_outbuffer.Reset();
		_netloop = nullptr;
		_server = nullptr;
		_datahandler = nullptr;
	}
}}