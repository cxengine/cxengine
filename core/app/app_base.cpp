#include "core/app/app_base.h"
#include "core/event/tcp_session.h"
#include "core/event/data_handler.h"
#include "core/event/event_loop.h"
#include "core/event/event_mgr.h"
#include "core/event/time_heap.h"
#include "core/common/string.h"
#include "core/common/except.h"

namespace CXEngine {
	namespace App {
		AppBase *g_appBase = nullptr;

		AppBase::AppBase()
			:_isRunning(false)
			,_ismulthread(false)
		{
			g_appBase = this;
			_loop = new CXEngine::Event::EventLoop;
		}

		AppBase::~AppBase()
		{
		}

		/*
		 *初始化AppBase
		 */
		void AppBase::InitBase(const std::string &config)
		{
			std::string tmpConfig = config;
			if (tmpConfig == "")
				tmpConfig = "./cfg/" + _cmdParser.GetProgrameName() + ".xml";
			_config.LoadConfig(tmpConfig);

			_ismulthread = _config.GetBoolValue("has_netthread");
		}

		/*
		*开始App
		*/
		void AppBase::StartApp()
		{
			if (!_isRunning) {
				_isRunning = true;
				if (_ismulthread) {
					_netthread = new std::thread(&AppBase::NetWorkThread, this);
				}
			}
		}

		/*
		*关闭App
		*/
		void AppBase::StopApp()
		{
			if (_isRunning) {
				_isRunning = false;
				_loop->StopLoop();
				if (_ismulthread) {
					_netthread->join();
				}
			}
		}

		/*
		*network thread work
		*/
		void AppBase::NetWorkThread()
		{
			while (_isRunning && _ismulthread){
				try {
					_loop->StartLoop(true);
				}catch (Common::NormalExcept e) {
					printf("[ERROR] catch except: %s\n", e.ToString().c_str());
					continue;
				}catch (...) {
					printf("[ERROR] catch a unknow except\n");
					continue;
				}
			}	
		}

		/*
		*网络监听
		*/
		void AppBase::RunNetwork()
		{
			if (_isRunning && !_ismulthread)
				_loop->StartLoop(false);
		}

		/*
		 *新连接处理
		 */
		bool AppBase::RunActive()
		{
			bool isBusy = false;
			while (true) {
				CXEngine::Event::TcpSession *session = CXEngine::Event::EventMgr::Instance()->GetConnectSession();
				if (!session)
					break;
				isBusy = true;
				session->EnableSession();
				CXEngine::Event::DataHandler *handler = session->GetDataHandler();
				if (handler) handler->SessionActive(session);
			}
			return isBusy;
		}

		/*
		 *断开连接处理
		 */
		bool AppBase::RunInactive()
		{
			bool isBusy = false;
			while (true) {
				CXEngine::Event::TcpSession *session = CXEngine::Event::EventMgr::Instance()->GetDisconnectSession();
				if (!session)
					break;

				isBusy = true;
				CXEngine::Event::DataHandler *handler = session->GetDataHandler();
				if (handler) handler->SessionInactive(session);
				CXEngine::Event::TcpSessionPool::Instance()->ReleaseObject(*session);
			}
			return isBusy;
		}

		/*
		 *处理定时器事件
		 */
		bool AppBase::RunTimer()
		{
			bool isBusy = false;
			while (true) {
				CXEngine::Event::TimeEvent *te = CXEngine::Event::EventMgr::Instance()->GetTimeEvent();
				if (!te)
					break;
				isBusy = true;
				te->CallHandler();
				if (!te->IsRepeat()) CXEngine::Event::TimeEventPool::Instance()->ReleaseObject(*te);
			}
			return isBusy;
		}

		/*
		 *处理Tcp网络数据逻辑
		 */
		bool AppBase::RunTcpData()
		{
			bool isBusy = false;
			while (true) {
				CXEngine::Event::TcpSession *session = CXEngine::Event::EventMgr::Instance()->GetReadableSession();
				if (!session)
					break;

				isBusy = true;
				CXEngine::Event::DataHandler *handler = session->GetDataHandler();
				if (handler) handler->DataArrival(session);
			}
			return isBusy;
		}
	}
}
