#include "core/app/app_base.h"
#include "core/common/time.h"
#include "core/common/except.h"

static bool ParseCmdLine()
{
	return true;
}

//逻辑循环线程
static void LogicRun()
{
	while (CXEngine::App::g_appBase->IsRunning())
	{
		bool isBusy = false;

		try {
			//单线程网络监听
			CXEngine::App::g_appBase->RunNetwork();

			//新连接
			isBusy |= CXEngine::App::g_appBase->RunActive();

			//断开连接
			isBusy |= CXEngine::App::g_appBase->RunInactive();

			//定时器逻辑处理
			isBusy |= CXEngine::App::g_appBase->RunTimer();

			//Tcp网络数据逻辑
			isBusy |= CXEngine::App::g_appBase->RunTcpData();
		}catch(Common::NormalExcept e){
			printf("[ERROR] catch except: %s\n", e.ToString().c_str());
			continue;
		}catch(...){
			printf("[ERROR] catch a unknow except\n");
			continue;
		}
		

		if (!isBusy) {
			Common::Sleep(100);
		}
	}
}



int main(int argc, char *argv[])
{
	//分析app命令
	CXEngine::App::CmdLineParser &parser = CXEngine::App::g_appBase->GetCmdParser();
	parser.Parse(argc, argv);

	//执行app指令
	if (ParseCmdLine() == false) return 0;

#ifdef __linux__
	//后台模式运行
	if (parser.IsDaemon()) {
		if (daemon(1, 1) != 0) 
			printf("[ERROR] Daemon mode failed, errno=%d: %s", errno, strerror(errno));
	}
#endif

	//加载config
	CXEngine::App::g_appBase->InitBase(parser.GetOptVal("--config"));

	//App逻辑初始化
	if (!CXEngine::App::g_appBase->InitApp()) {
		printf("[ERROR] Init %s failed.\n", parser.GetProgrameName().c_str());
		system("pause");
		return -1;
	}

	CXEngine::App::g_appBase->StartApp();

	printf("============================================================\n");
	printf("[LOG] start %s success.\n", parser.GetProgrameName().c_str());
	printf("============================================================\n");

	//逻辑线循环
	LogicRun();
}