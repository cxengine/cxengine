#include "core/app/cmd_line_parser.h"

namespace CXEngine {
namespace App {
	CmdLineParser::CmdLineParser() 
		:_daemon(false)
	{
	}
	const std::string& CmdLineParser::GetCmd() const
	{
		return _cmd;
	}

	bool CmdLineParser::IsOptExisted(const std::string& opt) const
	{
		auto ret = _opts.find(opt);
		return ret != _opts.end();
	}

	const std::string& CmdLineParser::GetOptVal(const std::string& opt) const
	{
		auto ret = _opts.find(opt);
		if (ret != _opts.end())
			return ret->second;

		return _emptyStr;
	}

	const std::string& CmdLineParser::GetProgramePath() const
	{
		return _programePath;
	}

	const std::string& CmdLineParser::GetProgrameName() const
	{
		return _programeName;
	}

	static std::string GenerateProgrameName(const std::string& appPath)
	{
		size_t count = std::string::npos;
#ifdef __linux__
		size_t begPos = appPath.find_last_of("/");
#else
		size_t begPos = appPath.find_last_of("\\");
		count = appPath.size() - 5 - begPos;
#endif
		return appPath.substr(begPos + 1, count);
	}

	bool CmdLineParser::Parse(int argc, char* argv[])
	{
		_programePath = argv[0];
		_programeName = GenerateProgrameName(argv[0]);
		for (int i = 1; i < argc; i++)
		{
			std::string str(argv[i]);
			std::string prefix = str.substr(0, 1);
			if (prefix != "-")
			{
				if (_cmd.empty())
				{
					if (!ParseCmd(str))
						return false;
				}
			}
			else
			{
				if (!ParseOpt(str))
					return false;
			}
		}

		return true;
	}

	bool CmdLineParser::ParseCmd(const std::string& str)
	{
		if (!_cmd.empty())
		{
			std::cerr << "Cmd line is invalid!\n";
			return false;
		}

		_cmd = str;

		return true;
	}

	bool CmdLineParser::ParseOpt(const std::string& str)
	{
		std::string prefix = str.substr(0, 2);
		if (prefix == "--")
		{
			size_t pos = str.find_first_of("=");
			if (pos == std::string::npos)
				return false;
			else
			{
				std::string opt = str.substr(0, pos);
				std::string val = str.substr(pos + 1);
				_opts.insert(std::make_pair(opt, val));
				return true;
			}
		}else {
			if (str == "-d") {
				_daemon = true;
			}
		}
		return false;
	}
}}
