#include "core/app/app_config.h"
#include "core/common/file.h"
#include "core/common/except.h"
#include "core/common/string.h"

namespace CXEngine {
namespace App {
	/*
	*加载配置文件
	*/
	void AppConfig::LoadConfig(const std::string& fileName)
	{
		if (!Common::IsPathExist(fileName))
		{
			throw NORMAL_EXCEPT("App config file not exist!");
		}
		_configValue = Common::GetValueMapFromFile(fileName);
	}

	/*
	*获取Value
	*/
	Common::Value AppConfig::GetValue(const std::string &key)
	{
		if (key.empty()) return _emptyValue;
		std::vector<std::string>keys = Common::SplitStr(key, ".");

		Common::Value* tmpValue = &_configValue;
		for (auto iter = keys.begin(); iter != keys.end(); ++iter) {
			Common::ValueMap &tmpMap = tmpValue->asValueMap();
			auto iterValue = tmpMap.find(*iter);
			if (iterValue == tmpMap.end()) {
				return _emptyValue;
			}
			tmpValue = &iterValue->second;
		}


		return *tmpValue;
	}

	/*
	*获得字符串参数
	*/
	std::string AppConfig::GetStrValue(const std::string& key)
	{
		Common::Value value = GetValue(key);
		try {
			return value.asString();
		}
		catch (Common::NormalExcept e) {
			return "";
		}
	}

	/*
	*获得字符串列表
	*/
	std::vector<std::string> AppConfig::GetStrVector(const std::string& key, const std::string& item)
	{
		int count = 1;
		std::vector<std::string> valueVector;
		while (true) {
			std::string fullkey = Common::Format("%s.%s%d", key.c_str(), item.c_str(), count);
			Common::Value value = GetValue(fullkey);
			if (value.getType() != Common::Value::Type::STRING)
				break;
			valueVector.push_back(value.asString());
			count++;
		}
		return valueVector;
	}

	/*
	*获得整数参数
	*/
	unsigned int AppConfig::GetIntValue(const std::string& key)
	{
		Common::Value value = GetValue(key);
		try {
			return value.asInt();
		}
		catch (Common::NormalExcept e) {
			return 0;
		}
	}

	/*
	*获得boolean参数
	*/
	bool AppConfig::GetBoolValue(const std::string& key)
	{
		const Common::Value& value = GetValue(key);
		try {
			return value.asBool();
		}
		catch (Common::NormalExcept e) {
			return false;
		}
	}
}}
