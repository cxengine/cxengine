#ifndef __CXENGINE_APP_APPCONFIG_H__
#define __CXENGINE_APP_APPCONFIG_H__
#include "core/common/container/value.h"
#include <unordered_map>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <string>

namespace CXEngine {
namespace App {
	class AppConfig
	{
	public:
		/*
		*加载配置文件
		*/
		void LoadConfig(const std::string& fileName);

		/*
		*获取Value
		*/
		Common::Value GetValue(const std::string &key);

		/*
		*获得字符串参数
		*/
		std::string GetStrValue(const std::string& name);

		/*
		*获得字符串列表
		*/
		std::vector<std::string> GetStrVector(const std::string& name, const std::string& item);

		/*
		*获得整数参数
		*/
		unsigned int GetIntValue(const std::string& name);

		/*
		*获得boolean参数
		*/
		bool GetBoolValue(const std::string& name);


	private:
		Common::Value _emptyValue;
		Common::Value _configValue;
	};
}}
#endif

