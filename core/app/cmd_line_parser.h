#ifndef __CXENGINE_APP_CMDLINEPARSER_H__
#define __CXENGINE_APP_CMDLINEPARSER_H__
#include <iostream>
#include <string>
#include <map>

namespace CXEngine {
namespace App {
	class  CmdLineParser
	{
	public:
		CmdLineParser();
		bool Parse(int argc, char* argv[]);
		const std::string& GetCmd() const;
		bool IsDaemon() { return _daemon; }
		bool IsOptExisted(const std::string& opt) const;
		const std::string& GetOptVal(const std::string& opt) const;
		const std::string& GetProgramePath() const;
		const std::string& GetProgrameName() const;
	private:
		bool ParseCmd(const std::string& str);
		bool ParseOpt(const std::string& str);

	private:
		std::string _programePath;
		std::string _programeName;
		std::string _cmd;
		bool _daemon;
		std::map<std::string, std::string> _opts;
		std::string _emptyStr;
	};
}}

#endif
