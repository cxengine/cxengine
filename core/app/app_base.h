#ifndef __CXENGINE_APP_APPBASE_H__
#define __CXENGINE_APP_APPBASE_H__
#include "core/app/cmd_line_parser.h"
#include "core/app/app_config.h"
#include <thread>

#ifdef WIN32
#pragma comment(lib, "libcore_event.lib")
#pragma comment(lib, "libcore_common.lib")
#pragma comment(lib, "libcore_app.lib")
#pragma comment(lib,"ws2_32.lib")
#endif

namespace CXEngine {
namespace Event {
	class EventLoop;
	class TcpSession;
}

namespace App {
	class AppBase
	{
	public:

		AppBase();
		~AppBase();

		/*
		*初始化AppBase
		*/
		void InitBase(const std::string &config);

		/*
		*初始化App，需要被重写
		*/
		virtual bool InitApp() = 0;

		/*
		*服务器状态
		*/
		bool IsRunning() { return _isRunning; }

		/*
		*开始App
		*/
		void StartApp();
		/*
		*关闭App
		*/
		void StopApp();

		/*
		*获取App参数
		*/
		CmdLineParser &GetCmdParser() { return _cmdParser; }

		/*
		*获取config
		*/
		AppConfig &GetAppConfig() { return _config; }

		/*
		*获得网络EventLoop
		*/
		CXEngine::Event::EventLoop *GetEventLoop() { return _loop; }

		/*
		*network thread work
		*/
		void NetWorkThread();
		/*
		*网络监听
		*/
		void RunNetwork();

		/*
		*新连接处理
		*/
		bool RunActive();

		/*
		*断开连接处理
		*/
		bool RunInactive();

		/*
		*处理定时器事件
		*/
		bool RunTimer();

		/*
		*处理Tcp网络数据逻辑
		*/
		bool RunTcpData();


	private:
		bool _isRunning;
		bool _ismulthread;
		CmdLineParser _cmdParser;
		AppConfig _config;
		CXEngine::Event::EventLoop *_loop;
		std::thread *_netthread;
	};

	extern AppBase *g_appBase;
}}
#endif

