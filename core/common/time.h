#ifndef __CXENGINE_COMMON_TIME_H__
#define __CXENGINE_COMMON_TIME_H__
#include "types.h"
namespace Common {
	/*
	*thread sleep us
	*/
	void Sleep(int us);

	/*
	*Get time
	*/
	int GetTimeOfDay(struct timeval *tp);

	/*
	*Get system tick(ms)
	*/
	long long GetSystemTick();

	/*
	*Different time1, time2
	*/
	bool DifferentDay(time_t t1, time_t t2);

	/*
	*Get second today form 00:00:00
	*/
	int GetSecondFromZone(time_t t);

	/*
	*time to string ex: hh:mm:ss
	*/
	std::string TimeToStr(time_t t, const std::string& format);
}
#endif
