#ifndef __CXENGINE_COMMON_OBJPOOL_H__
#define __CXENGINE_COMMON_OBJPOOL_H__
#include "types.h"

namespace Common {
	template <typename T>
	class ObjPool
	{
	public:
		ObjPool(int chunkSize = kDefaultChunkSize);
		~ObjPool();
		T& AcquireObject();
		void ReleaseObject(T& obj);
		virtual void OnAcquire(T& obj) {}
		virtual void OnRelease(T& obj) {}

	protected:
		std::queue<T*> mFreeList;
		std::vector<T*> mAllObjects;
		std::mutex		mtx;
		int mChunkSize;
		static const int kDefaultChunkSize = 64;

		void allocateChunk();
		static void arrayDeleteObject(T* obj);

		ObjPool(const ObjPool<T>& src) = delete;
		ObjPool<T>& operator=(const ObjPool<T>& rhs) = delete;
	};

	template<typename T>
	const int ObjPool<T>::kDefaultChunkSize;

	template <typename T>
	ObjPool<T>::ObjPool(int chunkSize)
		: mChunkSize(chunkSize)
	{
		if (mChunkSize <= 0) {
			mChunkSize = kDefaultChunkSize;
		}
		//	allocateChunk();
	}

	template <typename T>
	void ObjPool<T>::allocateChunk()
	{
		T* newObjects = new T[mChunkSize];
		mAllObjects.push_back(newObjects);
		for (int i = 0; i < mChunkSize; i++) {
			mFreeList.push(&newObjects[i]);
		}
	}

	template<typename T>
	void ObjPool<T>::arrayDeleteObject(T* obj)
	{
		delete[] obj;
	}

	template <typename T>
	ObjPool<T>::~ObjPool()
	{
		for_each(mAllObjects.begin(), mAllObjects.end(), arrayDeleteObject);
	}

	template <typename T>
	T& ObjPool<T>::AcquireObject()
	{
		std::lock_guard<std::mutex> lock(mtx);
		if (mFreeList.empty()) {
			allocateChunk();
		}
		T* obj = mFreeList.front();
		mFreeList.pop();
		OnAcquire(*obj);
		return (*obj);
	}

	template <typename T>
	void ObjPool<T>::ReleaseObject(T& obj)
	{
		std::lock_guard<std::mutex> lock(mtx);
		OnRelease(obj);
		mFreeList.push(&obj);
	}
}
#endif
