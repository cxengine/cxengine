#ifndef __CXENGINE_COMMON_FILE_H__
#define __CXENGINE_COMMON_FILE_H__
#include "container/data.h"
#include "container/value.h"
#include <iostream>

namespace Common
{
	/*
	*获取本执行文件所在文件夹路径
	*/
	std::string GetAppDir();

	/*
	*获取本执行文件的路径
	*/
	std::string GetAppPath();

	/*
	*查询文件夹是否存在
	*/
	bool IsPathExist(std::string path);

	/*
	*创建文件夹
	*/
	bool CreatePath(std::string path);

	/*
	*判断是否绝对路径
	*/
	bool IsAbsolutePath(const std::string& strPath);

	/*
	*获取绝对路径
	*/
	std::string FullPathForFilename(const std::string &filename);

	/*
	*读取文件数据
	*/
	Data GetDataFromFile(const std::string& filename, bool forString = false);

	/*
	*从文件中获取ValueMap
	*/
	ValueMap GetValueMapFromFile(const std::string& filename);

	/*
	*从字符串中获取ValueMap
	*/
	ValueMap GetValueMapFromData(const char* filedata, int filesize);
}
#endif

