#ifndef __CXENGINE_COMMON_MATH_H__
#define __CXENGINE_COMMON_MATH_H__
#include <iostream>

namespace Common {
	bool IsPowerOf2(unsigned int x);

	unsigned int RoundupPowOfTwo(unsigned int x);

	/*
	*Cal String CRC16
	*/
	unsigned short CRC16(const char *buf, int len);
}
#endif

