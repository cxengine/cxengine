#ifndef __CXENGINE_COMMON_SINGLETON_H__
#define __CXENGINE_COMMON_SINGLETON_H__

namespace Common 
{
	template <class T>
	class Singleton
	{
	public:
		virtual ~Singleton()
		{
		}

		static T* Instance()
		{
			if (!_instance)
				_instance = new T;

			return _instance;
		}

		static void Delete()
		{
			delete _instance;
			_instance = nullptr;
		}

	private:
		typedef T InstanceType;
		static T* _instance;
	};

	template <class T>
	typename Singleton<T>::InstanceType* Singleton<T>::_instance = nullptr;
}
#endif
