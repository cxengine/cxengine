#include "core/common/xml/dict_maker.h"
#include "core/common/except.h"
#include <math.h>


namespace Common
{
	DictMaker::DictMaker()
		: _resultType(SAX_RESULT_NONE)
	{
	}

	DictMaker::~DictMaker()
	{
	}

	ValueMap DictMaker::DictionaryWithContentsOfFile(const std::string& fileName)
	{
		_resultType = SAX_RESULT_DICT;
		SAXParser parser;

		if(!parser.Init("UTF-8"))
			throw NORMAL_EXCEPT("The file format isn't UTF-8");

		parser.SetDelegator(this);

		parser.Parse(fileName);
		return _rootDict;
	}

	ValueMap DictMaker::DictionaryWithDataOfFile(const char* filedata, int filesize)
	{
		_resultType = SAX_RESULT_DICT;
		SAXParser parser;

		if(!parser.Init("UTF-8"))
			throw NORMAL_EXCEPT("The file format isn't UTF-8");

		parser.SetDelegator(this);

		parser.Parse(filedata, filesize);
		return _rootDict;
	}

	ValueVector DictMaker::ArrayWithContentsOfFile(const std::string& fileName)
	{
		_resultType = SAX_RESULT_ARRAY;
		SAXParser parser;

		if (!parser.Init("UTF-8"))
			throw NORMAL_EXCEPT("The file format isn't UTF-8");

		parser.SetDelegator(this);

		parser.Parse(fileName);
		return _rootArray;
	}

	void DictMaker::StartElement(void *ctx, const char *name, const char **atts)
	{
		const std::string sName(name);
		if (sName == "dict")
		{
			if (_resultType == SAX_RESULT_DICT && _rootDict.empty())
			{
				_curDict = &_rootDict;
			}

			_state = SAX_DICT;

			SAXState preState = SAX_NONE;
			if (!_stateStack.empty())
			{
				preState = _stateStack.top();
			}

			if (SAX_ARRAY == preState)
			{
				// add a new dictionary into the array
				_curArray->push_back(Value(ValueMap()));
				_curDict = &(_curArray->rbegin())->asValueMap();
			}
			else if (SAX_DICT == preState)
			{
				if(_dictStack.empty())
					throw NORMAL_EXCEPT("The state is wrong!");

				ValueMap* preDict = _dictStack.top();
				(*preDict)[_curKey] = Value(ValueMap());
				_curDict = &(*preDict)[_curKey].asValueMap();
			}

			// record the dict state
			_stateStack.push(_state);
			_dictStack.push(_curDict);
		}
		else if (sName == "key")
		{
			_state = SAX_KEY;
		}
		else if (sName == "integer")
		{
			_state = SAX_INT;
		}
		else if (sName == "real")
		{
			_state = SAX_REAL;
		}
		else if (sName == "string")
		{
			_state = SAX_STRING;
		}
		else if (sName == "array")
		{
			_state = SAX_ARRAY;

			if (_resultType == SAX_RESULT_ARRAY && _rootArray.empty())
			{
				_curArray = &_rootArray;
			}
			SAXState preState = SAX_NONE;
			if (!_stateStack.empty())
			{
				preState = _stateStack.top();
			}

			if (preState == SAX_DICT)
			{
				(*_curDict)[_curKey] = Value(ValueVector());
				_curArray = &(*_curDict)[_curKey].asValueVector();
			}
			else if (preState == SAX_ARRAY)
			{
				if(_arrayStack.empty())
					throw NORMAL_EXCEPT("The state is wrong!");

				ValueVector* preArray = _arrayStack.top();
				preArray->push_back(Value(ValueVector()));
				_curArray = &(_curArray->rbegin())->asValueVector();
			}
			// record the array state
			_stateStack.push(_state);
			_arrayStack.push(_curArray);
		}
		else
		{
			_state = SAX_NONE;
		}
	}

	void DictMaker::EndElement(void *ctx, const char *name)
	{
		SAXState curState = _stateStack.empty() ? SAX_DICT : _stateStack.top();
		const std::string sName((char*)name);
		if (sName == "dict")
		{
			_stateStack.pop();
			_dictStack.pop();
			if (!_dictStack.empty())
			{
				_curDict = _dictStack.top();
			}
		}
		else if (sName == "array")
		{
			_stateStack.pop();
			_arrayStack.pop();
			if (!_arrayStack.empty())
			{
				_curArray = _arrayStack.top();
			}
		}
		else if (sName == "true")
		{
			if (SAX_ARRAY == curState)
			{
				_curArray->push_back(Value(true));
			}
			else if (SAX_DICT == curState)
			{
				(*_curDict)[_curKey] = Value(true);
			}
		}
		else if (sName == "false")
		{
			if (SAX_ARRAY == curState)
			{
				_curArray->push_back(Value(false));
			}
			else if (SAX_DICT == curState)
			{
				(*_curDict)[_curKey] = Value(false);
			}
		}
		else if (sName == "string" || sName == "integer" || sName == "real")
		{
			if (SAX_ARRAY == curState)
			{
				if (sName == "string")
					_curArray->push_back(Value(_curValue));
				else if (sName == "integer")
					_curArray->push_back(Value(atoi(_curValue.c_str())));
				else
					_curArray->push_back(Value(atof(_curValue.c_str())));
			}
			else if (SAX_DICT == curState)
			{
				if (sName == "string")
					(*_curDict)[_curKey] = Value(_curValue);
				else if (sName == "integer")
					(*_curDict)[_curKey] = Value(atoi(_curValue.c_str()));
				else
					(*_curDict)[_curKey] = Value(atof(_curValue.c_str()));
			}

			_curValue.clear();
		}

		_state = SAX_NONE;
	}

	void DictMaker::TextHandler(void *ctx, const char *ch, int len)
	{
		if (_state == SAX_NONE)
		{
			return;
		}

		SAXState curState = _stateStack.empty() ? SAX_DICT : _stateStack.top();
		const std::string text = std::string((char*)ch, len);

		switch (_state)
		{
		case SAX_KEY:
			_curKey = text;
			break;
		case SAX_INT:
		case SAX_REAL:
		case SAX_STRING:
		{
			if (curState == SAX_DICT)
			{
				if(_curKey.empty())
					throw NORMAL_EXCEPT("key not found : <integer/real>");
			}

			_curValue.append(text);
		}
		break;
		default:
			break;
		}
	}

}