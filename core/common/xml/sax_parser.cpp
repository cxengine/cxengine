#include "core/common/xml/sax_parser.h"
#include "core/common/xml/tinyxml2.h"
#include "core/common/container/data.h"
#include "core/common/file.h"
#include <vector>


namespace Common
{
	class XmlSaxHander : public tinyxml2::XMLVisitor
	{
	public:
		XmlSaxHander() :_ccsaxParserImp(0) {};

		virtual bool VisitEnter(const tinyxml2::XMLElement& element, const tinyxml2::XMLAttribute* firstAttribute);
		virtual bool VisitExit(const tinyxml2::XMLElement& element);
		virtual bool Visit(const tinyxml2::XMLText& text);
		virtual bool Visit(const tinyxml2::XMLUnknown&) { return true; }

		void setSAXParserImp(SAXParser* parser)
		{
			_ccsaxParserImp = parser;
		}

	private:
		SAXParser *_ccsaxParserImp;
	};


	bool XmlSaxHander::VisitEnter(const tinyxml2::XMLElement& element, const tinyxml2::XMLAttribute* firstAttribute)
	{
		//log(" VisitEnter %s",element.Value());

		std::vector<const char*> attsVector;
		for (const tinyxml2::XMLAttribute* attrib = firstAttribute; attrib; attrib = attrib->Next())
		{
			//log("%s", attrib->Name());
			attsVector.push_back(attrib->Name());
			//log("%s",attrib->Value());
			attsVector.push_back(attrib->Value());
		}

		// nullptr is used in c++11
		//attsVector.push_back(nullptr);
		attsVector.push_back(nullptr);

		SAXParser::StartElement(_ccsaxParserImp, (const unsigned char *)element.Value(), (const unsigned char **)(&attsVector[0]));
		return true;
	}

	bool XmlSaxHander::VisitExit(const tinyxml2::XMLElement& element)
	{
		//log("VisitExit %s",element.Value());

		SAXParser::EndElement(_ccsaxParserImp, (const unsigned char *)element.Value());
		return true;
	}

	bool XmlSaxHander::Visit(const tinyxml2::XMLText& text)
	{
		//log("Visit %s",text.Value());
		SAXParser::TextHandler(_ccsaxParserImp, (const unsigned char *)text.Value(), static_cast<int>(strlen(text.Value())));
		return true;
	}

	SAXParser::SAXParser()
	{
		_delegator = nullptr;
	}

	SAXParser::~SAXParser(void)
	{
	}

	bool SAXParser::Init(const char *encoding)
	{
		(void)encoding;
		return true;
	}

	bool SAXParser::Parse(const char* xmlData, size_t dataLength)
	{
		tinyxml2::XMLDocument tinyDoc;
		tinyDoc.Parse(xmlData, dataLength);
		XmlSaxHander printer;
		printer.setSAXParserImp(this);

		return tinyDoc.Accept(&printer);
	}

	bool SAXParser::Parse(const std::string& filename)
	{
		bool ret = false;
		Data data = Common::GetDataFromFile(filename);
		if (!data.isNull())
		{
			ret = Parse((const char*)data.getBytes(), data.getSize());
		}
		return ret;
	}

	void SAXParser::StartElement(void *ctx, const unsigned char *name, const unsigned char **atts)
	{
		((SAXParser*)(ctx))->_delegator->StartElement(ctx, (char*)name, (const char**)atts);
	}

	void SAXParser::EndElement(void *ctx, const unsigned char *name)
	{
		((SAXParser*)(ctx))->_delegator->EndElement(ctx, (char*)name);
	}

	void SAXParser::TextHandler(void *ctx, const unsigned char *name, int len)
	{
		((SAXParser*)(ctx))->_delegator->TextHandler(ctx, (char*)name, len);
	}

	void SAXParser::SetDelegator(SAXDelegator* delegator)
	{
		_delegator = delegator;
	}
}
