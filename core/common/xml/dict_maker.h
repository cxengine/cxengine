#ifndef __CXENGINE_COMMON_DICTMAKER_H__
#define __CXENGINE_COMMON_DICTMAKER_H__
#include "core/common/container/value.h"
#include "core/common/xml/sax_parser.h"
#include <stack>

namespace Common
{

	typedef enum
	{
		SAX_NONE = 0,
		SAX_KEY,
		SAX_DICT,
		SAX_INT,
		SAX_REAL,
		SAX_STRING,
		SAX_ARRAY
	}SAXState;

	typedef enum
	{
		SAX_RESULT_NONE = 0,
		SAX_RESULT_DICT,
		SAX_RESULT_ARRAY
	}SAXResult;

	class DictMaker : public SAXDelegator
	{
	public:
		SAXResult _resultType;
		ValueMap _rootDict;
		ValueVector _rootArray;

		std::string _curKey;   ///< parsed key
		std::string _curValue; // parsed value
		SAXState _state;

		ValueMap*  _curDict;
		ValueVector* _curArray;

		std::stack<ValueMap*> _dictStack;
		std::stack<ValueVector*> _arrayStack;
		std::stack<SAXState>  _stateStack;

	public:
		DictMaker();
		~DictMaker();

		ValueMap DictionaryWithContentsOfFile(const std::string& fileName);

		ValueMap DictionaryWithDataOfFile(const char* filedata, int filesize);

		ValueVector ArrayWithContentsOfFile(const std::string& fileName);

		void StartElement(void *ctx, const char *name, const char **atts);

		void EndElement(void *ctx, const char *name);

		void TextHandler(void *ctx, const char *ch, int len);
	};
}
#endif
