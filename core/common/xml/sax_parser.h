#ifndef __CXENGINE_COMMON_SAXPARSER_H__
#define __CXENGINE_COMMON_SAXPARSER_H__
#include <iostream>

namespace Common
{
	class SAXDelegator
	{
	public:
		virtual ~SAXDelegator() {}

		virtual void StartElement(void *ctx, const char *name, const char **atts) = 0;
		virtual void EndElement(void *ctx, const char *name) = 0;
		virtual void TextHandler(void *ctx, const char *s, int len) = 0;
	};

	class SAXParser
	{
		SAXDelegator*    _delegator;
	public:
		SAXParser();
		~SAXParser(void);
		bool Init(const char *encoding);
		bool Parse(const char* xmlData, size_t dataLength);
		bool Parse(const std::string& filename);
		void SetDelegator(SAXDelegator* delegator);
		static void StartElement(void *ctx, const unsigned char *name, const unsigned char **atts);
		static void EndElement(void *ctx, const unsigned char *name);
		static void TextHandler(void *ctx, const unsigned char *name, int len);
	};

}
#endif

