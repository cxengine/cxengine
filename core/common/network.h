#ifndef __CXENGINE_COMMON_NETWORK_H__
#define __CXENGINE_COMMON_NETWORK_H__
#include "types.h"

namespace Common {

	/*
	*string ip4 covert to network addr
	*/
	Common::uint32_t Ip4ToAddr(const std::string &str);

	/*
	*net work ip4 addr convert string
	*/
	std::string AddrToIp4(Common::uint32_t addr);

	/*
	*Get network string address
	*/
	std::string GetInterfaceAddressStr();

	/*
	*Get network address
	*/
	Common::uint32_t GetInterfaceAddress();
}
#endif
