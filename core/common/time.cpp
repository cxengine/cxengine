#include "time.h"
namespace Common {
	/*
	*thread sleep us
	*/
	void Sleep(int us)
	{
#ifdef WIN32
		if (us < 1000)
			us = 1000;
		::Sleep(us / 1000);
#else
		struct timeval tv;
		tv.tv_sec = 0;
		tv.tv_usec = us;
		select(0, nullptr, nullptr, nullptr, &tv);
#endif
	}

	/*
	*Get time
	*/
	int GetTimeOfDay(struct timeval *tp)
	{
#ifdef WIN32
		time_t clock;
		struct tm time;
		SYSTEMTIME wtm;
		GetLocalTime(&wtm);
		time.tm_year = wtm.wYear - 1900;
		time.tm_mon = wtm.wMonth - 1;
		time.tm_mday = wtm.wDay;
		time.tm_hour = wtm.wHour;
		time.tm_min = wtm.wMinute;
		time.tm_sec = wtm.wSecond;
		time.tm_isdst = -1;
		clock = mktime(&time);
		tp->tv_sec = clock;
		tp->tv_usec = wtm.wMilliseconds * 1000;
		return (0);
#else
		return ::gettimeofday(tp, NULL);
#endif
	}

	/*
	*Get system tick(ms)
	*/
	long long GetSystemTick()
	{
#ifdef WIN32
		return GetTickCount();
#else
        struct timespec ts;
        clock_gettime(CLOCK_MONOTONIC,&ts); 
        return (ts.tv_sec*1000 + ts.tv_nsec/(1000*1000));
#endif 
	}

	/*
	*Different time1, time2
	*/
	bool DifferentDay(time_t t1, time_t t2)
	{
		tm* tm1 = localtime(&t1);
		int year1 = tm1->tm_year;
		int day1 = tm1->tm_yday;

		tm* tm2 = localtime(&t2);
		int year2 = tm2->tm_year;
		int day2 = tm2->tm_yday;

		return year1 != year2 || day1 != day2;
	}

	/*
	*Get second today form 00:00:00
	*/
	int GetSecondFromZone(time_t t)
	{
		tm* tm = localtime(&t);
		return tm->tm_hour * 3600 + tm->tm_min * 60 + tm->tm_sec;
	}

	/*
	*time to string ex: hh:mm:ss
	*/
	std::string TimeToStr(time_t t, const std::string& format)
	{
		struct tm *pTm = localtime(&t);
		char tmpStr[200] = { 0 };
		strftime(tmpStr, sizeof(tmpStr), format.c_str(), pTm);  //not thread safe

		return std::string(tmpStr);
	}
}
