#include "except.h"

namespace Common {
	NormalExcept::NormalExcept(const std::string& file, int line, const std::string& func, const std::string& desc)
	{
		std::ostringstream oss(desc);
		oss << "[" << file << ":" << line << " " << func << "] ";
		oss << "errno: " << GetError() << " errstr: " << GetErrorString(GetError());
		oss << " " <<desc;
	}
}