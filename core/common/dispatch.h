#ifndef __CXENGINE_COMMON_DISPATCH_H__
#define __CXENGINE_COMMON_DISPATCH_H__
#include <functional>
#include <unordered_map>
#include <vector>

namespace Common {

	const short DefCmdType = 0X1111;
	const short DefCmdCode = 0X2222;

	template <class Content, class DispParam>
	class Dispatcher
	{
	public:
		typedef std::function<void(const Content&, DispParam&)> Handler;
		struct HandlerContent
		{
			HandlerContent(Handler h, const void* f = nullptr)
			{
				removed = false;
				handler = h;
				flag = f;
			}

			bool removed;
			Handler handler;
			const void* flag; //same key, diffrent handler
		};
		typedef std::unordered_multimap<int, HandlerContent> Handlers;
	public:
		virtual ~Dispatcher() {}
		void Register(short type, short code, Handler handler, const void* flag = nullptr)
		{
			int cmd = type << 16 | code;
			_handlers.insert(std::make_pair(cmd, HandlerContent(handler, flag)));
		}

		void Remove(short type, short code, const void* flag = nullptr)
		{
			int cmd = type << 16 | code;
			auto ret = _handlers.equal_range(cmd);
			for (auto it = ret.first; it != ret.second; ++it)
			{
				HandlerContent& hc = it->second;
				if (hc.flag == flag)
					hc.removed = true;
			}
		}

		bool Dispatch(short type, short code, const Content& mc, DispParam& param)
		{
			bool ret = DoDispatch(type << 16 | code, mc, param);

			if (!ret) //not found, dispatch default handler
				ret = DoDispatch(DefCmd, mc, param);

			return ret;
		}
	private:
		

		bool DoDispatch(int cmd, const Content& mc, DispParam& param)
		{
			bool ret = false;
			auto retIt = _handlers.equal_range(cmd);
			for (auto it = retIt.first; it != retIt.second; ++it)
			{
				HandlerContent& hc = it->second;
				hc.handler(mc, param);
				ret = true;
			}

			return ret;
		}

	private:
		static const int DefCmd = DefCmdType << 16 | DefCmdCode;
		Handlers _handlers;
	};

	template <class Content, class DispParam>
	class LiteDispatcher : public Dispatcher<Content, DispParam>
	{
	public:
		void Register(int cmd, typename Dispatcher<Content, DispParam>::Handler handler,
			void* flag)
		{
			Dispatcher<Content, DispParam>::Register(0, cmd, handler, flag);
		}

		bool Dispatch(int cmd, const Content& mc, DispParam& param)
		{
			return Dispatcher<Content, DispParam>::Dispatch(0, cmd, mc, param);
		}

		void Remove(int cmd, void* flag)
		{
			Dispatcher<Content, DispParam>::Remove(0, cmd, flag);
		}
	};
}
#endif
