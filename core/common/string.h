#ifndef __CXENGINE_COMMON_STRING_H__
#define __CXENGINE_COMMON_STRING_H__
#include "types.h"

namespace Common {
	std::string Vform(const char* format, va_list args);
	std::string Format(const char* stringFormat, ...);
	int ReplaceStr(std::string& str, const std::string& pattern, const std::string& newpat);
	std::vector<std::string> SplitStr(const std::string& str, const std::string& dec);
	std::string Trim(std::string& str, std::string& trimStr);

	template <class Result>
	Result StrToNum(const std::string& str)
	{
		std::istringstream iss(str);
		Result val;
		iss >> val;
		return val;
	}

	template <class IntParam>
	std::string NumToStr(IntParam val)
	{
		std::ostringstream oss;
		oss << val;
		return oss.str();
	}
}
#endif
