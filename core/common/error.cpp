#include "error.h"
#include "types.h"

namespace Common {
	int GetError()
	{
#ifdef WIN32
		return WSAGetLastError();
#else
		return errno;
#endif
	}

	void SetError(int errNo)
	{
#ifdef WIN32
		WSASetLastError(errNo);
#else
		errno = errNo;
#endif
	}

	std::string GetErrorString(int errNo)
	{
#ifdef WIN32
		switch (errNo) {
		case WSAEINTR:
			return "Interrupted function call";
		case WSAEBADF:
			return "File handle is not valid";
		case WSAEACCES: return "Permission denied";
		case WSAEFAULT: return "Bad address";
		case WSAEINVAL: return "Invalid argument";
		case WSAEMFILE: return "Too many open files";
		case WSAEWOULDBLOCK: return "Resource temporarily unavailable";
		case WSAEINPROGRESS: return "Operation now in progress";
		case WSAEALREADY: return "Operation already in progress";
		case WSAENOTSOCK: return "Socket operation on nonsocket";
		case WSAEDESTADDRREQ: return "Destination address required";
		case WSAEMSGSIZE: return "Message too long";
		case WSAEPROTOTYPE: return "Protocol wrong type for socket";
		case WSAENOPROTOOPT: return "Bad protocol option";
		case WSAEPROTONOSUPPORT: return "Protocol not supported";
		case WSAESOCKTNOSUPPORT: return "Socket type not supported";
		case WSAEOPNOTSUPP: return "Operation not supported";
		case WSAEPFNOSUPPORT: return "Protocol family not supported";
		case WSAEAFNOSUPPORT: return "Address family not supported by protocol family";
		case WSAEADDRINUSE: return "Address already in use";
		case WSAEADDRNOTAVAIL: return "Cannot assign requested address";
		case WSAENETDOWN: return "Deriver is down";
		case WSAENETUNREACH: return "Deriver is unreachable";
		case WSAENETRESET: return "Deriver dropped connection on reset";
		case WSAECONNABORTED: return "Software caused connection abort";
		case WSAECONNRESET: return "Connection reset by peer";
		case WSAENOBUFS: return "No buffer space available";
		case WSAEISCONN: return "Socket is already connected";
		case WSAENOTCONN: return "Socket is not connected";
		case WSAESHUTDOWN: return "Cannot send after socket shutdown";
		case WSAETOOMANYREFS: return "Too many references";
		case WSAETIMEDOUT: return "Connection timed out";
		case WSAECONNREFUSED: return "Connection refused";
		case WSAELOOP: return "Cannot translate name";
		case WSAENAMETOOLONG: return "Name too long";
		case WSAEHOSTDOWN: return "Host is down";
		case WSAEHOSTUNREACH: return "No route to host";
		case WSAENOTEMPTY: return "Directory not empty";
		case WSAEPROCLIM: return "Too many processes";
		case WSAEUSERS: return "User quota exceeded";
		case WSAEDQUOT: return "Disk quota exceeded";
		case WSAESTALE: return "Stale file handle reference";
		case WSAEREMOTE: return "Item is remote";
		case WSASYSNOTREADY: return "Deriver subsystem is unavailable";
		case WSAVERNOTSUPPORTED: return "Winsock.dll version out of range";
		case WSANOTINITIALISED: return "Successful WSAStartup not yet performed";
		case WSAEDISCON: return "Graceful shutdown in progress";
		case WSAENOMORE: return "No more results";
		case WSAECANCELLED: return "Call has been canceled";
		case WSAEINVALIDPROCTABLE: return "Procedure call table is invalid";
		case WSAEINVALIDPROVIDER: return "Service provider is invalid";
		case WSAEPROVIDERFAILEDINIT: return "Service provider failed to initialize";
		case WSASYSCALLFAILURE: return "System call failure";
		case WSASERVICE_NOT_FOUND: return "Service not found";
		default: return "Unknow error";
		}
#else
		return strerror(errNo);
#endif
	}
}
