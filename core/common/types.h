#ifndef __CXENGINE_COMMON_TYPES_H__
#define __CXENGINE_COMMON_TYPES_H__
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <map>
#include <vector>
#include <queue>
#include <unordered_map>
#include <iterator>
#include <stdexcept>
#include <memory>
#include <mutex>
#include <thread>
#include <algorithm>
#include <cmath>
#include <functional> 

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <stdarg.h>
#ifdef WIN32
#include <winsock.h>
#include <windows.h>
#include <direct.h>
#include <io.h>
#else
#include <sys/types.h>
#include <sys/socket.h>   
#include <sys/epoll.h>
#include <sys/ioctl.h>
#include <sys/stat.h> 
#include <sys/time.h>
#include <net/if.h>
#include <netinet/in.h>   
#include <netinet/tcp.h> 
#include <arpa/inet.h>  
#include <netdb.h>   
#include <unistd.h>
#include <fcntl.h>
#include <errno.h> 
#include <dirent.h>
#include <exception>
#endif

#ifdef WIN32
typedef int socklen_t;
#else
#endif

namespace Common {
#ifdef WIN32
	typedef char int8_t;
	typedef unsigned char uint8_t;
	typedef short int16_t;
	typedef unsigned short uint16_t;
	typedef int int32_t;
	typedef unsigned int uint32_t;
	typedef __int64 int64_t;
	typedef unsigned __int64 uint64_t;


#else // FreeBSD/FreeBSD64/Linux
	typedef char int8_t;
	typedef unsigned char uint8_t;
	typedef short int16_t;
	typedef unsigned short uint16_t;
	typedef int int32_t;
	typedef unsigned int uint32_t;
	typedef long long int64_t;
	typedef unsigned long long uint64_t;
#endif
}

#define BIND_CALLBACK_0(__selector__,__target__, ...) std::bind(&__selector__,__target__, ##__VA_ARGS__)
#define BIND_CALLBACK_1(__selector__,__target__, ...) std::bind(&__selector__,__target__, std::placeholders::_1, ##__VA_ARGS__)
#define BIND_CALLBACK_2(__selector__,__target__, ...) std::bind(&__selector__,__target__, std::placeholders::_1, std::placeholders::_2, ##__VA_ARGS__)
#define BIND_CALLBACK_3(__selector__,__target__, ...) std::bind(&__selector__,__target__, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, ##__VA_ARGS__)


#define MAX(a,b)            (((a) > (b)) ? (a) : (b))
#define MIN(a,b)            (((a) < (b)) ? (a) : (b))

#ifdef WIN32
#define barrier() MemoryBarrier()
#else
#define barrier() __asm__ __volatile__("": : :"memory")
#endif

#define mb()    barrier()
#define rmb()   mb()
#define wmb()   mb()

#define smp_mb()    mb()
#define smp_rmb()   rmb()
#define smp_wmb()   wmb()

#endif
