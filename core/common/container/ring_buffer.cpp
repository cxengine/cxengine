#include "ring_buffer.h"
#include "../math.h"
#include "../types.h"
#include <stdlib.h>
#include <string.h>


namespace Common
{
	RingBuffer::RingBuffer(unsigned int size)
	{
		if (size < RINGBUF_MINSIZE) size = RINGBUF_MINSIZE;
		_size = RoundupPowOfTwo(size);
		_in = 0;
		_out = 0;
		_mask = _size - 1;
		_data = malloc(_size);
	}

	RingBuffer::~RingBuffer()
	{

	}

	void RingBuffer::Reset()
	{
		_in = 0;
		_out = 0;
	}

	unsigned int RingBuffer::In(const void *src, unsigned int len)
	{
		unsigned int l, off;
		l = Unused();
		if (len > l)
			len = l;

		off = _in & _mask;
		l = MIN(len, _size - off);

		memcpy((char *)_data + off, src, l);
		memcpy(_data, (char *)src + l, len - l);

		smp_wmb();
		_in += len;
		return len;
	}

	unsigned int RingBuffer::Out(void *dst, unsigned int len)
	{
		unsigned int l, off;
		l = Used();
		if (len > l)
			len = l;

		off = _out & _mask;
		l = MIN(len, _size - off);

		memcpy(dst, (char *)_data + off, l);
		memcpy((char *)dst + l, _data, len - l);

		smp_wmb();
		_out += len;
		return len;
	}

	unsigned int RingBuffer::Peek(void *dst, unsigned int len)
	{
		unsigned int l, off;
		l = Unused();
		if (len > l)
			len = l;

		off = _out & _mask;
		l = MIN(len, _size - off);

		memcpy(dst, (char *)_data + off, l);
		memcpy((char *)dst + l, _data, len - l);

		smp_wmb();
		return len;
	}
}
