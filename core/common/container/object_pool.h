#ifndef __CORE_BASE_OBJECTPOOL_H__
#define __CORE_BASE_OBJECTPOOL_H__

#include <queue>
#include <vector>
#include <stdexcept>
#include <memory>
#include <mutex>
#include <algorithm>

using namespace std;

namespace Core
{

template <typename T>
class ObjectPool
{
public:
	ObjectPool(int chunkSize = kDefaultChunkSize);
	~ObjectPool();
	T& AcquireObject();
	void ReleaseObject(T& obj);
	virtual void OnAcquire(T& obj) {}
	virtual void OnRelease(T& obj) {}

protected:
	queue<T*> mFreeList;
	vector<T*> mAllObjects;
	mutex		mtx;
	int mChunkSize;
	static const int kDefaultChunkSize = 64;

	void allocateChunk();
	static void arrayDeleteObject(T* obj);

	ObjectPool(const ObjectPool<T>& src) = delete;
	ObjectPool<T>& operator=(const ObjectPool<T>& rhs) = delete;
};

template<typename T>
const int ObjectPool<T>::kDefaultChunkSize;

template <typename T>
ObjectPool<T>::ObjectPool(int chunkSize) 
	: mChunkSize(chunkSize)
{
	if (mChunkSize <= 0) {
		mChunkSize = kDefaultChunkSize;
	}
//	allocateChunk();
}

template <typename T>
void ObjectPool<T>::allocateChunk()
{
	T* newObjects = new T[mChunkSize];
	mAllObjects.push_back(newObjects);
	for (int i = 0; i < mChunkSize; i++) {
		mFreeList.push(&newObjects[i]);
	}
}

template<typename T>
void ObjectPool<T>::arrayDeleteObject(T* obj)
{
	delete[] obj;
}

template <typename T>
ObjectPool<T>::~ObjectPool()
{
	for_each(mAllObjects.begin(), mAllObjects.end(), arrayDeleteObject);
}

template <typename T>
T& ObjectPool<T>::AcquireObject()
{
	std::lock_guard<std::mutex> lock(mtx);
	if (mFreeList.empty()) {
		allocateChunk();
	}
	T* obj = mFreeList.front();
	mFreeList.pop();
	OnAcquire(*obj);
	return (*obj);
}

template <typename T>
void ObjectPool<T>::ReleaseObject(T& obj)
{
	lock_guard<mutex> lock(mtx);
	OnRelease(obj);
	mFreeList.push(&obj);
}
}
#endif
