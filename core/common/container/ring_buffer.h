#ifndef __CXENGINE_COMMON_RINGBUFFER_H__
#define __CXENGINE_COMMON_RINGBUFFER_H__
#define RINGBUF_MINSIZE		1024

namespace Common {
	class RingBuffer
	{
	public:
		RingBuffer(unsigned int size = RINGBUF_MINSIZE);
		~RingBuffer();

		void Reset();

		inline unsigned int Unused(){ return _size - (_in - _out); }
		inline unsigned int Used() { return _in - _out; }

		unsigned int In(const void *src, unsigned int len);
		unsigned int Out(void *dst, unsigned int len);
		unsigned int Peek(void *dst, unsigned int len);

	protected:
		unsigned int    _in;
		unsigned int    _out;
		unsigned int    _mask;
		unsigned int    _size;
		void        *_data;
	};
}
#endif

