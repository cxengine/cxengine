#ifndef __CXENGINE_COMMON_MINHEAP_H__
#define __CXENGINE_COMMON_MINHEAP_H__
#include <stdlib.h>
namespace Common
{
	template<typename Element>
	class MinHeap
	{
	public:
		MinHeap();
		~MinHeap();

		bool HeapEmpty() { return 0u == _num; }
		int HeapSize() { return _num; }
		Element* HeapTop() { return _num ? *_pElement : 0; }

		int HeapPush(Element* e);
		Element*  HeapPop();
		int HeapErase(Element* e);

	private:
		int HeapReserve(unsigned n);
		void HeapShiftUp(unsigned hole_index, Element* e);
		void HeapShiftDown(unsigned hole_index, Element* e);

		virtual void ElementInit(Element *element) = 0;
		virtual int ElementIndex(Element *element) = 0;
		virtual void ElementSetIndex(Element *element, int index) = 0;
		virtual bool ElementCmp(Element *a, Element *b) = 0;

	private:
		Element** _pElement;
		unsigned _num, _all;
	};

	template<typename Element> 
	MinHeap<Element>::MinHeap()
	{
		_pElement = (Element**)malloc(8 * sizeof(Element*));
		_all = 8; _num = 0;
	}

	template<typename Element> 
	MinHeap<Element>::~MinHeap()
	{
		if (_pElement) free(_pElement);
	}

	template<typename Element> 
	int MinHeap<Element>::HeapPush(Element* e)
	{
		if (HeapReserve(_num + 1))
			return -1;
		HeapShiftUp(_num++, e);
		return 0;
	}

	template<typename Element> 
	Element*  MinHeap<Element>::HeapPop()
	{
		if (_num)
		{
			Element* e = *_pElement;
			HeapShiftDown(0u, _pElement[--_num]);
			ElementInit(e);
			return e;
		}
		return 0;
	}

	template<typename Element> 
	int MinHeap<Element>::HeapErase(Element* e)
	{
		if (-1 != ElementIndex(e))
		{
			Element *last = _pElement[--_num];
			unsigned parent = (ElementIndex(e) - 1) / 2;
			if (ElementIndex(e) > 0 && ElementCmp(_pElement[parent], last))
				HeapShiftUp(ElementIndex(e), last);
			else
				HeapShiftDown(ElementIndex(e), last);
			ElementSetIndex(e, -1);
			return 0;
		}
		return -1;
	}

	template<typename Element> 
	int MinHeap<Element>::HeapReserve(unsigned n)
	{
		if (_all < n)
		{
			Element** p;
			unsigned a = (_all ? _all * 2 : 8);
			if (a < n)
				a = n;
			if (!(p = (Element**)realloc(_pElement, a * sizeof(Element*))))
				return -1;
			_pElement = p;
			_all = a;
		}
		return 0;
	}

	template<typename Element> 
	void MinHeap<Element>::HeapShiftUp(unsigned hole_index, Element* e)
	{
		unsigned parent = (hole_index - 1) / 2;
		while (hole_index && ElementCmp(_pElement[parent], e)){
			ElementSetIndex((_pElement[hole_index] = _pElement[parent]), hole_index);
			hole_index = parent;
			parent = (hole_index - 1) / 2;
		}
		ElementSetIndex((_pElement[hole_index] = e), hole_index);
	}

	template<typename Element> 
	void MinHeap<Element>::HeapShiftDown(unsigned hole_index, Element* e)
	{
		unsigned min_child = 2 * (hole_index + 1);
		while (min_child <= _num)
		{
			min_child -= min_child == _num || ElementCmp(_pElement[min_child], _pElement[min_child - 1]);
			if (!(ElementCmp(e, _pElement[min_child])))
				break;
			ElementSetIndex((_pElement[hole_index] = _pElement[min_child]), hole_index);
			hole_index = min_child;
			min_child = 2 * (hole_index + 1);

		}
		ElementSetIndex((_pElement[hole_index] = e), hole_index);
	}
}
#endif

