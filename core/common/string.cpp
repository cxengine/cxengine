#include "string.h"

namespace Common {
	std::string Vform(const char* format, va_list args)
	{
		size_t size = 1024;
		char* buffer = new char[size];

		while (1) {
			va_list args_copy;
#ifdef WIN32
			args_copy = args;
#else
			va_copy(args_copy, args);
#endif
			int n = vsnprintf(buffer, size, format, args_copy);
			va_end(args_copy);

			if ((n > -1) && (static_cast<size_t>(n) < size)) {
				std::string s(buffer);
				delete[] buffer;
				return s;
			}

			size = (n > -1) ? n + 1 : size * 2;
			delete[] buffer;
			buffer = new char[size];
		}
	}

	std::string Format(const char* stringFormat, ...)
	{
		va_list va;
		va_start(va, stringFormat);
		std::string tmpStr = Vform(stringFormat, va);
		va_end(va);

		return tmpStr;
	}

	int ReplaceStr(std::string& str, const std::string& pattern, const std::string& newpat)
	{
		int count = 0;
		const size_t nsize = newpat.size();
		const size_t psize = pattern.size();

		for (size_t pos = str.find(pattern, 0);
			pos != std::string::npos;
			pos = str.find(pattern, pos + nsize))
		{
			str.replace(pos, psize, newpat);
			count++;
		}

		return count;
	}


	std::vector<std::string> SplitStr(const std::string& str, const std::string& dec)
	{
		std::string outStr = str;
		if (dec != " ")
		{
			outStr = str;
			ReplaceStr(outStr, dec, " ");
		}

		std::vector<std::string> ret;
		std::istringstream iss(outStr);
		std::string s;
		while (iss >> s)
			ret.push_back(s);

		return ret;
	}


	std::string Trim(std::string& str, std::string& trimStr)
	{
		str.erase(str.find_last_not_of(trimStr) + 1);
		str.erase(0, str.find_first_not_of(trimStr));
		return str;
	}
}
