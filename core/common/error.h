#ifndef __CXENGINE_COMMON_ERROR_H__
#define __CXENGINE_COMMON_ERROR_H__
#include <iostream>

namespace Common {
	int GetError();
	void SetError(int errNo);
	std::string GetErrorString(int errNo);
}
#endif