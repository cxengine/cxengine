#ifndef __CXENGINE_COMMON_EXCEPT_H__
#define __CXENGINE_COMMON_EXCEPT_H__
#include <iostream>
#include <sstream>
#include <exception>
#include "error.h"

namespace Common {
	class NormalExcept : public std::exception
	{
	public:
		NormalExcept(const std::string& file, int line, const std::string& func, const std::string& desc);
		virtual ~NormalExcept() {}
		virtual const std::string& ToString() const { return _desc; }
	private:
		std::string _desc;
	};
}

#define NORMAL_EXCEPT(desc) Common::NormalExcept(__FILE__, __LINE__, __FUNCTION__, desc)
#endif
