#include "network.h"

namespace Common {

	Common::uint32_t Ip4ToAddr(const std::string &str)
	{
		return inet_addr(str.c_str());
	}

	std::string AddrToIp4(Common::uint32_t addr)
	{
		struct in_addr tmp; tmp.s_addr = addr;
		char *ip = inet_ntoa(tmp);
		return std::string(ip);
	}

	/*
	*Get network string address
	*/
	std::string GetInterfaceAddressStr()
	{
		return AddrToIp4(GetInterfaceAddress());
	}

	/*
	*Get network address
	*/
	Common::uint32_t GetInterfaceAddress()
	{
		char	myname[256];
		::gethostname(myname, sizeof(myname));
		struct hostent * myhost = gethostbyname(myname);
		if (!myhost) {
			return 0;
		}
		return ((struct in_addr*)(myhost->h_addr_list[0]))->s_addr;
	}
}
