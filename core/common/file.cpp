#include "file.h"
#include "string.h"
#include "except.h"
#include "xml/dict_maker.h"

namespace Common
{
	std::string GetAppDir()
	{
		std::string path = GetAppPath();
#ifdef WIN32
		int pos = (int)path.rfind("\\");
#else
		int pos = (int)path.rfind("/");
#endif
		path = path.substr(0, ++pos);
		return path;
	}

	/*
	*获取本执行文件的路径
	*/
	std::string GetAppPath()
	{
#ifdef WIN32
		char file[MAX_PATH];
		DWORD len = GetModuleFileNameA(nullptr, file, sizeof(file));

		// path = "d:\\imo\\bin\\IMOClientD.exe"
		std::string path(file, len);
		return path;
#else
		char buf[512];
		int ret = readlink("/proc/self/exe", buf, sizeof(buf) - 1);
		if (ret < 0 || (ret >= (int)sizeof(buf) - 1))
		{
			return "";
		}
		buf[ret] = '\0';
		return std::string(buf);
#endif
	}

	/*
	*查询文件目录是否存在
	*/
	bool IsPathExist(std::string path)
	{
		std::string fullpath = FullPathForFilename(path);
#ifdef WIN32
		if (_access(fullpath.c_str(), 0))
#else
		if (access(fullpath.c_str(), 0))
#endif
			return false;
		return true;
	}

	/*
	*创建文件夹
	*/
	bool CreatePath(std::string path)
	{
#ifdef WIN32
		if (_mkdir(path.c_str()) == -1)
#else
		if (mkdir(path.c_str(), 0755) == -1)
#endif 
		{
			return false;
		}

		return true;
	}

	/*
	*判断是否绝对路径
	*/
	bool IsAbsolutePath(const std::string& strPath)
	{
#ifdef WIN32
		if ((strPath.length() > 2
			&& ((strPath[0] >= 'a' && strPath[0] <= 'z') || (strPath[0] >= 'A' && strPath[0] <= 'Z'))
			&& strPath[1] == ':') || (strPath[0] == '/' && strPath[1] == '/'))
		{
			return true;
		}
		return false;
#else
		return (strPath[0] == '/');
#endif
	}

	/*
	*获取绝对路径
	*/
	std::string FullPathForFilename(const std::string &filename)
	{
		if (filename.empty())
		{
			return "";
		}

		if (IsAbsolutePath(filename))
		{
			return filename;
		}
		
		std::string appDir = GetAppDir();
		std::string fullpath = appDir + filename;
		return fullpath;
	}

	Data GetDataFromFile(const std::string& filename, bool forString)
	{
		if (filename.empty())
		{
			return Data::Null;
		}

		Data ret;
		unsigned char* buffer = nullptr;
		size_t size = 0;
		size_t readsize;
		const char* mode = nullptr;

		if (forString)
			mode = "rt";
		else
			mode = "rb";

		do
		{
			// Read the file from hardware
			std::string fullPath = FullPathForFilename(filename);
			FILE *fp = fopen(fullPath.c_str(), mode);
			if (!fp) break;

			fseek(fp, 0, SEEK_END);
			size = ftell(fp);
			fseek(fp, 0, SEEK_SET);

			if (forString)
			{
				buffer = (unsigned char*)malloc(sizeof(unsigned char) * (size + 1));
				buffer[size] = '\0';
			}
			else
			{
				buffer = (unsigned char*)malloc(sizeof(unsigned char) * size);
			}

			readsize = fread(buffer, sizeof(unsigned char), size, fp);
			fclose(fp);

			if (forString && readsize < size)
			{
				buffer[readsize] = '\0';
			}
		} while (0);

		if (nullptr == buffer || 0 == readsize)
		{
			std::string des = Format("Get data from file %s failed", filename.c_str());
			throw NORMAL_EXCEPT(des);
		}
		else
		{
			ret.fastSet(buffer, readsize);
		}

		return ret;
	}

	/*
	*从文件中获取ValueMap
	*/
	ValueMap GetValueMapFromFile(const std::string& filename)
	{
		const std::string fullPath = FullPathForFilename(filename.c_str());
		DictMaker tMaker;
		return tMaker.DictionaryWithContentsOfFile(fullPath.c_str());
	}

	/*
	*从字符串中获取ValueMap
	*/
	ValueMap GetValueMapFromData(const char* filedata, int filesize)
	{
		DictMaker tMaker;
		return tMaker.DictionaryWithDataOfFile(filedata, filesize);
	}
}
