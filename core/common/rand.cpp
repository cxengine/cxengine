#include "rand.h"
#include "time.h"
#include <assert.h>

namespace Common
{
	Common::uint64_t GetRankU64()
	{
		static uint16_t lastNum = 0;

		static uint64_t tv = (uint64_t)(time(NULL));
		uint64_t now = (uint64_t)(time(NULL));
		if (now != tv)
		{
			tv = now;
			lastNum = 0;
		}

		// 时间戳32位，随机数16位，16位迭代数（最大为65535-1）
		static uint32_t rnd = 0;
		if (rnd == 0){
			srand(GetSystemTick());
			rnd = (uint32_t)(rand() << 16);
		}

		assert(lastNum < 65535 && "genUUID64(): overflow!");

		return (tv << 32) | rnd | lastNum++;
	}
}
