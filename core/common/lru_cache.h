#ifndef __CXENGINE_COMMON_LRUCACHE_H__
#define __CXENGINE_COMMON_LRUCACHE_H__
#include <unordered_map>
#include <vector>
#include <mutex>
#include <assert.h>
#include <string.h>

namespace Common
{
    template <class K, class D>
        class LRUCache{
            public:
				struct Node {
					K key;
					D *data;
					Node *prev, *next;
					Node() { data = nullptr; prev = next = nullptr; }
				};

                LRUCache(int size , bool thread_safe = false)
					:_size(size)
					, _thread_safe(thread_safe)
				{
					if (size <= 0) 
						_size = 1024;

					_addsize = _size >> 2;
					if (_addsize < 128)
						_addsize = 128;
                }

                ~LRUCache()
				{
                    delete head;
                    delete tail;
                }

				void InitLRUCache()
				{
					for (int i = 0; i < _size; ++i)
					{
						Node *node = new Node;
						_free_cached_entries.push_back(node);
						_all_cached_entries.push_back(node);
					}
					head = new Node;
					tail = new Node;
					head->prev = NULL;
					head->next = tail;
					tail->prev = head;
					tail->next = NULL;
				}

                /*
                 *Put Data
                 */
                void Put(const K &key, D* data);

                /*
                 *Get Data by key, if not exist return null
                 */
                D* Get(const K &key);

                /*
                 *Check Data is exist
                 */
                bool Exist(const K &key);

            private:
				virtual bool CanDelete(D* data)
				{
					return true;
				}

				virtual void DeleteNodeData(D *data)
				{
				}

                void cached_lock(void)
				{
                    if(_thread_safe)
                        mtx.lock();
                }

                void cached_unlock(void)
				{
                    if(_thread_safe)
                        mtx.unlock();
                }

                void detach(Node* node)
				{
                    node->prev->next = node->next;
                    node->next->prev = node->prev;
                }

                void attach(Node* node)
				{
                    node->prev = head;
                    node->next = head->next;
                    head->next = node;
                    node->next->prev = node;
                }

            private:
				int _size;
				int _addsize; 
                std::unordered_map<K, Node* > _cached_map;
                std::vector<Node* > _free_cached_entries;
				std::vector<Node* > _all_cached_entries;
                Node * head, *tail;
                bool _thread_safe;
                std::mutex mtx;
        };

    /*
     *Put Data
     */
    template<class K , class D>
        void LRUCache<K, D>::Put(const K &key , D* data){
            cached_lock();
            Node *node = _cached_map[key];
            if(node) //node exist
			{
                detach(node);
				DeleteNodeData(node->data);
				node->data = data;
                attach(node);
            }
			else //node not exist
			{
                if(_free_cached_entries.empty()) //free node size is 0
				{
                    node = tail->prev;
					if (CanDelete(node->data)) //can delete data
					{ 
						DeleteNodeData(node->data);
						detach(node);
						_cached_map.erase(node->key);
					}
					else //can't delete data, new data
					{ 
						node = new Node;
						_all_cached_entries.push_back(node);
						for (int i = 0; i < _size; ++i) 
						{
							Node *new_node = new Node;
							_free_cached_entries.push_back(new_node);
							_all_cached_entries.push_back(new_node);
						}
					}
                    
                }
				else
				{
                    node = _free_cached_entries.back();
                    _free_cached_entries.pop_back();
                }
                node->key = key;
				node->data = data;
                _cached_map[key] = node;
                attach(node);
            }
            cached_unlock();
        }

    /*
     *Get Data by key, if not exist return null
     */
    template<class K , class D>
        D* LRUCache<K,D>::Get(const K &key)
		{
            cached_lock();
            Node *node = _cached_map[key];
            if(node)
			{
                detach(node);
                attach(node);
                cached_unlock();
                return node->data;
            }
			else
			{
                cached_unlock();
                return nullptr;
            }
        }

    /*
     *Check Data is exist
     */
    template<class K , class D>
        bool LRUCache<K, D>::Exist(const K &key)
        {
            Node *node = _cached_map[key];
            if(node)
                return true;
            return false;
        }

}
#endif


