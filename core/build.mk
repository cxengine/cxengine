#common.mk

DEBUG = yes

# directories
CX_BUILD_DIR = $(CX_HOME)/core/lib/build
CX_INC_DIR = $(CX_HOME)
CX_LIB_DIR = $(CX_HOME)/core/lib

# compiler and link name
CXX = g++
EXE_LD = g++
STATICLIB_LD = ar -crs


# CXX compiler option
 CXXFLAGS = -c
 ifeq "$(DEBUG)" "no"
 	CXXFLAGS += -O2
 else
 	CXXFLAGS += -g
 endif

 CXXFLAGS += -std=c++11 -Wall -Wno-unused-parameter -Wextra -pipe -D_NEW_LIC -D_GNU_SOURCE -D_REENTRANT 
 CXXFLAGS += -fPIC -fpermissive

 CXXFLAGS += -I$(CX_INC_DIR)

# link option 
LDFLAGS = -L$(CX_LIB_DIR) 
